﻿using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Exporters.Json;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Order;
using Benchmarking.Models;

namespace Benchmarking.Helpers
{
    public class BenchmarkConfig : ManualConfig
    {
        public BenchmarkConfig(TestTaskSettings settings)
        {
            Options = ConfigOptions.StopOnFirstError;

            AddColumnProvider(DefaultColumnProviders.Instance);
            AddDiagnoser(MemoryDiagnoser.Default);
            AddLogger(ConsoleLogger.Default);
            AddExporter(DefaultExporters.Json);
            AddExporter(DefaultExporters.Plain);
            
            string nameSuffix = $"_{settings.DatabaseProvider}_{settings.InputScaleLevel}_{settings.ReferenceScale}";

            AddExporter(new JsonExporter(fileNameSuffix: nameSuffix, indentJson: true, excludeMeasurements: true));

            var orderer = new DefaultOrderer(SummaryOrderPolicy.FastestToSlowest, MethodOrderPolicy.Declared);

            WithOrderer(orderer);

            var defaultRoutineJob = Job.Default
                .WithStrategy(RunStrategy.Throughput)
                .WithUnrollFactor(1)
                .WithIterationCount(100)
                .WithInvocationCount(1)
                .WithLaunchCount(1);

            AddJob(defaultRoutineJob);

            var onceExecutionJob = Job.Default
                .WithStrategy(RunStrategy.ColdStart)
                .WithUnrollFactor(1)
                .WithIterationCount(1)
                .WithInvocationCount(1)
                .WithLaunchCount(10);

            if (settings.MustUseColdCacheTesting)
                AddJob(onceExecutionJob);
        }
    }
}
