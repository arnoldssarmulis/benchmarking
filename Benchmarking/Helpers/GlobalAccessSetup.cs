﻿using BenchmarkDotNet.Attributes;
using DBAccess;
using EFCoreDBContext.Config;
using LibraryEntities;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.Helpers
{
    public abstract class GlobalAccessSetup
    {
        protected LibraryDBContext _dbContext;

        protected DataAccessLayer _dbAccess;

        /// <summary>
        /// Since each benchmark in BenchmarkDotNet is executed as a separate process, the GlobalSetup is called independently for each benchmark, and the state is not shared across them. 
        /// </summary>
        [GlobalSetup]
        public void Setup()
        {
            var currentRuntimeSettings = JsonHelper.GetCurrentTaskSettings();

            //Šo vajadzēs kārtīgi aprakstīt par to ka katrs benchmark tiek laists atsevišķā procesā un
            //starp tiem nav iespējams šeirot resursus atmiņā kā static vai kā citādi,
            //var šeirot tikai ar starprocesu komunikāciju vai failiem ,vai db
            if (currentRuntimeSettings.IsDBInitializedForTask)
            {
                currentRuntimeSettings.MustRecreateDB = false;
                currentRuntimeSettings.MustUseSeeding = false;

                _dbContext = LibraryDBContext.InitDBContext(currentRuntimeSettings);
            }
            else
            {
                //Katram [Benchmark] tiek savs _dbContext, jo katrs tiek laists atsevišķā procesā un tie nevar šeirot resursus tāpēc kopīgīe dati jāglabā kādā failā vai starp procesu sheirota kratuve
                _dbContext = LibraryDBContext.InitDBContext(JsonHelper.GetCurrentTaskSettings());

                currentRuntimeSettings.IsDBInitializedForTask = true;
                JsonHelper.SetCurrentTaskSettings(currentRuntimeSettings);
                Console.WriteLine("\n\n\nDB successfuly initialized! " + DateTime.UtcNow + "\n\n\n");

            }
            PrintOutSeedDataAndConfig(currentRuntimeSettings);

            _dbAccess = new DataAccessLayer();
        }

        // <summary>
        // Katra benchmarka nobeigumā tiek izpildīts
        // </summary>
        [GlobalCleanup]
        public void Cleanup()
        {
            //if (DBProvider.IsMSSQLServer)
            //{
            //    _dbContext.Database.ExecuteSqlRaw("DBCC DROPCLEANBUFFERS;");
            //}
            //else if (DBProvider.IsMySQL)
            //{
            //    _dbContext.Database.ExecuteSqlRaw("RESET QUERY CACHE;");
            //}
            //else if (DBProvider.IsPostgreSQL)
            //{

            //}

            _dbContext.Dispose();
        }

        //[IterationSetup]
        //public void IterationSetup()
        //{
        //    _dbContext.Database.ExecuteSqlRaw("DBCC DROPCLEANBUFFERS;");
        //}

        //Nav saderīgs ar microbenchmarkiem - [Benchmark]
        //[IterationCleanup]
        //public void Cleanup()
        //{
        //    _dbContext.Database.ExecuteSqlRaw("DBCC DROPCLEANBUFFERS;");
        //}

        private void PrintOutSeedDataAndConfig(DBContextSettings currentSettings)
        {
            Console.Write(Environment.NewLine);
            Enumerable.Range(1, 30).ToList().ForEach(action => Console.Write("-"));
            Console.Write(Environment.NewLine);
            Console.WriteLine($"Current configuration:");
            Console.Write(Environment.NewLine);

            Console.WriteLine($"ReferenceScale: {currentSettings.ReferenceScale}");
            Console.WriteLine($"InputScale: {currentSettings.InputScaleLevel}");
            Console.WriteLine($"DatabaseProvider: {currentSettings.DatabaseProvider}");
            Console.WriteLine($"UsedSeeding: {currentSettings.MustUseSeeding}");
            Console.WriteLine($"RecreatedDatabase: {currentSettings.MustRecreateDB}");

            Console.Write(Environment.NewLine);
            Enumerable.Range(1, 30).ToList().ForEach(action => Console.Write("-"));
            Console.Write(Environment.NewLine);
            Console.WriteLine("Current record counts in tables: ");
            Console.Write(Environment.NewLine);

            var dbContextType = typeof(LibraryDBContext);

            var dbSetProperties = dbContextType.GetProperties()
                .Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>));

            foreach (var dbSetProperty in dbSetProperties)
            {
                var dbSet = dbSetProperty.GetValue(_dbContext) as IQueryable<object>;

                int recordCount = dbSet.Count();

                Console.WriteLine($"Table: {dbSetProperty.Name}, Record Count: {recordCount}");
            }

            Console.Write(Environment.NewLine);
            Enumerable.Range(1, 30).ToList().ForEach(action => Console.Write("-"));
            Console.Write(Environment.NewLine);
            Console.WriteLine("Reference counts:");
            Console.Write(Environment.NewLine);

            var authorBookCounts = _dbContext.BookAuthors
                .GroupBy(ba => ba.AuthorId)
                .Select(authorGroup => new
                {
                    AuthorBookCount = authorGroup.Count()
                });

            var maxAuthorBookCount = authorBookCounts.Max(ab => ab.AuthorBookCount);
            var minAuthorBookCount = authorBookCounts.Min(ab => ab.AuthorBookCount);
            var avgAuthorBookCount = authorBookCounts.Average(ab => ab.AuthorBookCount);

            Console.WriteLine($"{nameof(minAuthorBookCount)}: " + minAuthorBookCount);
            Console.WriteLine($"{nameof(maxAuthorBookCount)}: " + maxAuthorBookCount);
            Console.WriteLine($"{nameof(avgAuthorBookCount)}: " + avgAuthorBookCount);

            Console.WriteLine();
            Console.WriteLine();

            var bookAuthorCounts = _dbContext.BookAuthors
                .GroupBy(ba => ba.BookId)
                .Select(authorGroup => new
                {
                    AuthorBookCount = authorGroup.Count()
                });

            var maxBookAuthorCount = bookAuthorCounts.Max(ab => ab.AuthorBookCount);
            var minBookAuthorCount = bookAuthorCounts.Min(ab => ab.AuthorBookCount);
            var avgBookAuthorCount = bookAuthorCounts.Average(ab => ab.AuthorBookCount);

            Console.WriteLine($"{nameof(minBookAuthorCount)}: " + minBookAuthorCount);
            Console.WriteLine($"{nameof(maxBookAuthorCount)}: " + maxBookAuthorCount);
            Console.WriteLine($"{nameof(avgBookAuthorCount)}: " + avgBookAuthorCount);
            Console.WriteLine();

            var bookReviewCounts = _dbContext.BookReviews
                .GroupBy(bk => bk.BookId)
                .Select(bookGroup => new
                {
                    BookReviewCount = bookGroup.Count()
                });

            var maxBookReviewCount = bookReviewCounts.Max(bk => bk.BookReviewCount);
            var minBookReviewCount = bookReviewCounts.Min(bk => bk.BookReviewCount);
            var avgBookReviewCount = bookReviewCounts.Average(bk => bk.BookReviewCount);

            Console.WriteLine($"{nameof(minBookReviewCount)}: " + minBookReviewCount);
            Console.WriteLine($"{nameof(maxBookReviewCount)}: " + maxBookReviewCount);
            Console.WriteLine($"{nameof(avgBookReviewCount)}: " + avgBookReviewCount);
            Console.WriteLine();

            var authorFriendCounts = _dbContext.AuthorFriendList
                .GroupBy(afl => afl.AuthorId)
                .Select(authorGroup => new
                {
                    AuthorFriendCount = authorGroup.Count()
                });

            var maxAuthorFriendCount = authorFriendCounts.Max(afl => afl.AuthorFriendCount);
            var minAuthorFriendCount = authorFriendCounts.Min(afl => afl.AuthorFriendCount);
            var avgAuthorFriendCount = authorFriendCounts.Average(afl => afl.AuthorFriendCount);

            Console.WriteLine($"{nameof(minAuthorFriendCount)}: " + minAuthorFriendCount);
            Console.WriteLine($"{nameof(maxAuthorFriendCount)}: " + maxAuthorFriendCount);
            Console.WriteLine($"{nameof(avgAuthorFriendCount)}: " + avgAuthorFriendCount);

            Console.WriteLine();
            Console.WriteLine();

            var friendAuthorCounts = _dbContext.AuthorFriendList
                .GroupBy(afl => afl.FriendsWithAuthorId)
                .Select(authorGroup => new
                {
                    AuthorFriendCount = authorGroup.Count()
                });

            var maxFriendAuthorCount = friendAuthorCounts.Max(afl => afl.AuthorFriendCount);
            var minFriendAuthorCount = friendAuthorCounts.Min(afl => afl.AuthorFriendCount);
            var avgFriendAuthorCount = friendAuthorCounts.Average(afl => afl.AuthorFriendCount);

            Console.WriteLine($"{nameof(minFriendAuthorCount)}: " + minFriendAuthorCount);
            Console.WriteLine($"{nameof(maxFriendAuthorCount)}: " + maxFriendAuthorCount);
            Console.WriteLine($"{nameof(avgFriendAuthorCount)}: " + avgFriendAuthorCount);

            Enumerable.Range(1, 30).ToList().ForEach(action => Console.Write("-"));
            Console.Write(Environment.NewLine);
        }
    }
}
