﻿using Benchmarking.Models;
using EFCoreDBContext.Config;
using System.Text.Json;

namespace Benchmarking.Helpers
{
    internal static class JsonHelper
    {
        private static readonly string fileName = "currentConfig.json";
        private static readonly string rootDirectory = Path.GetPathRoot(Environment.SystemDirectory);
        private static readonly string jsonConfigDirectory = Path.Combine(rootDirectory, "BenchmarkConfig");
        private static readonly string filePath = Path.Combine(jsonConfigDirectory, fileName);

        public static bool IsDBInitializedForCurrentTask
        {
            get 
            {
                var currentFileContent = ReadObjectFromJsonFile<RuntimeTaskSettings>();

                return currentFileContent.IsDBInitializedForTask;
            }
        }

        public static void SetCurrentTaskSettings(DBContextSettings contextSettings, bool isFirstIteration)
        {
            var runtimeContextSettings = new RuntimeTaskSettings(contextSettings)
            {
                IsDBInitializedForTask = !isFirstIteration
            };

            WriteObjectToJsonFile(runtimeContextSettings);
        }

        public static void SetCurrentTaskSettings(RuntimeTaskSettings contextSettings)
        {
            WriteObjectToJsonFile(contextSettings);
        }

        public static RuntimeTaskSettings GetCurrentTaskSettings()
        {
            return ReadObjectFromJsonFile<RuntimeTaskSettings>();
        }

        public static void WriteObjectToJsonFile<T>(T obj)
        {
            try
            {
                if (!Directory.Exists(jsonConfigDirectory))
                    Directory.CreateDirectory(jsonConfigDirectory);

                string jsonString = JsonSerializer.Serialize(obj, new JsonSerializerOptions { WriteIndented = true });

                File.WriteAllText(filePath, jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error writing JSON to file: {ex.Message}");
            }
        }

        public static T ReadObjectFromJsonFile<T>()
        {
            try
            {
                if (File.Exists(filePath))
                {
                    string jsonString = File.ReadAllText(filePath);
                    return JsonSerializer.Deserialize<T>(jsonString);
                }
                else
                    return default;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error reading JSON from file: {ex.Message}");
                return default;
            }
        }

    }
}
