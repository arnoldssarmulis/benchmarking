﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using Benchmarking.Models;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Serilog;
using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using System.Text.RegularExpressions;

namespace Benchmarking.Helpers
{
    public class TaskRunner
    {
        private static ILogger _logger;

        static TaskRunner()
        {
            _logger = new LoggerConfiguration()
                .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        public static void ExecuteTasks()
        {
            try
            {
                var tasks = ReadTasksFromConfiguration();

                tasks.ForEach(task =>
                {
                    bool isFirstBenchmarkExecution = true;

                    task.TestClassess.ForEach(c =>
                    {
                        var obj = GetBenchmarkInstance(c);
                        if (obj == null)
                            return;

                        JsonHelper.SetCurrentTaskSettings(task, isFirstBenchmarkExecution);

                        MethodInfo runMethod = typeof(BenchmarkRunner).GetMethod("Run", new[] { typeof(IConfig), typeof(string[]) });

                        Type typeOfTestClass = obj.GetType();

                        MethodInfo genericRunMethod = runMethod.MakeGenericMethod(typeOfTestClass);

                        IConfig config = new BenchmarkConfig(task);
                        string[] benchmarkArgs = null;

                        object result = genericRunMethod.Invoke(null, new object[] { config, benchmarkArgs });

                        isFirstBenchmarkExecution = false;
                    });

                    GroupResultsByTask(task.TestClassess, task.DatabaseProvider, task.InputScaleLevel, task.ReferenceScale);
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error: {Message}", ex.Message);
            }

        }

        private static void GroupResultsByTask(List<string> testClasses, DatabaseProviderType databaseProvider, InputScaleLevel inputScaleLevel, double referenceScale)
        {
            string sourceDirectory = "C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\EFCodeFirst\\Benchmarking\\bin\\Release\\net7.0";// AppDomain.CurrentDomain.BaseDirectory;
            string resultsDir = "BenchmarkDotNet.Artifacts";
            sourceDirectory = Path.Combine(sourceDirectory, resultsDir);

            string newFolderName = $"{databaseProvider}_{DateTime.Now.Date:dd-MM-yyyy}_18";
            string newDatabaseFolderPath = Path.Combine(sourceDirectory, newFolderName);

            if (!Directory.Exists(newDatabaseFolderPath))
                Directory.CreateDirectory(newDatabaseFolderPath);

            newFolderName = inputScaleLevel.ToString();
            string newinputScaleLevelFolderPath = Path.Combine(newDatabaseFolderPath, newFolderName);

            if (!Directory.Exists(newinputScaleLevelFolderPath))
                Directory.CreateDirectory(newinputScaleLevelFolderPath);

            var files = Directory.GetFiles(sourceDirectory)
                .ToList();

            files.ForEach(filePath =>
            {
                string fileName = Path.GetFileName(filePath);
                fileName = Path.ChangeExtension(fileName, null);

                string fileClassName = Regex.Match(fileName, @"[-][.a-zA-Z]*[-]").Value;
                fileClassName = Regex.Replace(fileClassName, @"[-][\w]*[.]", string.Empty);
                fileClassName = Regex.Replace(fileClassName, @"[^\w]*", string.Empty);

                if (string.IsNullOrEmpty(fileClassName))
                    return;

                var classNames = testClasses.Where(tc =>
                {
                    string className = Regex.Match(tc, @"[.]\w+$").Value;

                    return className.Contains(fileClassName);
                });

                if (!classNames.Any() || classNames.Count() > 1)
                    return;

                var className = Regex.Match(classNames.First(), @"[^.]*$").Value;

                string id = Regex.Match(fileName, @"[-]\d*$").Value;

                string newFileName = $"{className}_withReferenceScale_{referenceScale:0.00}{id}.txt";

                string newScalePath = Path.Combine(newinputScaleLevelFolderPath, newFileName);

                File.Move(filePath, newScalePath);
            });
        }

        private static List<TestTaskSettings> ReadTasksFromConfiguration()
        {
            IConfigurationRoot configuration = Configuration.GetConfiguration;

            var tasks = configuration.GetSection("Tasks").Get<List<TestTaskSettings>>();

            if (tasks == null)
                throw new Exception("No records found!");

            return tasks;
        }

        private static object GetBenchmarkInstance(string className)
        {
            var type = Type.GetType(className);
            if (type != null)
            {
                return Activator.CreateInstance(type);
            }
            else
            {
                Console.WriteLine($"Class {className} not found.");
            }

            return null;
        }
    }
}
