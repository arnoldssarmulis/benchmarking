﻿using EFCoreDBContext.Config;

namespace Benchmarking.Models
{
    public class RuntimeTaskSettings : DBContextSettings
    {
        //Vajag priekš JSON deserializācijas citādi ir exception!
        public RuntimeTaskSettings() { }

        public RuntimeTaskSettings(DBContextSettings contextSettings) : base(contextSettings) { }

        public bool IsDBInitializedForTask { get; set; } = false;
    }
}
