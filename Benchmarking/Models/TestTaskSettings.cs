﻿using EFCoreDBContext.Config;

namespace Benchmarking.Models
{
    public class TestTaskSettings : DBContextSettings
    {
        public List<string> TestClassess { get; set; }
    }
}
