﻿using Benchmarking.Helpers;
using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using LibraryEntities;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace Benchmarking
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Start of operations");

            TaskRunner.ExecuteTasks();
           
            Console.WriteLine("End of operations");
            Console.WriteLine("Press enter to end");

            Console.ReadLine();
        }
    }
}