﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects
{
    [MemoryDiagnoser]
    public class AnotherQuery : GlobalAccessSetup
    {
        //Katrs savaa procesaa
        [Benchmark]
        public void ToList()
        {
            var book = _dbContext.Books.Take(1).ToList();
        }

        //Katram pa jaunu izsauc global setup
        [Benchmark]
        public void ShorterToList()
        {
            var book = _dbContext.Books.Take(1).ToList();
        }

        //Resursus starp benchmark testiem nevar sheirot jo tie ir atseviski procesi
        [Benchmark]
        public void ShortestToList()
        {
            var book = _dbContext.Books.Take(10).ToList();
        }
    }
}
