﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects
{
    [MemoryDiagnoser]
    public class BookQuery : GlobalAccessSetup
    {
        [Benchmark]
        public void QueryBooksByGenreAndAuthorUsingFirstOrDefault()
        {
            var book = _dbContext.Books
                .FirstOrDefault(b => b.BookAuthors.Any(ba => ba.Author.Name == "Ace Collins")
                    && b.Genre.Contains("Holiday"));
        }

        [Benchmark]
        public void QueryBooksByGenreAndAuthorUsingFirst()
        {
            var book = _dbContext.Books
                .FirstOrDefault(b => b.BookAuthors.Any(ba => ba.Author.Name == "Ace Collins")
                    && b.Genre.Contains("Holiday"));
        }

        [Benchmark]
        public void QueryBooksByGenreAndAuthorUsingUsingWhere()
        {
            var book = _dbContext.Books
                .Where(b => b.BookAuthors.Any(ba => ba.Author.Name == "Ace Collins")
                    && b.Genre.Contains("Holiday"))
                .FirstOrDefault();
        }

        [Benchmark]
        public void QueryBooksByGenreUsingSelectManyFirstOrDefault()
        {
            var book = _dbContext.Books
                .SelectMany(b => b.BookAuthors,
                            (b, ba) => new { Book = b, BookAuthor = ba })
                .FirstOrDefault(b => b.BookAuthor.Author.Name == "Ace Collins" &&
                                     b.Book.Genre.Contains("Holiday"));
        }

        /*
        [Benchmark]
        public void QueryBooksByGenreUsingJoinAndFirstOrDefault()
        {
            var book = DBAccess._dbContext.Books
                .Join(DBAccess._dbContext.BookAuthors,
                      book => book.Id,
                      bookAuthor => bookAuthor.BookId,
                      (book, bookAuthor) => new { book, bookAuthor })
                .Join(DBAccess._dbContext.Authors,
                      combined => combined.bookAuthor.AuthorId,
                      author => author.Id,
                      (combined, author) => new { combined.book, combined.bookAuthor, author })
                .Where(combined => combined.author.Name == "Ace Collins" && combined.book.Genre.Contains("Holiday"))
                .Select(combined => combined.book)
                .FirstOrDefault();
        }

        //[Benchmark]
        //public void QueryBooksWhereFirst()
        //{
        //    //#1
        //    var holidayBooks = DBAccess._dbContext.Books
        //        .Where(b => b.Genre.Contains("Holiday"))
        //        .ToList();
        //}

        [Benchmark]
        public void QueryBooksWhereSecond()
        {
            //#1
            var holidayBooks = DBAccess._dbContext.Books
                .Where(b => b.Genre.Contains("Holiday"))
                .ToList();

            //#2
            var _holidayBooks = DBAccess._dbContext.Books
                .ToList()
                .Where(b => b.Genre.Contains("Holiday"));
        }

        /*
        [Benchmark]
        public void QueryBookssWhereVsAnyFirst()S
        {
            //#1
            bool isLargeBookInDB = DBAccess._dbContext.Books
                .Where(b => b.Pages > 100)
                .Count() > 0;
        }

        [Benchmark]
        public void QueryBookssWhereVsAnySecond()
        {
            //#2
            bool _isLargeBookInDB = DBAccess._dbContext.Books
                .Any(b => b.Pages > 100);

        }

        [Benchmark]
        public void QueryAuthorFirst()
        {
            //#1
            var books = DBAccess._dbContext.Books.Where(b => b.Pages == 0);

            if (books.Count() > 1)
            {
                books.ToList()
                    .ForEach(b => b.AvarageRating = 0);

                DBAccess._dbContext.SaveChanges();
            }
        }

        [Benchmark]
        public void QueryAuthorSecond()
        {

            //#2
            DBAccess._dbContext.Books
                .Where(b => b.Pages == 0)
                .ToList()
                .ForEach(b => b.AvarageRating = 0);

            DBAccess._dbContext.SaveChanges();
        }*/
    }
}
