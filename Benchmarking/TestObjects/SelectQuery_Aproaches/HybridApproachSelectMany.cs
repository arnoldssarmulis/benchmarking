﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using LibraryEntities;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    public class HybridApproachSelectMany : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_HybridAproach_02()
        {
            //var authorStatistics = _dbContext.Authors
            //        .Join(
            //            _dbContext.AuthorFriendList
            //            .GroupBy(a => a.AuthorId)
            //            .Select(g => new
            //            {
            //                AuthorId = g.Key,
            //                AuthorFriendCount = g.Count()
            //            }),
            //            author => author.Id,
            //            friendList => friendList.AuthorId,
            //            (author, friendList) => new
            //            {
            //                AuthorId = author.Id,
            //                friendList.AuthorFriendCount
            //            })
            //        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
            //        .Select(g => new
            //        {
            //            g.Key.AuthorFriendCount,
            //            AuthorAverageReviewRating = g.Select(a => a.).Average(c => c.ReviewRating)
            //        })
            //        .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
            //        .OrderByDescending(b => b.AuthorFriendCount)
            //        .ThenByDescending(b => b.AuthorAverageReviewRating)
            //        .Take(10)
            //        .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_02()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_HybridAproach_03()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                    author => author.Id,
                    bookAuthor => bookAuthor.AuthorId,
                    (author, bookAuthor) => new
                    {
                        Author = author,
                        Book = bookAuthor.Book
                    })
                .GroupBy(result => result.Author.Id)
                .Select(g => new
                {
                    AuthorFriendCount = g.First().Author.Friends
                        .Count(),
                    AuthorAverageReviewRating = g.SelectMany(bg => bg.Book.BookReviews)
                        .Average(review => review.Rating),
                    AuthorBestBook = g
                        .Max(bg => bg.Book.AvarageRating)
                })
                .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_03()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.Rating
                        }
                    )
                    .Where(br => br.Rating > 0)
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.Books,
                        book => book.BookId,
                        review => review.Id,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.AvarageRating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorBestBook = b.Max(c => c.AvarageRating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        author_fl.AuthorAverageReviewRating,
                        author_br.AuthorBestBook
                    })
                .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }


        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_HybridAproach_04()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                    author => author.Id,
                    bookAuthor => bookAuthor.AuthorId,
                    (author, bookAuthor) => new 
                    { 
                        Author = author, 
                        Book = bookAuthor.Book 
                    })
                .GroupBy(result => result.Author.Id)
                .Select(g => new
                {
                    AuthorFriendCount = g.First().Author.Friends
                        .Count(),
                    AuthorAverageReviewRating = g.SelectMany(bg => bg.Book.BookReviews)
                        .Average(review => review.Rating),
                    AuthorBestBook = g
                        .Max(bg => bg.Book.AvarageRating),
                    AuthorTotalReviewCount = g.SelectMany(bg => bg.Book.BookReviews)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .ThenByDescending(a => a.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_04()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.Rating
                        }
                    )
                    .Where(br => br.Rating > 0)
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating),
                        AuthorTotalReviewCount = b.Count()
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating,
                        author_br.AuthorTotalReviewCount
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.Books,
                        book => book.BookId,
                        review => review.Id,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.AvarageRating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorBestBook = b.Max(c => c.AvarageRating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        author_fl.AuthorAverageReviewRating,
                        author_br.AuthorBestBook,
                        author_fl.AuthorTotalReviewCount
                    })
                    .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(100)
                    .ToList();
        }

    }
}
