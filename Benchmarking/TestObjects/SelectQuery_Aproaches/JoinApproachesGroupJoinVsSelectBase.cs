﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    public class JoinApproachesGroupJoinVsSelectBase : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_01_GroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .GroupJoin(
                    _dbContext.AuthorFriendList,
                    author => author.Id,
                    friend => friend.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorFriendCount = friendList.Count()
                    })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_02_GroupJoin()
        {
            var alalal = _dbContext.Authors
                .GroupJoin(_dbContext.BookAuthors,
                    author => author.Id,
                    authorReviews => authorReviews.AuthorId,
                    (author, bookAuthors) => new
                    {
                        AuthorAverageReviewRating = bookAuthors.SelectMany(ba => ba.Book.BookReviews)
                             .Average(ar => ar.Rating),
                        AuthorFriendCount = author.Friends
                             .Count()
                    })
                .Take(10)
                .ToList();
        }


        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors.SelectMany(ba => ba.Book.BookReviews)
                        .Average(ar => ar.Rating)
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_03_GroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .GroupJoin(_dbContext.BookAuthors,
                    author => author.Id,
                    bookAuthors => bookAuthors.AuthorId,
                    (author, bookAuthors) => new
                    {
                        AuthorFriendCount = author.Friends
                            .Count(),
                        AuthorAverageReviewRating = bookAuthors.SelectMany(ba => ba.Book.BookReviews)
                            .Average(ba => ba.Rating),
                        AuthorBestBook = bookAuthors.Select(ba => ba.Book)
                            .Max(b => b.AvarageRating)
                    })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors.SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors
                        .Max(ba => ba.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_04_GroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .GroupJoin(_dbContext.BookAuthors,
                    author => author.Id,
                    bookAuthors => bookAuthors.AuthorId,
                    (author, bookAuthors) => new
                    {
                        AuthorFriendCount = author.Friends
                            .Count(),
                        AuthorAverageReviewRating = bookAuthors.SelectMany(ba => ba.Book.BookReviews)
                            .Average(ba => ba.Rating),
                        AuthorBestBook = bookAuthors.Select(ba => ba.Book)
                            .Max(b => b.AvarageRating),
                        AuthorTotalReviewCount = bookAuthors.SelectMany(ba => ba.Book.BookReviews)
                            .Count()
                    })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_04()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors.SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors
                        .Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors.SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Take(10)
                .ToList();
        }


        ///// <summary>
        ///// Selects AuthorFriendCount for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_01_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friend => friend.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorFriendCount = friendList.Count()
        //            })
        //        .Take(10)
        //        .ToList();
        //}

        ////What happens when GroupJoin is beeing used as left join
        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_02_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                AuthorFriendCount = friendList.Count(),
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author_fl => author_fl.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author_fl, bookAuthor) => new
        //            {
        //                author_fl.AuthorId,
        //                author_fl.AuthorFriendCount,
        //                bookAuthor.BookId
        //            })
        //        .Join(
        //            _dbContext.Books,
        //            authorBook => authorBook.BookId,
        //            book => book.Id,
        //            (authorBook, bookAuthor) => new
        //            {
        //                authorBook.AuthorId,
        //                authorBook.AuthorFriendCount,
        //                bookAuthor.AvarageRating
        //            })
        //        .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
        //        .Select(a => new
        //        {
        //            a.Key.AuthorFriendCount,
        //            AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //        })
        //    .Take(10)
        //    .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_02_Select()
        //{
        //    var authorStatistics = _dbContext.Authors
        //            .Join(
        //                _dbContext.BookAuthors,
        //                author => author.Id,
        //                bookAuthor => bookAuthor.AuthorId,
        //                (author, bookAuthor) => new
        //                {
        //                    AuthorId = author.Id,
        //                    bookAuthor.BookId
        //                })
        //            .Join(
        //                _dbContext.Books,
        //                authorBook => authorBook.BookId,
        //                book => book.Id,
        //                (authorBook, book) => new
        //                {
        //                    authorBook.AuthorId,
        //                    book.AvarageRating
        //                })
        //            .GroupBy(a => new { a.AuthorId })
        //            .Select(a => new
        //            {
        //                AuthorFriendCount = _dbContext.AuthorFriendList
        //                    .Where(afl => afl.AuthorId == a.Key.AuthorId)
        //                    .Count(),
        //                AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //            })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_03_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                AuthorFriendCount = friendList.Count(),
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author => author.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author, bookAuthor) => new
        //            {
        //                author.AuthorId,
        //                bookAuthor.BookId,
        //                author.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookReviews,
        //            ba => ba.BookId,
        //            review => review.BookId,
        //            (ba, review) => new
        //            {
        //                ba.AuthorId,
        //                ba.AuthorFriendCount,
        //                ReviewRating = review.Rating,
        //            })
        //        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Key.AuthorFriendCount,
        //            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_03_Join()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Join(
        //            _dbContext.AuthorFriendList
        //            .GroupBy(a => a.AuthorId)
        //            .Select(g => new
        //            {
        //                AuthorId = g.Key,
        //                AuthorFriendCount = g.Count()
        //            }),
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                friendList.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author => author.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author, bookAuthor) => new
        //            {
        //                author.AuthorId,
        //                bookAuthor.BookId,
        //                author.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookReviews,
        //            ba => ba.BookId,
        //            review => review.BookId,
        //            (ba, review) => new
        //            {
        //                ba.AuthorId,
        //                ba.AuthorFriendCount,
        //                ReviewRating = review.Rating,
        //            })
        //        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Key.AuthorFriendCount,
        //            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_01()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Join(_dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                friendList.AuthorId,
        //                friendList.FriendsWithAuthorId
        //            })
        //        .GroupBy(a => a.AuthorId)
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Count()
        //        })
        //        .Where(b => b.AuthorFriendCount > 0)
        //        .OrderByDescending(b => b.AuthorFriendCount)
        //        .Take(10)
        //        .ToList();
        //}
    }
}
