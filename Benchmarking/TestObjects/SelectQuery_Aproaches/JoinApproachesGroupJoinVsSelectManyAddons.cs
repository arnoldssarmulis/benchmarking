﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using LibraryEntities;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    public class JoinApproachesGroupJoinVsSelectManyAddons : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Where(b => b.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_01_WithSelectMany()
        {
            var authorStatistics = _dbContext.Authors
                .SelectMany(author => _dbContext.AuthorFriendList
                    .Where(friend => friend.AuthorId == author.Id),
                    (author, friend) => new { 
                        AuthorId = author.Id, 
                        friendId = friend.FriendsWithAuthorId 
                    })
                .GroupBy(
                    result => result.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorFriendCount = friendList.Count()
                    })
                .Where(b => b.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .Average(br => br.Book.AvarageRating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_02_SelectMany()
        {
            var authorStatistics = _dbContext.Authors
                .SelectMany(author => _dbContext.BookAuthors
                    .Where(ba => ba.AuthorId == author.Id),
                    (author, book) => new
                    {
                        Author = author,
                        AuthorBook = book
                    })
                .GroupBy(result => result.Author.Id)
                .Select(a => new
                {
                    AuthorFriendCount = a.First().Author.Friends
                        .Count(),
                    AuthorAverageReviewRating = a.SelectMany(bg => bg.AuthorBook.Book.BookReviews)
                        .Average(review => review.Rating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(result => result.AuthorFriendCount)
                .ThenByDescending(result => result.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors.SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors
                        .Max(ba => ba.Book.AvarageRating)
                })
                .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_03_SelectMany()
        {
            var authorStatistics = _dbContext.Authors
                .SelectMany(author => _dbContext.BookAuthors
                    .Where(ba => ba.AuthorId == author.Id),
                    (author, book) => new
                    {
                        Author = author,
                        AuthorBook = book
                    })
                .GroupBy(result => result.Author.Id)
                .Select(a => new
                {
                    AuthorFriendCount = a.First().Author.Friends
                        .Count(),
                    AuthorAverageReviewRating = a.SelectMany(bg => bg.AuthorBook.Book.BookReviews)
                        .Average(review => review.Rating),
                    AuthorBestBook = a
                        .Max(bg => bg.AuthorBook.Book.AvarageRating)
                })
                .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_04()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends
                        .Count(),
                    AuthorAverageReviewRating = author.BookAuthors.SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors
                        .Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors.SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_04_SelectMany()
        {
            var authorStatistics = _dbContext.Authors
                .SelectMany(author => _dbContext.BookAuthors
                    .Where(ba => ba.AuthorId == author.Id),
                    (author, book) => new
                    {
                        Author = author,
                        AuthorBook = book.Book
                    })
                .GroupBy(result => result.Author.Id)
                .Select(a => new 
                {
                    AuthorFriendCount = a.First().Author.Friends
                        .Count(),
                    AuthorAverageReviewRating = a.SelectMany(bg => bg.AuthorBook.BookReviews)
                        .Average(review => review.Rating),
                    AuthorBestBook = a
                        .Max(bg => bg.AuthorBook.AvarageRating),
                    AuthorTotalReviewCount = a.SelectMany(ba => ba.AuthorBook.BookReviews)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }


        ///// <summary>
        ///// Selects AuthorFriendCount for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_01_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friend => friend.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorFriendCount = friendList.Count()
        //            })
        //        .Take(10)
        //        .ToList();
        //}

        ////What happens when GroupJoin is beeing used as left join
        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_02_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                AuthorFriendCount = friendList.Count(),
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author_fl => author_fl.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author_fl, bookAuthor) => new
        //            {
        //                author_fl.AuthorId,
        //                author_fl.AuthorFriendCount,
        //                bookAuthor.BookId
        //            })
        //        .Join(
        //            _dbContext.Books,
        //            authorBook => authorBook.BookId,
        //            book => book.Id,
        //            (authorBook, bookAuthor) => new
        //            {
        //                authorBook.AuthorId,
        //                authorBook.AuthorFriendCount,
        //                bookAuthor.AvarageRating
        //            })
        //        .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
        //        .Select(a => new
        //        {
        //            a.Key.AuthorFriendCount,
        //            AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //        })
        //    .Take(10)
        //    .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_02_Select()
        //{
        //    var authorStatistics = _dbContext.Authors
        //            .Join(
        //                _dbContext.BookAuthors,
        //                author => author.Id,
        //                bookAuthor => bookAuthor.AuthorId,
        //                (author, bookAuthor) => new
        //                {
        //                    AuthorId = author.Id,
        //                    bookAuthor.BookId
        //                })
        //            .Join(
        //                _dbContext.Books,
        //                authorBook => authorBook.BookId,
        //                book => book.Id,
        //                (authorBook, book) => new
        //                {
        //                    authorBook.AuthorId,
        //                    book.AvarageRating
        //                })
        //            .GroupBy(a => new { a.AuthorId })
        //            .Select(a => new
        //            {
        //                AuthorFriendCount = _dbContext.AuthorFriendList
        //                    .Where(afl => afl.AuthorId == a.Key.AuthorId)
        //                    .Count(),
        //                AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //            })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_03_GroupJoin()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .GroupJoin(
        //            _dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                AuthorFriendCount = friendList.Count(),
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author => author.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author, bookAuthor) => new
        //            {
        //                author.AuthorId,
        //                bookAuthor.BookId,
        //                author.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookReviews,
        //            ba => ba.BookId,
        //            review => review.BookId,
        //            (ba, review) => new
        //            {
        //                ba.AuthorId,
        //                ba.AuthorFriendCount,
        //                ReviewRating = review.Rating,
        //            })
        //        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Key.AuthorFriendCount,
        //            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_03_Join()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Join(
        //            _dbContext.AuthorFriendList
        //            .GroupBy(a => a.AuthorId)
        //            .Select(g => new
        //            {
        //                AuthorId = g.Key,
        //                AuthorFriendCount = g.Count()
        //            }),
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                AuthorId = author.Id,
        //                friendList.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookAuthors,
        //            author => author.AuthorId,
        //            bookAuthor => bookAuthor.AuthorId,
        //            (author, bookAuthor) => new
        //            {
        //                author.AuthorId,
        //                bookAuthor.BookId,
        //                author.AuthorFriendCount
        //            })
        //        .Join(
        //            _dbContext.BookReviews,
        //            ba => ba.BookId,
        //            review => review.BookId,
        //            (ba, review) => new
        //            {
        //                ba.AuthorId,
        //                ba.AuthorFriendCount,
        //                ReviewRating = review.Rating,
        //            })
        //        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Key.AuthorFriendCount,
        //            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using join.
        ///// </summary>
        //[Benchmark]
        //public void Query_Join_01()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Join(_dbContext.AuthorFriendList,
        //            author => author.Id,
        //            friendList => friendList.AuthorId,
        //            (author, friendList) => new
        //            {
        //                friendList.AuthorId,
        //                friendList.FriendsWithAuthorId
        //            })
        //        .GroupBy(a => a.AuthorId)
        //        .Select(g => new
        //        {
        //            AuthorFriendCount = g.Count()
        //        })
        //        .Where(b => b.AuthorFriendCount > 0)
        //        .OrderByDescending(b => b.AuthorFriendCount)
        //        .Take(10)
        //        .ToList();
        //}
    }
}
