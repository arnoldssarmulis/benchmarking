﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    [MemoryDiagnoser]
    public class NavPropertiesMandatoryApproaches : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01_AnyBeforeSelection()
        {
            var authorStatistics = _dbContext.Authors
                .Where(a => a.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Take(10)
                .ToList();
        }

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_01_AnyInSelection()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Any() ? author.Friends.Count() : 0
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01_Where()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Where(a => a.AuthorFriendCount > 0)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02_AnyBeforeSelection()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.Friends.Any() && author.BookAuthors.Any(a => a.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Take(10)
                .ToList();
        }


        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_02_AnyInSelection()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Any() ? author.Friends.Count() : 0,
        //            AuthorAverageReviewRating = author.BookAuthors.Any(a => a.Book.BookReviews.Any()) ? author.BookAuthors
        //                .SelectMany(a => a.Book.BookReviews)
        //                .Average(br => br.Rating) : 0
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02_Where()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Where(rr => rr.AuthorFriendCount > 0 && rr.AuthorAverageReviewRating > 0)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03_AnyBeforeSelection()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.Friends.Any() && author.BookAuthors.Any(a => a.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_03_AnyInSelection()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Any() ? author.Friends.Count() : 0,
        //            AuthorAverageReviewRating = author.BookAuthors.Any(a => a.Book.BookReviews.Any()) ? author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating) : 0,
        //            AuthorBestBook = author.BookAuthors.Any() ? author.BookAuthors.Max(ba => ba.Book.AvarageRating) : 0
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03_Where()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Where(a => a.AuthorFriendCount > 0 && a.AuthorAverageReviewRating.HasValue)
                .Take(10)
                .ToList();
        }

    }
}
