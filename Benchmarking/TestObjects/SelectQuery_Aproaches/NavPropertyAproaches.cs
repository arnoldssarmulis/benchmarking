﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    [MemoryDiagnoser]
    public class NavPropertyAproaches : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_01_AFCAnyCheck()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()) && author.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_02_AFCWhereCheck()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
        //        })
        //        .Where(a => a.AuthorFriendCount > 0)
        //        .Take(10)
        //        .ToList();
        //}

        [Benchmark]
        public void Query_NavigationProperty_03_WithoutAFCCheck()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_04_InSelectCheck()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors.Any(author => author.Book.BookReviews.Any()) ? 
        //                author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating) : 0,
        //            AuthorBestBook = author.BookAuthors.Any() ? author.BookAuthors.Max(ba => ba.Book.AvarageRating) : 0
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        [Benchmark]
        public void Query_NavigationProperty_04_DefaultIfEmpty()
        {
            var auasdthorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Select(br => br.Rating)
                        .DefaultIfEmpty()
                        .Average(),
                    AuthorBestBook = author.BookAuthors
                        .Select(br => br.Book.AvarageRating)
                        .DefaultIfEmpty()
                        .Max()
                })
                .Where(a => a.AuthorFriendCount > 0 && a.AuthorAverageReviewRating > 0 && a.AuthorBestBook > 0)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_04_HasValueWithDefault()
        {
            var auasdthorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Select(br => br.Rating)
                        .DefaultIfEmpty()
                        .Average(),
                    AuthorBestBook = author.BookAuthors
                        .Select(br => br.Book.AvarageRating)
                        .DefaultIfEmpty()
                        .Max()
                })
                .Where(a => a.AuthorFriendCount > 0 && a.AuthorAverageReviewRating.HasValue && a.AuthorBestBook.HasValue)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_04_HasValue()
        {
            var auasdthorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Select(br => br.Rating)
                        .DefaultIfEmpty()
                        .Average(),
                    AuthorBestBook = author.BookAuthors
                        .Select(br => br.Book.AvarageRating)
                        .DefaultIfEmpty()
                        .Max()
                })
                .Where(a => a.AuthorFriendCount > 0 && a.AuthorAverageReviewRating.HasValue && a.AuthorBestBook.HasValue)
                .Take(10)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_05()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
        //        })
        //        .Select(r => new
        //        {
        //            AuthorFriendCount = r.AuthorFriendCount != null ? r.AuthorFriendCount : 0,
        //            AuthorAverageReviewRating = r.AuthorAverageReviewRating != null ? r.AuthorAverageReviewRating : 0,
        //            AuthorBestBook = r.AuthorBestBook != null ? r.AuthorBestBook : 0
        //        })
        //        .Take(10)
        //        .ToList();
        //}
        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_01()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()) && author.Friends.Any())
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(author => author.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_02()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(author => author.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_03()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Select(a => a.Rating)
        //                .DefaultIfEmpty()
        //                .Average(),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_00_With()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_00_Without()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //        })
        //        .Take(10)
        //        .ToList();
        //}
    }
}
