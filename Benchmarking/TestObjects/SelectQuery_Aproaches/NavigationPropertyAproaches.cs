﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using LibraryEntities;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class NavigationPropertyAproaches : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count,
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03_BeforeSelect()
        {
            var authorStatistics = _dbContext.Authors
                .Where(a => a.Friends.Count > 1 && a.BookAuthors.SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating) > 0)
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count,
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_04()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_04_BeforeSelect()
        {
            var authorStatistics = _dbContext.Authors
                .Where(a => a.BookAuthors.SelectMany(author => author.Book.BookReviews).Where(br => br.Rating > 0)
                        .Average(br => br.Rating) > 0 && a.Friends.Count() > 1)
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_05()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_05_BeforeSelect()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Max(ba => ba.Book.AvarageRating) > 0 && author.BookAuthors.SelectMany(ba => ba.Book.BookReviews)
                        .Count() > 0)
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Where(br => br.Rating > 0)
                        .Count()
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(100)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_01_InSelect()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating) > 1 ? 
        //                author.BookAuthors.Max(ba => ba.Book.AvarageRating) : 0,
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count() > 3 ?
        //                author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count() : 0
        //        })
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_01_MultiSelect()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .Select(author => new
        //        {
        //            author.AuthorFriendCount,
        //            author.AuthorAverageReviewRating,
        //            AuthorBestBook = author.AuthorBestBook > 1 ? author.AuthorBestBook : 0,
        //            AuthorTotalReviewCount = author.AuthorTotalReviewCount > 3 ? author.AuthorTotalReviewCount : 0,
        //        })
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_08()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_01()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_02()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count,
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .GroupBy(review => review.BookId)
        //                .Select(group => group.Average(review => review.Rating))
        //                .Average()
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_03()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count,
        //            AuthorAverageReviewRating = _dbContext.BookAuthors
        //                .Where(ba => ba.AuthorId == author.Id)
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Average(br => br.Rating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_04()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count,
        //            AuthorAverageReviewRating = _dbContext.BookAuthors
        //                .Where(ba => ba.AuthorId == author.Id)
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Average(br => br.Rating)
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_05()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count,
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .Join(
        //                    _dbContext.BookReviews,
        //                    ba => ba.BookId,
        //                    review => review.BookId,
        //                    (ba, review) => new
        //                    {
        //                        ba.AuthorId,
        //                        ReviewRating = review.Rating
        //                    })
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_06()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        //.Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Select(br => br.Rating)
        //                .DefaultIfEmpty()
        //                .Average()
        //        })
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_061()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Select(br => br.Rating)
        //                .Average()
        //        })
        //        .Take(10)
        //        .ToList();
        //}


        //[Benchmark]
        //public void Query_NavigationProperty_062()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Select(br => br.Rating)
        //                .Average()
        //        })
        //        .Take(10)
        //        .ToList();
        //}



        ////[Benchmark]
        ////public void Query_NavigationProperty_07()
        ////{
        ////    var authorStatistics = _dbContext.Authors
        ////        .Select(author => new
        ////        {
        ////            AuthorFriendCount = author.Friends.Count(),
        ////            AuthorAverageReviewRating = author.BookAuthors
        ////                .SelectMany(author => author.Book.BookReviews)
        ////                .Average(br => br.Rating),
        ////            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        ////            AuthorTotalReviewCount = author.BookAuthors
        ////                .SelectMany(ba => ba.Book.BookReviews)
        ////                .Count()
        ////        })
        ////        .Select(author => new
        ////        {
        ////            author.AuthorFriendCount,
        ////            AuthorAverageReviewRating = author.AuthorAverageReviewRating == null ? 0 : author.AuthorAverageReviewRating,
        ////            AuthorBestBook = author.AuthorBestBook == null ? 0 : author.AuthorBestBook,
        ////            AuthorTotalReviewCount = author.AuthorTotalReviewCount == null ? 0 : author.AuthorTotalReviewCount
        ////        })
        ////        .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
        ////        .OrderByDescending(a => a.AuthorFriendCount)
        ////        .ThenByDescending(a => a.AuthorAverageReviewRating)
        ////        .Take(100)
        ////        .ToList();
        ////}

        //[Benchmark]
        //public void Query_NavigationProperty_08()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}
    }
}
