﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class NavigationPropertyHybridAproach : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_CombinedAproaches_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Join(
                    _dbContext.BookAuthors.SelectMany(ba => ba.Book.BookReviews, (ba, review) => new
                    {
                        ba.AuthorId,
                        review.Rating
                    })
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating)
                    }),
                    author => author.AuthorId,
                    author_br => author_br.AuthorId,
                    (author, author_br) => new
                    {
                        author.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_CombinedAproaches_02_WithGroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.AuthorFriendCount
                        })
                .GroupJoin(
                    _dbContext.BookAuthors
                    .SelectMany(ba => ba.Book.BookReviews, (ba, review) => new
                    {
                        ba.AuthorId,
                        review.Rating
                    }),
                    author => author.AuthorId,
                    author_br => author_br.AuthorId,
                    (author, author_br) => new
                    {
                        AuthorFriendCount = author.AuthorFriendCount == null ? author.AuthorFriendCount : 0,
                        AuthorAverageReviewRating = author_br.Any() ? author_br.Average(br => br.Rating) : 0
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }


    }
}
