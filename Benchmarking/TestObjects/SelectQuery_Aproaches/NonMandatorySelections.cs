﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarking.TestObjects.SelectQuery_Aproaches
{
    internal class NonMandatorySelections : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    Id = author.Id,
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderBy(a => a.Id)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.AuthorFriendCount
                        })
                    .GroupJoin(
                        _dbContext.BookAuthors
                            .Join(
                                _dbContext.BookReviews.Where(br => br.Rating > 0),
                                ba => ba.BookId,
                                review => review.BookId,
                                (ba, review) => new
                                {
                                    ba.AuthorId,
                                    review.Rating
                                }),
                        author => author.AuthorId,
                        authorReviews => authorReviews.AuthorId,
                        (author, authorReviews) => new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorReviewRatings = authorReviews.Select(ar => ar.Rating)
                        })
                    .Select(author =>
                        new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorAverageReviewRating = author.AuthorReviewRatings.Any() ?
                                author.AuthorReviewRatings.Average() : 0
                        })
                    .OrderBy(a => a.AuthorId)
                    .Take(10)
                    .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Where(rr => rr.AuthorAverageReviewRating.HasValue && rr.AuthorFriendCount > 0)
                .Take(10)
                .ToList();
        }

        /// <summary>
        ///// Selects AuthorFriendCount for every author using navigation property.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_02()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating)
        //        })
        //        .Where(rr => rr.AuthorAverageReviewRating.HasValue && rr.AuthorFriendCount > 0)
        //        .Take(10)
        //        .ToList();
        //}
    }
}
