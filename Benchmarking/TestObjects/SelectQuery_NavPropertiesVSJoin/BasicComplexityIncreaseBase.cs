﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class BasicComplexityIncreaseBase : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Where(a => a.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList,
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        friendList.AuthorId,
                        friendList.FriendsWithAuthorId
                    })
                .GroupBy(a => a.AuthorId)
                .Select(g => new
                {
                    AuthorFriendCount = g.Count()
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any() && author.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .Average(br => br.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_02()
        {
            var authorStatistics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author => author.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author, bookAuthor) => new
                        {
                            author.AuthorId,
                            author.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.Books,
                        authorBook => authorBook.BookId,
                        book => book.Id,
                        (authorBook, book) => new
                        {
                            authorBook.AuthorId,
                            authorBook.AuthorFriendCount,
                            book.AvarageRating
                        })
                    .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
                    .Select(a => new
                    {
                        AuthorFriendCount = a.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = a.Average(ab => ab.AvarageRating)
                    })
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()) && author.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count,
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Take(10)
                .ToList();
        }


        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_03()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author => author.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author, bookAuthor) => new
                    {
                        author.AuthorId,
                        bookAuthor.BookId,
                        author.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating,
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    AuthorFriendCount = g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .Take(10)
                .ToList();
        }

        [Benchmark(Baseline = true)]
        public void Query_03_Baseline()
        {
            var results = _dbAccess.ExecuteSQLToDB<DBAccess.Models.AuthorFriendsAndBookRatingBase>();
        }

    }
}
