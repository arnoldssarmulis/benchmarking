﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class BasicComplexityIncreaseWithAddons : GlobalAccessSetup
    {
        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .Where(b => b.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList,
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        friendList.AuthorId,
                        friendList.FriendsWithAuthorId
                    })
                .GroupBy(a => a.AuthorId)
                .Select(g => new
                {
                    AuthorFriendCount = g.Count()
                })
                .Where(b => b.AuthorFriendCount > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .Average(br => br.Book.AvarageRating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageBookRating for every author using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_02()
        {
            var authorStatistics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author_fl => author_fl.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author_fl, bookAuthor) => new
                        {
                            AuthorId = author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.Books,
                        authorBook => authorBook.BookId,
                        book => book.Id,
                        (authorBook, bookAuthor) => new
                        {
                            authorBook.AuthorId,
                            authorBook.AuthorFriendCount,
                            AvarageRating = bookAuthor.AvarageRating
                        })
                    .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
                    .Select(a => new
                    {
                        AuthorFriendCount = a.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = a.Average(ab => ab.AvarageRating)
                    })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using navigation properties.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count,
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Selects AuthorFriendCount and AuthorAverageReviewRating for every author with BookAuthors and BookReviews using join.
        /// </summary>
        [Benchmark]
        public void Query_Join_03()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .Where(b => b.AuthorFriendCount > 1 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark(Baseline = true)]
        public void Query_03_Baseline()
        {
            var results = _dbAccess.ExecuteSQLToDB<DBAccess.Models.AuthorFriendsAndBookRating>();
        }

    }
}
