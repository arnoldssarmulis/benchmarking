﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class EqualComplexity : GlobalAccessSetup
    {
        //[Benchmark]
        //public void Query_Join_02_01()
        //{
        //    var authorStatistics = _dbContext.Authors
        //            .Join(
        //                _dbContext.AuthorFriendList
        //                .GroupBy(a => a.AuthorId)
        //                .Select(g => new
        //                {
        //                    AuthorId = g.Key,
        //                    AuthorFriendCount = g.Count()
        //                }),
        //                author => author.Id,
        //                friendList => friendList.AuthorId,
        //                (author, friendList) => new
        //                {
        //                    AuthorId = author.Id,
        //                    friendList.AuthorFriendCount
        //                })
        //            .Join(
        //                _dbContext.BookAuthors,
        //                author_fl => author_fl.AuthorId,
        //                bookAuthor => bookAuthor.AuthorId,
        //                (author_fl, bookAuthor) => new
        //                {
        //                    author_fl.AuthorId,
        //                    author_fl.AuthorFriendCount,
        //                    bookAuthor.BookId
        //                })
        //            .Join(
        //                _dbContext.Books,
        //                authorBook => authorBook.BookId,
        //                book => book.Id,
        //                (authorBook, bookAuthor) => new
        //                {
        //                    authorBook.AuthorId,
        //                    authorBook.AuthorFriendCount,
        //                    AvarageRating = (int)bookAuthor.AvarageRating
        //                })

        //            .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
        //            .Select(a => new
        //            {
        //                a.Key.AuthorFriendCount,
        //                AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //            })
        //        //.Where(b => b.AuthorFriendCount > 0 && b.AuthorAverageBookRating > 0)
        //        .Where(b => b.AuthorFriendCount > 0)
        //        .OrderByDescending(b => b.AuthorFriendCount)
        //        //.ThenByDescending(b => b.AuthorAverageBookRating)
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_Join_02_0111()
        //{
        //    var authorStatistics = _dbContext.Authors
        //            .Join(
        //                _dbContext.BookAuthors,
        //                author_fl => author_fl.Id,
        //                bookAuthor => bookAuthor.AuthorId,
        //                (author_fl, bookAuthor) => new
        //                {
        //                    AuthorId = author_fl.Id,
        //                    bookAuthor.BookId
        //                })
        //            .Join(
        //                _dbContext.Books,
        //                authorBook => authorBook.BookId,
        //                book => book.Id,
        //                (authorBook, bookAuthor) => new
        //                {
        //                    authorBook.AuthorId,
        //                    AvarageRating = bookAuthor.AvarageRating
        //                })

        //            .GroupBy(a => new { a.AuthorId })
        //            .Select(a => new
        //            {
        //                AuthorFriendCount = _dbContext.AuthorFriendList
        //                    .Where(afl => afl.AuthorId == a.Key.AuthorId)
        //                    .Count(),
        //                AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //            })
        //        .Where(b => b.AuthorFriendCount > 0 && b.AuthorAverageBookRating > 0)
        //        .OrderByDescending(b => b.AuthorFriendCount)
        //        .ThenByDescending(b => b.AuthorAverageBookRating)
        //        .Take(10)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_Join_02_02()
        //{
        //    var authorStatistics = _dbContext.Authors
        //            .Join(
        //                _dbContext.AuthorFriendList
        //                .GroupBy(a => a.AuthorId)
        //                .Select(g => new
        //                {
        //                    AuthorId = g.Key,
        //                    AuthorFriendCount = g.Count()
        //                }),
        //                author => author.Id,
        //                friendList => friendList.AuthorId,
        //                (author, friendList) => new
        //                {
        //                    AuthorId = author.Id,
        //                    friendList.AuthorFriendCount
        //                })
        //            .Join(
        //                _dbContext.BookAuthors,
        //                author_fl => author_fl.AuthorId,
        //                bookAuthor => bookAuthor.AuthorId,
        //                (author_fl, bookAuthor) => new
        //                {
        //                    author_fl.AuthorId,
        //                    author_fl.AuthorFriendCount,
        //                    bookAuthor.BookId
        //                })
        //            .Join(
        //                _dbContext.Books,
        //                authorBook => authorBook.BookId,
        //                book => book.Id,
        //                (authorBook, bookAuthor) => new
        //                {
        //                    authorBook.AuthorId,
        //                    authorBook.AuthorFriendCount,
        //                    bookAuthor.AvarageRating
        //                })
        //            .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
        //            .Select(a => new
        //            {
        //                a.Key.AuthorFriendCount,
        //                AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
        //            })
        //        .Take(10)
        //        .ToList();
        //}

        ///// <summary>
        ///// Selects AuthorFriendCount and AuthorAverageBookRating for every author using navigation properties.
        ///// </summary>
        //[Benchmark]
        //public void Query_NavigationProperty_02()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any())
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageBookRating = author.BookAuthors
        //                .Average(br => br.Book.AvarageRating)
        //        })
        //        .Where(b => b.AuthorFriendCount > 0 && b.AuthorAverageBookRating > 0)
        //        //.Where(b => b.AuthorFriendCount > 0)
        //        .OrderByDescending(b => b.AuthorFriendCount)
        //        .ThenByDescending(b => b.AuthorAverageBookRating)
        //        .Take(10)
        //        .ToList();
        //}

        [Benchmark]
        public void Query_Join_01()
        {
            var asdnasdn = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = _dbContext.AuthorFriendList
                        .Where(afl => afl.AuthorId == author.Id)
                        .Count(),
                    AuthorAverageReviewRating = _dbContext.BookAuthors
                        .Where(a => a.AuthorId == author.Id)
                        .GroupBy(a => a.AuthorId)
                        .Join(
                            _dbContext.BookReviews,
                            ba => ba.Key,
                            review => review.BookId,
                            (ba, review) => new
                            {
                                AuthorId = ba.Key,
                                ReviewRating = review.Rating
                            })
                        .GroupBy(x => x.AuthorId)
                        .Select(g => new
                        {
                            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                        })
                })
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var asdasd = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count,
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Select(br => br.Rating)
                        .DefaultIfEmpty()
                        .Average()
                })
                .Take(10)
                .ToList();
        }


        /// <summary>
        /// Selects AuthorFriendCount for every author using navigation property.
        /// </summary>
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .Take(10)
                .ToList();
        }

        [Benchmark(Baseline = true)]
        public void Query_Baseline()
        {
            var results = _dbAccess.ExecuteSQLToDB<DBAccess.Models.AvarageComplexityWithAddonsBase>();
        }
    }
}
