﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class MaxComplexityBase : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()) && author.Friends.Any())
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Take(100)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_01_WithInclude()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Include(author => author.Friends)
        //        .Include(author => author.BookAuthors)
        //            .ThenInclude(ba => ba.Book)
        //                .ThenInclude(book => book.BookReviews)
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .Take(100)
        //        .ToList();
        //}

        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.Rating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating),
                        AuthorTotalReviewCount = b.Count()
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating,
                        author_br.AuthorTotalReviewCount
                    })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.Books,
                        book => book.BookId,
                        review => review.Id,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.AvarageRating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorBestBook = b.Max(c => c.AvarageRating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        author_fl.AuthorAverageReviewRating,
                        author_br.AuthorBestBook,
                        author_fl.AuthorTotalReviewCount
                    })
                    .Take(100)
                    .ToList();
        }

        [Benchmark(Baseline = true)]
        public void Query_Baseline()
        {
            var results = _dbAccess.ExecuteSQLToDB<DBAccess.Models.MaxComplexityWithAddonsBase>();
        }


    }
}
