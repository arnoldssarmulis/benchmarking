﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoin
{
    [MemoryDiagnoser]
    public class MissuseComplexity : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.BookAuthors
                .Where(author => author.Book.BookReviews.Any())
                .GroupBy(bookAuthor => bookAuthor.AuthorId)
                .Select(group => new
                {
                    AuthorFriendCount = group.First().Author.Friends.Count,
                    AuthorAverageReviewRating = group
                        .SelectMany(bookAuthor => bookAuthor.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(author => author.AuthorFriendCount)
                .ThenByDescending(author => author.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_CombinedAproaches_02_WithGroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.AuthorFriendCount
                        })
                .GroupJoin(
                    _dbContext.BookAuthors
                    .SelectMany(ba => ba.Book.BookReviews, (ba, review) => new
                    {
                        ba.AuthorId,
                        review.Rating
                    }),
                    author => author.AuthorId,
                    author_br => author_br.AuthorId,
                    (author, author_br) => new
                    {
                        AuthorFriendCount = author.AuthorFriendCount == null ? author.AuthorFriendCount : 0,
                        AuthorAverageReviewRating = author_br.Any() ? author_br.Average(br => br.Rating) : 0
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_WithGroupJoin()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .GroupJoin(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.Rating
                        }
                    ),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        AuthorAverageReviewRating = author_br.Any() ? author_br.Average(br => br.Rating) : 0
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }
    }
}
