﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_Basic : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_Join_01()
        {
            var authorReviews = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_02()
        {
            var authorReviews = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_03()
        {
            var authorReviews = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author,
                          Review = review
                      })
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorReviews = _dbContext.Authors
                .SelectMany(author => author.BookAuthors,
                    (author, bookAuthor) => new
                    {
                        author,
                        bookAuthor
                    })
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorReviews = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.Select(ba => ba.Book),
                    (author, book) => new
                    {
                        author,
                        book
                    })
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorReviews = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.SelectMany(ba => ba.Book.BookReviews),
                    (author, bookReview) => new
                    {
                        author,
                        bookReview
                    })
                .Take(1000)
                .ToList();
        }
    }
}
