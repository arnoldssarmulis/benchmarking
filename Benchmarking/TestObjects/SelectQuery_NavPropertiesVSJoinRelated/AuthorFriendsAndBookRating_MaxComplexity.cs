﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_MaxComplexity : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_00()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 1 && a.AuthorBestBook > 1)
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void _Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author_fl => author_fl.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author_fl, bookAuthor) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.Books,
                        authorBook => authorBook.BookId,
                        book => book.Id,
                        (authorBook, bookAuthor) => new
                        {
                            authorBook.AuthorId,
                            authorBook.AuthorFriendCount,
                            authorBook.BookId,
                            bookAuthor.AvarageRating
                        })
                    .Join(
                        _dbContext.BookReviews,
                        ba => ba.BookId,
                        review => review.BookId,
                        (ba, review) => new
                        {
                            ba.AuthorId,
                            ba.AuthorFriendCount,
                            ReviewRating = review.Rating,
                            BookAvgRating = ba.AvarageRating
                        })
                    .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                    .Select(g => new
                    {
                        g.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = g.Average(c => c.ReviewRating),
                        AuthorTotalReviewCount = g.Count(),
                        AuthorBestBook = g.Max(c => c.BookAvgRating)
                    })
                    .Where(a => a.AuthorTotalReviewCount > 1 && a.AuthorBestBook > 1)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .Take(100)
                    .ToList();
        }

        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            ReviewRating = review.Rating,
                            BookAvgRating = book.Book.AvarageRating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.ReviewRating),
                        AuthorTotalReviewCount = b.Count(),
                        AuthorBestBook = b.Max(c => c.BookAvgRating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating,
                        author_br.AuthorBestBook,
                        author_br.AuthorTotalReviewCount
                    })
                .Where(a => a.AuthorTotalReviewCount > 1 && a.AuthorBestBook > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }
    }
}
