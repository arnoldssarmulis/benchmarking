﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_OriginalFast : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_CombinedAproaches_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Join(
                    _dbContext.BookAuthors.SelectMany(ba => ba.Book.BookReviews, (ba, review) => new
                    {
                        ba.AuthorId,
                        review.Rating
                    })
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating)
                    }),
                    author => author.AuthorId,
                    author_br => author_br.AuthorId,
                    (author, author_br) => new
                    {
                        author.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_01()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_02()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Join(
                    _dbContext.BookAuthors.Join(
                        _dbContext.BookReviews,
                        book => book.BookId,
                        review => review.BookId,
                        (book, review) => new
                        {
                            book.AuthorId,
                            review.Rating
                        }
                    )
                    .GroupBy(b => b.AuthorId)
                    .Select(b => new
                    {
                        AuthorId = b.Key,
                        AuthorAverageReviewRating = b.Average(c => c.Rating)
                    }),
                    author_fl => author_fl.AuthorId,
                    author_br => author_br.AuthorId,
                    (author_fl, author_br) => new
                    {
                        author_fl.AuthorFriendCount,
                        author_br.AuthorAverageReviewRating
                    })
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_03()
        {
            var query = _dbContext.Authors
                .Join(_dbContext.BookAuthors, a => a.Id, ba => ba.AuthorId, (a, ba) => new { Author = a, BookAuthor = ba })
                .Join(_dbContext.BookReviews, ab => ab.BookAuthor.BookId, br => br.BookId, (ab, br) => new { ab.Author, br.Rating })
                .GroupBy(abr => abr.Author.Id)
                .Select(g => new
                {
                    AuthorId = g.Key,
                    AuthorAverageReviewRating = g.Average(x => x.Rating)
                })
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(af => af.AuthorId)
                    .Select(g => new { AuthorId = g.Key, AuthorFriendCount = g.Count() }),
                    avg_r => avg_r.AuthorId,
                    afl => afl.AuthorId,
                    (avg_r, afl) => new
                    {
                        avg_r.AuthorAverageReviewRating,
                        afl.AuthorFriendCount
                    })
                .OrderByDescending(x => x.AuthorFriendCount)
                .ThenByDescending(x => x.AuthorAverageReviewRating)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_04()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.Books,
                    authorBook => authorBook.BookId,
                    book => book.Id,
                    (authorBook, bookAuthor) => new
                    {
                        authorBook.AuthorId,
                        authorBook.AuthorFriendCount,
                        authorBook.BookId,
                        bookAuthor.AvarageRating
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating,
                        ba.AvarageRating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g
                        .Where(a => a.ReviewRating > 0)
                        .Average(c => c.ReviewRating),
                    AuthorBestBook = g.Max(c => c.AvarageRating)
                })
                .Where(a => a.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_05()
        {
            var authorStatistics = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.Books,
                    authorBook => authorBook.BookId,
                    book => book.Id,
                    (authorBook, bookAuthor) => new
                    {
                        authorBook.AuthorId,
                        authorBook.AuthorFriendCount,
                        authorBook.BookId,
                        bookAuthor.AvarageRating
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating,
                        BookAvgRating = ba.AvarageRating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Where(br => br.ReviewRating > 0)
                        .Average(c => c.ReviewRating),
                    AuthorBestBook = g.Max(c => c.BookAvgRating),
                    AuthorTotalReviewCount = g.Where(br => br.ReviewRating > 0)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 1 && a.AuthorBestBook > 1)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .ThenByDescending(b => b.AuthorBestBook)
                .Take(100)
                .ToList();
        }

        [Benchmark(Baseline = true)]
        public void Query_Baseline()
        {
            var results = _dbAccess.ExecuteSQLToDB<DBAccess.Models.AuthorFriendsAndBookRating>();
        }
    }
}
