﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_Parts : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_01()
        {
            var authorReviews = _dbContext.Authors
                .Select(author => new
                {
                    AuthorId = author.Id,
                    AuthorBookReviews = author.BookAuthors.SelectMany(bookAuthor => bookAuthor.Book.BookReviews)
                })
                .Where(br => br.AuthorBookReviews.Any())
                .OrderBy(a => a.AuthorId)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_02()
        {
            var authorReviews = _dbContext.Authors
                //.Include(author => author.BookAuthors)
                //    .ThenInclude(bookAuthor => bookAuthor.Book)
                //        .ThenInclude(book => book.BookReviews)
                .Select(author => new
                {
                    AuthorId = author.Id,
                    AuthorBookReviews = author.BookAuthors.SelectMany(bookAuthor => bookAuthor.Book.BookReviews)
                })
                .Where(br => br.AuthorBookReviews.Any())
                .OrderBy(a => a.AuthorId)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_03()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor })
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author.Id,
                          Review = review
                      })
                .GroupBy(temp => temp.Id)
                .Select(group => new
                {
                    AuthorId = group.Key,
                    AuthorBookReviews = group.Select(item => item.Review)
                })
                .OrderBy(a => a.AuthorId)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_04()
        {
            var authorReviews = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorId = author.Id,
                    AuthorAverageReviewRating =
                        author.BookAuthors.SelectMany(bookAuthor => bookAuthor.Book.BookReviews)
                        .DefaultIfEmpty()
                        .Average(br => br.Rating)
                })
                .OrderBy(a => a.AuthorId)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_05()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor })
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author.Id,
                          Review = review
                      })
                .GroupBy(temp => temp.Id)
                .Select(group => new
                {
                    AuthorId = group.Key,
                    AuthorAverageReviewRating = group.Select(item => item.Review).Average(br => br.Rating)
                })
                .OrderBy(a => a.AuthorId)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_06()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count()
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_07()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Select(author => new
                {
                    author.AuthorFriendCount
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_08()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_09()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count(),
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .Select(author => new
                {
                    author.AuthorId,
                    author.AuthorFriendCount
                })
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }

        [Benchmark]
        public void Query_10()
        {
            var authorStatistics = _dbContext.Authors
                .Join(_dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count(),
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                .OrderByDescending(b => b.AuthorFriendCount)
                .Take(10)
                .ToList();
        }
    }
}
