﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_Parts_NavProperties : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        //[Benchmark]
        //public void Query_NavigationProperty_03()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_04()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any())) //Vajadzīgs lai nenoķertu nullable object has exception!
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .Average(br => br.Rating),
        //            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            AuthorTotalReviewCount = author.BookAuthors
        //                .SelectMany(ba => ba.Book.BookReviews)
        //                .Count()
        //        })
        //        .Where(a => a.AuthorTotalReviewCount > 100 && a.AuthorBestBook > 2)
        //        .OrderByDescending(a => a.AuthorFriendCount)
        //        .ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        //[Benchmark]
        //public void Query_NavigationProperty_05()
        //{
        //    var authorStatistics = _dbContext.Authors
        //        .Select(author => new
        //        {
        //            AuthorFriendCount = author.Friends.Count(),
        //            AuthorAverageReviewRating = author.BookAuthors
        //                .SelectMany(author => author.Book.BookReviews)
        //                .DefaultIfEmpty(new LibraryEntities.Entities.BookReview { })
        //                .Average(br => br.Rating)//,
        //            //AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
        //            //AuthorTotalReviewCount = author.BookAuthors
        //            //    .SelectMany(ba => ba.Book.BookReviews)
        //            //    .Count()
        //        })
        //        //.Where(a => a.AuthorTotalReviewCount > 100 && a.AuthorBestBook > 2)
        //        //.OrderByDescending(a => a.AuthorFriendCount)
        //        //.ThenByDescending(a => a.AuthorAverageReviewRating)
        //        .Take(100)
        //        .ToList();
        //}

        [Benchmark]
        public void Query_NavigationProperty_06()
        {
            var authorStatistics = _dbContext.Authors
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Select(author => new
                {
                    author.AuthorFriendCount,
                    AuthorAverageReviewRating = author.AuthorAverageReviewRating == null ? 0 : author.AuthorAverageReviewRating,
                    AuthorBestBook = author.AuthorBestBook == null ? 0 : author.AuthorBestBook,
                    AuthorTotalReviewCount = author.AuthorTotalReviewCount == null ? 0 : author.AuthorTotalReviewCount
                })
                .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_07()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorTotalReviewCount = author.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews)
                        .Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_08()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating),
                    AuthorBooks = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                })
                .Select(author => new
                {
                    author.AuthorFriendCount,
                    AuthorAverageReviewRating = author.AuthorBooks.Average(br => br.Rating),
                    author.AuthorBestBook,
                    AuthorTotalReviewCount = author.AuthorBooks.Count()
                })
                .Where(a => a.AuthorTotalReviewCount > 3 && a.AuthorBestBook > 1)
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_09()
        {
            var authorStatistics = _dbContext.Authors
                .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                .Select(author => new
                {
                    AuthorFriendCount = author.Friends.Count(),
                    AuthorAverageReviewRating = author.BookAuthors
                        .SelectMany(author => author.Book.BookReviews)
                        .Average(br => br.Rating)
                })
                .OrderByDescending(a => a.AuthorFriendCount)
                .ThenByDescending(a => a.AuthorAverageReviewRating)
                .Take(100)
                .ToList();
        }
    }
}
