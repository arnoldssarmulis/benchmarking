﻿using BenchmarkDotNet.Attributes;
using Benchmarking.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Benchmarking.TestObjects.SelectQuery_NavPropertiesVSJoinRelated
{
    [MemoryDiagnoser]
    public class AuthorFriendsAndBookRating_Parts_OrderBy : GlobalAccessSetup
    {
        [Benchmark]
        public void Query_Join_01()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author,
                          Review = review
                      })
                .OrderByDescending(author => author.author.Id)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_01()
        {
            var authorReviewss = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.SelectMany(ba => ba.Book.BookReviews),
                    (author, bookReview) => new
                    {
                        author,
                        bookReview
                    })
                .OrderByDescending(author => author.author.Id)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_02()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author,
                          Review = review
                      })
                .OrderByDescending(author => author.author.Name)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_02()
        {
            var authorReviewss = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.SelectMany(ba => ba.Book.BookReviews),
                    (author, bookReview) => new
                    {
                        author,
                        bookReview
                    })
                .OrderByDescending(author => author.author.Name)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_03()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author,
                          Review = review
                      })
                .OrderByDescending(author => author.author.Name)
                .ThenByDescending(author => author.Review.Rating)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_03()
        {
            var authorReviewss = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.SelectMany(ba => ba.Book.BookReviews),
                    (author, bookReview) => new
                    {
                        author,
                        bookReview
                    })
                .OrderByDescending(author => author.author.Name)
                .ThenByDescending(author => author.bookReview.Rating)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_Join_04()
        {
            var authorReviewss = _dbContext.Authors
                .Join(_dbContext.BookAuthors,
                      author => author.Id,
                      bookAuthor => bookAuthor.AuthorId,
                      (author, bookAuthor) => new { author, bookAuthor }
                )
                .Join(_dbContext.Books,
                      temp => temp.bookAuthor.BookId,
                      book => book.Id,
                      (temp, book) => new { temp.author, book })
                .Join(_dbContext.BookReviews,
                      temp => temp.book.Id,
                      review => review.BookId,
                      (temp, review) => new
                      {
                          temp.author,
                          Review = review
                      })
                .OrderByDescending(author => author.author.Name)
                .ThenByDescending(author => author.Review.Rating)
                .ThenByDescending(author => author.Review.Timestamp)
                .Take(1000)
                .ToList();
        }

        [Benchmark]
        public void Query_NavigationProperty_04()
        {
            var authorReviewss = _dbContext.Authors
                .SelectMany(author => author.BookAuthors.SelectMany(ba => ba.Book.BookReviews),
                    (author, bookReview) => new
                    {
                        author,
                        bookReview
                    })
                .OrderByDescending(author => author.author.Name)
                .ThenByDescending(author => author.bookReview.Rating)
                .ThenByDescending(author => author.bookReview.Timestamp)
                .Take(1000)
                .ToList();
        }
    }
}
