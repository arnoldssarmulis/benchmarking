﻿using DBAccess.Models;
using System.Data;
using DBAccess.DatabaseProviders;
using EFCoreDBContext.Config;

namespace DBAccess
{
    public class DataAccessLayer
    {
        private readonly IDatabaseProvider databaseProvider;

        public DataAccessLayer()
        {
            databaseProvider = ResolveDBProvider();
        }

        public List<T> ExecuteSQLToDB<T>(IDbDataParameter[]? parameters = null)
            where T : WithSQLObject<T>, IMapable<T>, new()
        {
            List<T> resultList = new();
            T objInstance = new();

            string SQLObjectName = objInstance.SQLObjectName;

            using (IDbConnection connection = databaseProvider.CreateConnection())
            {
                using (IDbCommand command = databaseProvider.CreateCommand(SQLObjectName, connection))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null && parameters.Length > 0)
                    {
                        foreach (var param in parameters)
                        {
                            command.Parameters.Add(param);
                        }
                    }

                    connection.Open();

                    using (IDataReader reader = databaseProvider.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            T resultItem = objInstance.MapObject(reader);

                            resultList.Add(resultItem);
                        }
                    }
                }
            }

            return resultList;
        }

        private IDatabaseProvider ResolveDBProvider()
        {
            switch (DBProvider.ProviderType)
            {
                case DatabaseProviderType.MSSQLServer:
                    return new MSSQLServer();

                case DatabaseProviderType.PostgreSQL:
                    return new PostgreSQL();

                case DatabaseProviderType.MySQL:
                    return new MySQL();

                default:
                    throw new NotSupportedException();
            }
        }
    }
}
