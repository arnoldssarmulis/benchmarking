﻿using System.Data;

namespace DBAccess.DatabaseProviders
{
    public interface IDatabaseProvider
    {
        IDbConnection CreateConnection();

        IDbCommand CreateCommand(string commandText, IDbConnection connection);

        IDbDataParameter CreateParameter(string parameterName, object value);

        void OpenConnection(IDbConnection connection);

        void CloseConnection(IDbConnection connection);

        IDataReader ExecuteReader(IDbCommand command);
    }
}
