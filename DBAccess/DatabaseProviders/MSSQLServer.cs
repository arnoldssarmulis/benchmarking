﻿using DBAccess.Helpers;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DBAccess.DatabaseProviders
{
    public class MSSQLServer : WithConnectionString, IDatabaseProvider
    {
        public IDbConnection CreateConnection()
        {
            return new SqlConnection(connectionString);
        }

        public IDbCommand CreateCommand(string commandText, IDbConnection connection)
        {
            return new SqlCommand(Query.SelectAll(commandText), (SqlConnection)connection);
        }

        public IDbDataParameter CreateParameter(string parameterName, object value)
        {
            return new SqlParameter(parameterName, value);
        }

        public void OpenConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        public void CloseConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }
        }

        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader();
        }
    }
}
