﻿using DBAccess.Helpers;
using MySql.Data.MySqlClient;
using System.Data;

namespace DBAccess.DatabaseProviders
{
    public class MySQL : WithConnectionString, IDatabaseProvider
    {
        public IDbConnection CreateConnection()
        {
            return new MySqlConnection(connectionString);
        }

        public IDbCommand CreateCommand(string commandText, IDbConnection connection)
        {
            return new MySqlCommand(Query.SelectAll(commandText), (MySqlConnection)connection);
        }

        public IDbDataParameter CreateParameter(string parameterName, object value)
        {
            return new MySqlParameter(parameterName, value);
        }

        public void OpenConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        public void CloseConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }
        }

        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader();
        }
    }
}
