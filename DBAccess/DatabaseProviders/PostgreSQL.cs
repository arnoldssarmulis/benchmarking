﻿using System.Data;
using DBAccess.Helpers;
using Npgsql;

namespace DBAccess.DatabaseProviders
{
    public class PostgreSQL : WithConnectionString, IDatabaseProvider
    {
        public IDbConnection CreateConnection()
        {
            return new NpgsqlConnection(connectionString);
        }

        public IDbCommand CreateCommand(string commandText, IDbConnection connection)
        {
            commandText = "public.\"" + commandText + "\"";

            return new NpgsqlCommand(Query.SelectAll(commandText), (NpgsqlConnection)connection);
        }

        public IDbDataParameter CreateParameter(string parameterName, object value)
        {
            return new NpgsqlParameter(parameterName, value);
        }

        public void OpenConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        public void CloseConnection(IDbConnection connection)
        {
            if (connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }
        }

        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader();
        }
    }

}
