﻿using EFCoreDBContext.Config;
using Microsoft.Extensions.Configuration;

namespace DBAccess.DatabaseProviders
{
    public class WithConnectionString
    {
        protected string connectionString;

        public WithConnectionString()
        {
            var config = Configuration.GetConfiguration;
            connectionString = config.GetConnectionString(DBProvider.ConnectionStringName) 
                ?? throw new ArgumentNullException(nameof(connectionString));
        }
    }
}
