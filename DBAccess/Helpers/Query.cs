﻿namespace DBAccess.Helpers
{
    internal static class Query
    {
        public static string SelectAll(string tableOrView)
        {
            return $"SELECT * FROM {tableOrView}";
        }
    }
}
