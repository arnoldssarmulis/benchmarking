﻿using System.Data;

namespace DBAccess.Models
{
    public class AuthorFriendsAndBookRating : WithSQLObject<AuthorFriendsAndBookRating>, IMapable<AuthorFriendsAndBookRating>
    {
        public int AuthorId { get; set; }

        public string AuthorName { get; set; }

        public double? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public AuthorFriendsAndBookRating MapObject(IDataReader reader)
        {
            return new AuthorFriendsAndBookRating
            {
                //AuthorId = Convert.ToInt32(reader["AuthorId"]),
                //AuthorName = reader["AuthorName"].ToString(),
                AuthorFriendCount = Convert.ToDouble(reader["AuthorFriendCount"]),
                AuthorAverageReviewRating =  Convert.ToDouble(reader["AuthorAverageReviewRating"])
            };
        }
    }
}
