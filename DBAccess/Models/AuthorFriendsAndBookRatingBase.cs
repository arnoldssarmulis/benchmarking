﻿using System.Data;

namespace DBAccess.Models
{
    public class AuthorFriendsAndBookRatingBase : WithSQLObject<AuthorFriendsAndBookRatingBase>, IMapable<AuthorFriendsAndBookRatingBase>
    {
        public double? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public AuthorFriendsAndBookRatingBase MapObject(IDataReader reader)
        {
            return new AuthorFriendsAndBookRatingBase
            {
                AuthorFriendCount = reader["AuthorFriendCount"] is not DBNull ? Convert.ToDouble(reader["AuthorFriendCount"]) : null,
                AuthorAverageReviewRating = reader["AuthorAverageReviewRating"] is not DBNull ? Convert.ToDouble(reader["AuthorAverageReviewRating"]) : null
            };
        }
    }
}
