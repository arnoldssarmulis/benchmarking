﻿using Microsoft.Extensions.Logging.Abstractions;
using System.Data;

namespace DBAccess.Models
{
    public class AvarageComplexityWithAddons : WithSQLObject<AvarageComplexityWithAddons>, IMapable<AvarageComplexityWithAddons>
    {
        public int? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public double? AuthorBestBook { get; set; }

        public AvarageComplexityWithAddons MapObject(IDataReader reader)
        {
            return new AvarageComplexityWithAddons
            {
                AuthorFriendCount =  Convert.ToInt32(reader["AuthorFriendCount"]),
                AuthorAverageReviewRating =  Convert.ToDouble(reader["AuthorAverageReviewRating"]),
                AuthorBestBook = Convert.ToDouble(reader["AuthorBestBook"])
            };
        }
    }
}
