﻿using System.Data;

namespace DBAccess.Models
{
    public class AvarageComplexityWithAddonsBase : WithSQLObject<AvarageComplexityWithAddonsBase>, IMapable<AvarageComplexityWithAddonsBase>
    {
        public int? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public double? AuthorBestBook { get; set; }

        public AvarageComplexityWithAddonsBase MapObject(IDataReader reader)
        {
            return new AvarageComplexityWithAddonsBase
            {
                AuthorFriendCount = reader["AuthorFriendCount"] is not DBNull ? Convert.ToInt32(reader["AuthorFriendCount"]) : null,
                AuthorAverageReviewRating = reader["AuthorAverageReviewRating"] is not DBNull ? Convert.ToDouble(reader["AuthorAverageReviewRating"]) : null,
                AuthorBestBook = reader["AuthorBestBook"] is not DBNull ? Convert.ToDouble(reader["AuthorBestBook"]) : null
            };
        }
    }
}
