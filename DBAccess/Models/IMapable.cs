﻿using System.Data;

namespace DBAccess.Models
{
    public interface IMapable<T>
    {
        T MapObject(IDataReader reader);
    }
}
