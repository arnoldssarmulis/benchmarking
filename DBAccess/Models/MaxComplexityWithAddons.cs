﻿using System.Data;

namespace DBAccess.Models
{
    public class MaxComplexityWithAddons : WithSQLObject<MaxComplexityWithAddons>, IMapable<MaxComplexityWithAddons>
    {
        public int? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public double? AuthorBestBook { get; set; }

        public int? AuthorTotalReviewCount { get; set; }

        public MaxComplexityWithAddons MapObject(IDataReader reader)
        {
            return new MaxComplexityWithAddons
            {
                AuthorFriendCount = Convert.ToInt32(reader["AuthorFriendCount"]),
                AuthorAverageReviewRating = Convert.ToDouble(reader["AuthorAverageReviewRating"]),
                AuthorBestBook = Convert.ToDouble(reader["AuthorBestBook"]),
                AuthorTotalReviewCount = Convert.ToInt32(reader["AuthorTotalReviewCount"])
            };
        }
    }
}
