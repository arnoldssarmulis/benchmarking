﻿using System.Data;

namespace DBAccess.Models
{
    public class MaxComplexityWithAddonsBase : WithSQLObject<MaxComplexityWithAddonsBase>, IMapable<MaxComplexityWithAddonsBase>
    {
        public int? AuthorFriendCount { get; set; }

        public double? AuthorAverageReviewRating { get; set; }

        public double? AuthorBestBook { get; set; }

        public int? AuthorTotalReviewCount { get; set; }

        public MaxComplexityWithAddonsBase MapObject(IDataReader reader)
        {
            return new MaxComplexityWithAddonsBase
            {
                AuthorFriendCount = reader["AuthorFriendCount"] is not DBNull ? Convert.ToInt32(reader["AuthorFriendCount"]) : null,
                AuthorAverageReviewRating = reader["AuthorAverageReviewRating"] is not DBNull ? Convert.ToDouble(reader["AuthorAverageReviewRating"]) : null,
                AuthorBestBook = reader["AuthorBestBook"] is not DBNull ? Convert.ToDouble(reader["AuthorBestBook"]) : null,
                AuthorTotalReviewCount = reader["AuthorTotalReviewCount"] is not DBNull ? Convert.ToInt32(reader["AuthorTotalReviewCount"]) : null
            };
        }
    }
}
