﻿namespace DBAccess.Models
{
    public abstract class WithSQLObject<T> where T : class
    {
        public virtual string SQLObjectName 
        { 
            get => typeof(T).Name; 
            set => SQLObjectName = value; 
        }
    }
}
