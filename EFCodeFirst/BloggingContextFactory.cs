﻿using Microsoft.EntityFrameworkCore.Design;
using LibraryEntities;

namespace EFCodeFirst
{
    public class LibraryDBContextFactory : IDesignTimeDbContextFactory<LibraryDBContext>
    {
        public LibraryDBContext CreateDbContext(string[] args)
        {
            var context = LibraryDBContext.InitDBContext();
            return context;
        }
    }
}
