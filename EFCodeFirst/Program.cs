﻿using DBAccess;
using DBAccess.Models;
using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using LibraryEntities;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Asn1.X509;
using System.Linq;

namespace EFCodeFirst
{
    internal class Program
    {
        static void Main()
        {
            try 
            {
                //string src = "C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\TestData\\jsonData\\BookRewiewsFull.json";
                //string dest = "C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\TestData\\jsonData\\BookRewiews.json";

                //FileCopyHandler.Rewrite(src, dest);

                //var _dbContext = LibraryDBContext.InitDBContext(new LibraryEntities.Models.DBContextSettings()
                //{
                //    MustRecreateDB = false,
                //    MustUseSeeding = false,
                //});

                //var authorStatistics = _dbContext.Authors
                //    .Join(_dbContext.AuthorFriendList,
                //            author => author.Id, 
                //            friend => friend.AuthorId,
                //            (author, friend) => new { author, friend }
                //    )
                //    .GroupBy(result => result.author.Id)

                //Console.WriteLine("\n Start - " + DateTime.UtcNow + "\n");

                var config = new DBContextSettings()
                {
                    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                    InputScaleLevel = InputScaleLevel.Percent100,
                    ReferenceScale = 0.5,
                    MustRecreateDB = true,
                    MustUseSeeding = true
                };
                var _dbContext = LibraryDBContext.InitDBContext(config);

                //var auth12orStatistics = _dbContext.Authors
                //.GroupJoin(_dbContext.BookAuthors,
                //    author => author.Id,
                //    bookAuthors => bookAuthors.AuthorId,
                //    (author, bookAuthors) => new
                //    {
                //        AuthorFriendCount = author.Friends
                //            .Count(),
                //        AuthorAverageReviewRating = bookAuthors.SelectMany(ba => ba.Book.BookReviews)
                //            .Average(ba => ba.Rating),
                //        AuthorBestBook = bookAuthors.Select(ba => ba.Book)
                //            .Max(b => b.AvarageRating)
                //    })
                //.Where(a => a.AuthorFriendCount > 1)
                //.OrderByDescending(b => b.AuthorFriendCount)
                //.ThenByDescending(b => b.AuthorAverageReviewRating)
                //.ThenByDescending(b => b.AuthorBestBook)
                //.Take(10)
                //.AsQueryable();

                var auth1orStat2istics = _dbContext.Authors
                    .Join(_dbContext.BookAuthors,
                        author => author.Id,
                        bookAuthor => bookAuthor.AuthorId,
                        (author, bookAuthor) => new
                        {
                            Author = author,
                            Book = bookAuthor.Book
                        })
                    .GroupBy(result => result.Author.Id)
                    .Select(g => new
                    {
                        AuthorFriendCount = g.First().Author.Friends
                            .Count(),
                        AuthorAverageReviewRating = g.SelectMany(bg => bg.Book.BookReviews)
                            .Average(review => review.Rating),
                        AuthorBestBook = g
                            .Max(bg => bg.Book.AvarageRating),
                        AuthorTotalReviewCount = g.SelectMany(bg => bg.Book.BookReviews)
                            .Count()
                    })
                    .Where(a => a.AuthorTotalReviewCount > 0 && a.AuthorBestBook > 0)
                    .OrderByDescending(a => a.AuthorFriendCount)
                    .ThenByDescending(a => a.AuthorAverageReviewRating)
                    .ThenByDescending(a => a.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();

                var authorSta1t2i3stics = _dbContext.Authors
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Where(br => br.Rating > 0)
                            .Average(br => br.Rating),
                        AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                    })
                    .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();


                var authorStatistic123 = _dbContext.Authors
                    .Select(author => new
                    {
                        Id = author.Id,
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Average(br => br.Rating)
                    })
                     .OrderByDescending(a => a.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var authorStatistics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.AuthorFriendCount
                        })
                    .GroupJoin(
                        _dbContext.BookAuthors
                            .Join(
                                _dbContext.BookReviews.Where(br => br.Rating > 0),
                                ba => ba.BookId,
                                review => review.BookId,
                                (ba, review) => new
                                {
                                    ba.AuthorId,
                                    review.Rating
                                }),
                        author => author.AuthorId,
                        authorReviews => authorReviews.AuthorId,
                        (author, authorReviews) => new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorReviewRatings = authorReviews.Select(ar => ar.Rating)
                        })
                    .Select(author =>
                        new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorAverageReviewRating = author.AuthorReviewRatings.Any() ?
                                author.AuthorReviewRatings.Average() : 0
                        })
                    .OrderBy(a => a.AuthorId)
                    .Take(10)
                    .ToList();



                var authorStatistic5132s = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.AuthorFriendCount
                        })
                    .GroupJoin(
                        _dbContext.BookAuthors
                            .Join(
                                _dbContext.BookReviews.Where(br => br.Rating > 0),
                                ba => ba.BookId,
                                review => review.BookId,
                                (ba, review) => new
                                {
                                    ba.AuthorId,
                                    review.Rating
                                }),
                        author => author.AuthorId,
                        authorReviews => authorReviews.AuthorId,
                        (author, authorReviews) => new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorReviewRatings = authorReviews.Select(ar => ar.Rating)
                        })
                    .Select(author =>
                        new
                        {
                            AuthorId = author.AuthorId,
                            AuthorFriendCount = author.AuthorFriendCount,
                            AuthorAverageReviewRating = author.AuthorReviewRatings.Any() ?
                                author.AuthorReviewRatings.Average() : 0
                        })
                    .OrderBy(a => a.AuthorId)
                    .Take(10)
                    .ToList();

                var auth1o1rStat33stics = _dbContext.Authors
                                .Select(author => new
                                {
                                    Id = author.Id,
                                    AuthorFriendCount = author.Friends.Count(),
                                    AuthorAverageReviewRating = author.BookAuthors
                                        .SelectMany(author => author.Book.BookReviews)
                                        .Where(br => br.Rating > 0)
                                        .Average(br => br.Rating),
                                    AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                                })
                                .OrderBy(a => a.Id)
                                //.OrderByDescending(b => b.AuthorFriendCount)
                                //.ThenByDescending(b => b.AuthorAverageReviewRating)
                                //.ThenByDescending(b => b.AuthorBestBook)
                                .Take(10)
                                .ToList();
                var au13thorStat2istics = _dbContext.Authors
                    .Join(_dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                            author => author.Id,
                            friendList => friendList.AuthorId,
                            (author, friendList) => new
                            {
                                AuthorId = author.Id,
                                AuthorFriendCount = friendList.AuthorFriendCount
                            })
                    .GroupJoin(
                        _dbContext.BookAuthors
                        .SelectMany(ba => ba.Book.BookReviews, (ba, review) => new
                        {
                            ba.AuthorId,
                            review.Rating
                        }),
                        author => author.AuthorId,
                        author_br => author_br.AuthorId,
                        (author, author_br) => new
                        {
                            AuthorFriendCount = author.AuthorFriendCount == null ? author.AuthorFriendCount : 0,
                            AuthorAverageReviewRating = author_br.Any() ? author_br.Average(br => br.Rating) : 0
                        })
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var auastdho2rStatistics = _dbContext.Authors
    .Join(
        _dbContext.AuthorFriendList
            .GroupBy(a => a.AuthorId)
            .Select(g => new
            {
                AuthorId = g.Key,
                AuthorFriendCount = g.Count()
            }),
        author => author.Id,
        friendList => friendList.AuthorId,
        (author, friendList) => new
        {
            AuthorId = author.Id,
            AuthorFriendCount = friendList.AuthorFriendCount
        })
    .GroupJoin(
        _dbContext.BookAuthors
            .SelectMany(
                ba => ba.Book.BookReviews.DefaultIfEmpty(), // Handle potential null review
                (ba, review) => new
                {
                    ba.AuthorId,
                    Rating = review != null ? review.Rating : 0 // Default rating to 0 if review is null
                }),
        author => author.AuthorId,
        author_br => author_br.AuthorId,
        (author, authorReviews) => new
        {
            AuthorId = author.AuthorId,
            AuthorFriendCount = author.AuthorFriendCount,
            AuthorAverageReviewRating = authorReviews.Any() ? authorReviews.Average(ar => ar.Rating) : 0
        })
    .OrderByDescending(a => a.AuthorFriendCount)
    .ThenByDescending(a => a.AuthorAverageReviewRating)
    .Take(10)
    .ToList(); var a1u4t5h1orStatistics = _dbContext.Authors
    .Join(
        _dbContext.AuthorFriendList
            .GroupBy(a => a.AuthorId)
            .Select(g => new
            {
                AuthorId = g.Key,
                AuthorFriendCount = g.Count()
            }),
        author => author.Id,
        friendList => friendList.AuthorId,
        (author, friendList) => new
        {
            AuthorId = author.Id,
            AuthorFriendCount = friendList.AuthorFriendCount
        })
    .GroupJoin(
        _dbContext.BookAuthors
            .SelectMany(
                ba => ba.Book.BookReviews.DefaultIfEmpty(), // Handle potential null review
                (ba, review) => new
                {
                    ba.AuthorId,
                    Rating = review != null ? review.Rating : 0 // Default rating to 0 if review is null
                }),
        author => author.AuthorId,
        author_br => author_br.AuthorId,
        (author, authorReviews) => new
        {
            AuthorId = author.AuthorId,
            AuthorFriendCount = author.AuthorFriendCount,
            AuthorAverageReviewRating = authorReviews.Any() ? authorReviews.Average(ar => ar.Rating) : 0
        })
    .OrderByDescending(a => a.AuthorFriendCount)
    .ThenByDescending(a => a.AuthorAverageReviewRating)
    .Take(10)
    .ToList();

                //var config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //var _dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.PostgreSQL,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MySQL,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                //var authoadsrStatistics = _dbContext.Authors
                //    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()) && author.Friends.Any())
                //    .Select(author => new
                //    {
                //        AuthorFriendCount = author.Friends.Count,
                //        AuthorAverageReviewRating = author.BookAuthors
                //            .SelectMany(author => author.Book.BookReviews)
                //            .Average(br => br.Rating)
                //    })
                //    .Take(10)
                //    .AsQueryable();

                return;

               

                var aut1horStat32istics = _dbContext.Authors
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Where(br => br.Rating > 0)
                            .Average(br => br.Rating),
                        AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                    })
                    .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();


                var aut1h2o4r5S5t6a6t7istics = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors.Join(
                            _dbContext.BookReviews,
                            book => book.BookId,
                            review => review.BookId,
                            (book, review) => new
                            {
                                book.AuthorId,
                                review.Rating
                            }
                        )
                        .Where(br => br.Rating > 0)
                        .GroupBy(b => b.AuthorId)
                        .Select(b => new
                        {
                            AuthorId = b.Key,
                            AuthorAverageReviewRating = b.Average(c => c.Rating)
                        }),
                        author_fl => author_fl.AuthorId,
                        author_br => author_br.AuthorId,
                        (author_fl, author_br) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            author_br.AuthorAverageReviewRating
                        })
                    .Join(
                        _dbContext.BookAuthors.Join(
                            _dbContext.Books,
                            book => book.BookId,
                            review => review.Id,
                            (book, review) => new
                            {
                                book.AuthorId,
                                review.AvarageRating
                            }
                        )
                        .GroupBy(b => b.AuthorId)
                        .Select(b => new
                        {
                            AuthorId = b.Key,
                            AuthorBestBook = b.Max(c => c.AvarageRating)
                        }),
                        author_fl => author_fl.AuthorId,
                        author_br => author_br.AuthorId,
                        (author_fl, author_br) => new
                        {
                            author_fl.AuthorFriendCount,
                            author_fl.AuthorAverageReviewRating,
                            author_br.AuthorBestBook
                        })
                    .Where(a => a.AuthorAverageReviewRating > 0 && a.AuthorFriendCount > 1)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();


                //var config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.01,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //var _dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.PostgreSQL,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.01,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MySQL,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.01,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                var authorStatistic123s = _dbContext.Authors
                    .GroupJoin(
                        _dbContext.AuthorFriendList,
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            AuthorFriendCount = friendList.Count(),
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author => author.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author, bookAuthor) => new
                        {
                            author.AuthorId,
                            bookAuthor.BookId,
                            author.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookReviews,
                        ba => ba.BookId,
                        review => review.BookId,
                        (ba, review) => new
                        {
                            ba.AuthorId,
                            ba.AuthorFriendCount,
                            ReviewRating = review.Rating,
                        })
                    .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                    .Select(g => new
                    {
                        AuthorFriendCount = g.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                    })
                    .Take(10)
                    .AsQueryable();


                var sdfsdf = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author_fl => author_fl.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author_fl, bookAuthor) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.Books,
                        authorBook => authorBook.BookId,
                        book => book.Id,
                        (authorBook, bookAuthor) => new
                        {
                            authorBook.AuthorId,
                            authorBook.AuthorFriendCount,
                            bookAuthor.AvarageRating
                        })
                    .GroupBy(a => new { a.AuthorId, a.AuthorFriendCount })
                    .Select(a => new
                    {
                        a.Key.AuthorFriendCount,
                        AuthorAverageBookRating = a.Average(ab => ab.AvarageRating)
                    })
                .Where(b => b.AuthorFriendCount > 0 && b.AuthorAverageBookRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageBookRating)
                .Take(10)
                .AsQueryable();

                var asdgg = _dbContext.Authors
                .Join(
                    _dbContext.AuthorFriendList
                    .GroupBy(a => a.AuthorId)
                    .Select(g => new
                    {
                        AuthorId = g.Key,
                        AuthorFriendCount = g.Count()
                    }),
                    author => author.Id,
                    friendList => friendList.AuthorId,
                    (author, friendList) => new
                    {
                        AuthorId = author.Id,
                        friendList.AuthorFriendCount
                    })
                .Join(
                    _dbContext.BookAuthors,
                    author_fl => author_fl.AuthorId,
                    bookAuthor => bookAuthor.AuthorId,
                    (author_fl, bookAuthor) => new
                    {
                        author_fl.AuthorId,
                        author_fl.AuthorFriendCount,
                        bookAuthor.BookId
                    })
                .Join(
                    _dbContext.BookReviews,
                    ba => ba.BookId,
                    review => review.BookId,
                    (ba, review) => new
                    {
                        ba.AuthorId,
                        ba.AuthorFriendCount,
                        ReviewRating = review.Rating
                    })
                .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                .Select(g => new
                {
                    g.Key.AuthorFriendCount,
                    AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                })
                .Where(b => b.AuthorFriendCount > 0 && b.AuthorAverageReviewRating > 0)
                .OrderByDescending(b => b.AuthorFriendCount)
                .ThenByDescending(b => b.AuthorAverageReviewRating)
                .Take(10)
               .AsQueryable();



                var a1 = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors.Join(
                            _dbContext.BookReviews,
                            book => book.BookId,
                            review => review.BookId,
                            (book, review) => new
                            {
                                book.AuthorId,
                                review.Rating
                            }
                        )
                        .Where(br => br.Rating > 0)
                        .GroupBy(b => b.AuthorId)
                        .Select(b => new
                        {
                            AuthorId = b.Key,
                            AuthorAverageReviewRating = b.Average(c => c.Rating)
                        }),
                        author_fl => author_fl.AuthorId,
                        author_br => author_br.AuthorId,
                        (author_fl, author_br) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            author_br.AuthorAverageReviewRating
                        })
                    .Join(
                        _dbContext.BookAuthors.Join(
                            _dbContext.Books,
                            book => book.BookId,
                            review => review.Id,
                            (book, review) => new
                            {
                                book.AuthorId,
                                review.AvarageRating
                            }
                        )
                        .GroupBy(b => b.AuthorId)
                        .Select(b => new
                        {
                            AuthorId = b.Key,
                            AuthorBestBook = b.Max(c => c.AvarageRating)
                        }),
                        author_fl => author_fl.AuthorId,
                        author_br => author_br.AuthorId,
                        (author_fl, author_br) => new
                        {
                            author_fl.AuthorFriendCount,
                            author_fl.AuthorAverageReviewRating,
                            author_br.AuthorBestBook
                        })
                        .Where(a => a.AuthorAverageReviewRating > 0)
                        .OrderByDescending(b => b.AuthorFriendCount)
                        .ThenByDescending(b => b.AuthorAverageReviewRating)
                        .ThenByDescending(b => b.AuthorBestBook)
                        .Take(10)
                        .AsQueryable();

                var a2 = _dbContext.Authors
                        .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                        .Select(author => new
                        {
                            AuthorFriendCount = author.Friends.Count(),
                            AuthorAverageReviewRating = author.BookAuthors
                                .SelectMany(author => author.Book.BookReviews)
                                .Where(br => br.Rating > 0)
                                .Average(br => br.Rating),
                            AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                        })
                        .Where(a => a.AuthorAverageReviewRating > 0)
                        .OrderByDescending(b => b.AuthorFriendCount)
                        .ThenByDescending(b => b.AuthorAverageReviewRating)
                        .ThenByDescending(b => b.AuthorBestBook)
                        .Take(10)
                        .AsQueryable();


                var autho1rSt2atistics = _dbContext.Authors
                    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Where(br => br.Rating > 0)
                            .Average(br => br.Rating),
                        AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                    })
                    .Where(a => a.AuthorAverageReviewRating > 0)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();

                var authorStatisticccc = _dbContext.Authors
                    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Average(br => br.Rating),
                        AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                    })
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(10)
                    .AsQueryable();

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MySQL,
                //    InputScaleLevel = InputScaleLevel.Percent10,
                //    ReferenceScale = 1,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                ////DBContextSettings config = null;
                ////LibraryDBContext _dbContext = null;

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MySQL,
                //    InputScaleLevel = InputScaleLevel.Percent10,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};

                //_dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                //    InputScaleLevel = InputScaleLevel.Percent100,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = false,
                //    MustUseSeeding = false
                //};
                //_dbContext = LibraryDBContext.InitDBContext(config);

                var _a1 = _dbContext.Authors
                        .Join(
                            _dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                            author => author.Id,
                            friendList => friendList.AuthorId,
                            (author, friendList) => new
                            {
                                AuthorId = author.Id,
                                friendList.AuthorFriendCount
                            })
                        .Join(
                            _dbContext.BookAuthors,
                            author_fl => author_fl.AuthorId,
                            bookAuthor => bookAuthor.AuthorId,
                            (author_fl, bookAuthor) => new
                            {
                                author_fl.AuthorId,
                                author_fl.AuthorFriendCount,
                                bookAuthor.BookId
                            })
                        .Join(
                            _dbContext.BookReviews,
                            ba => ba.BookId,
                            review => review.BookId,
                            (ba, review) => new
                            {
                                ba.AuthorId,
                                ba.AuthorFriendCount,
                                ReviewRating = review.Rating
                            })
                        .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                        .Select(g => new
                        {
                            g.Key.AuthorFriendCount,
                            AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                        })
                        .Where(b => b.AuthorFriendCount > 3 && b.AuthorAverageReviewRating > 1)
                        .OrderByDescending(b => b.AuthorFriendCount)
                        .ThenByDescending(b => b.AuthorAverageReviewRating)
                        .Take(10)
                        .AsQueryable();

                var b2 = _dbContext.Authors
                    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count,
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Average(br => br.Rating)
                    })
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .Take(10)
                    .AsQueryable();
                //.ToList();

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.PostgreSQL,
                //    InputScaleLevel = InputScaleLevel.Percent10,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};

                //_dbContext = LibraryDBContext.InitDBContext(config);

                //config = new DBContextSettings()
                //{
                //    DatabaseProvider = DatabaseProviderType.MySQL,
                //    InputScaleLevel = InputScaleLevel.Percent10,
                //    ReferenceScale = 0.5,
                //    MustRecreateDB = true,
                //    MustUseSeeding = true
                //};

                //_dbContext = LibraryDBContext.InitDBContext(config);

                //_dbContext = LibraryDBContext.InitDBContext(config);
                var _dbAccess = new DataAccessLayer();

                //var a1 = _dbContext.Authors
                //    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                //    .Select(author => new
                //    {
                //        AuthorFriendCount = author.Friends.Count(),
                //        AuthorAverageReviewRating = author.BookAuthors
                //            .SelectMany(author => author.Book.BookReviews)
                //            .Where(br => br.Rating > 0)
                //            .Average(br => br.Rating),
                //        AuthorBestBook = author.BookAuthors.Max(ba => ba.Book.AvarageRating)
                //    })
                //    .Where(a => a.AuthorAverageReviewRating > 0)
                //    .OrderByDescending(b => b.AuthorFriendCount)
                //    .ThenByDescending(b => b.AuthorAverageReviewRating)
                //    .ThenByDescending(b => b.AuthorBestBook)
                //    .Take(10)
                //    .AsQueryable();


                //bool areEqual = true;// ax1.SequenceEqual(ax2);

                var aaa1 =
                    _dbAccess.ExecuteSQLToDB<MaxComplexityWithAddons>()
                    .Select(x => new
                    {
                        AuthorFriendCount = x.AuthorFriendCount.Value,
                        AuthorAverageReviewRating = x.AuthorAverageReviewRating.Value,
                        AuthorTotalReviewCount = x.AuthorTotalReviewCount.Value,
                        AuthorBestBook = x.AuthorBestBook.Value
                    })
                    .ToList();

                var bbb1 = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author_fl => author_fl.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author_fl, bookAuthor) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.Books,
                        authorBook => authorBook.BookId,
                        book => book.Id,
                        (authorBook, bookAuthor) => new
                        {
                            authorBook.AuthorId,
                            authorBook.AuthorFriendCount,
                            authorBook.BookId,
                            bookAuthor.AvarageRating
                        })
                    .Join(
                        _dbContext.BookReviews,
                        ba => ba.BookId,
                        review => review.BookId,
                        (ba, review) => new
                        {
                            ba.AuthorId,
                            ba.AuthorFriendCount,
                            ReviewRating = review.Rating,
                            BookAvgRating = ba.AvarageRating
                        })
                    .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                    .Select(g => new
                    {
                        g.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = g.Where(br => br.ReviewRating > 0)
                            .Average(c => c.ReviewRating),
                        AuthorTotalReviewCount = g.Where(br => br.ReviewRating > 0)
                            .Count(),
                        AuthorBestBook = g.Max(c => c.BookAvgRating)
                    })
                    .Where(a => a.AuthorTotalReviewCount > 1 && a.AuthorBestBook > 1)
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .ThenByDescending(b => b.AuthorBestBook)
                    .Take(100)
                    .ToList();

                var aa = _dbContext.Authors
                    .Join(
                        _dbContext.AuthorFriendList
                        .GroupBy(a => a.AuthorId)
                        .Select(g => new
                        {
                            AuthorId = g.Key,
                            AuthorFriendCount = g.Count()
                        }),
                        author => author.Id,
                        friendList => friendList.AuthorId,
                        (author, friendList) => new
                        {
                            AuthorId = author.Id,
                            friendList.AuthorFriendCount
                        })
                    .Join(
                        _dbContext.BookAuthors,
                        author_fl => author_fl.AuthorId,
                        bookAuthor => bookAuthor.AuthorId,
                        (author_fl, bookAuthor) => new
                        {
                            author_fl.AuthorId,
                            author_fl.AuthorFriendCount,
                            bookAuthor.BookId
                        })
                    .Join(
                        _dbContext.BookReviews,
                        ba => ba.BookId,
                        review => review.BookId,
                        (ba, review) => new
                        {
                            ba.AuthorId,
                            ba.AuthorFriendCount,
                            ReviewRating = review.Rating
                        })
                    .GroupBy(x => new { x.AuthorId, x.AuthorFriendCount })
                    .Select(g => new
                    {
                        g.Key.AuthorFriendCount,
                        AuthorAverageReviewRating = g.Average(c => c.ReviewRating)
                    })
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var bb = _dbContext.Authors
                        .Join(_dbContext.AuthorFriendList
                                .GroupBy(a => a.AuthorId)
                                .Select(g => new
                                {
                                    AuthorId = g.Key,
                                    AuthorFriendCount = g.Count()
                                }),
                                author => author.Id,
                                friendList => friendList.AuthorId,
                                (author, friendList) => new
                                {
                                    AuthorId = author.Id,
                                    friendList.AuthorFriendCount
                                })
                        .Join(
                            _dbContext.BookAuthors.Join(
                                _dbContext.BookReviews,
                                book => book.BookId,
                                review => review.BookId,
                                (book, review) => new
                                {
                                    book.AuthorId,
                                    review.Rating
                                }
                            )
                            .GroupBy(b => b.AuthorId)
                            .Select(b => new
                            {
                                AuthorId = b.Key,
                                AuthorAverageReviewRating = b.Average(c => c.Rating)
                            }),
                            author_fl => author_fl.AuthorId,
                            author_br => author_br.AuthorId,
                            (author_fl, author_br) => new
                            {
                                author_fl.AuthorFriendCount,
                                author_br.AuthorAverageReviewRating
                            })
                        .OrderByDescending(b => b.AuthorFriendCount)
                        .ThenByDescending(b => b.AuthorAverageReviewRating)
                        .Take(10)
                        .ToList();

                var authorStatisticsasdas = _dbContext.Authors
                    .Join(_dbContext.AuthorFriendList
                            .GroupBy(a => a.AuthorId)
                            .Select(g => new
                            {
                                AuthorId = g.Key,
                                AuthorFriendCount = g.Count()
                            }),
                            author => author.Id,
                            friendList => friendList.AuthorId,
                            (author, friendList) => new
                            {
                                AuthorId = author.Id,
                                friendList.AuthorFriendCount
                            })
                    .Join(
                        _dbContext.BookAuthors.Join(
                            _dbContext.BookReviews,
                            book => book.BookId,
                            review => review.BookId,
                            (book, review) => new
                            {
                                book.AuthorId,
                                review.Rating
                            }
                        )
                        .GroupBy(b => b.AuthorId)
                        .Select(b => new
                        {
                            AuthorId = b.Key,
                            AuthorAverageReviewRating = b.Average(c => c.Rating)
                        }),
                        author_fl => author_fl.AuthorId,
                        author_br => author_br.AuthorId,
                        (author_fl, author_br) => new
                        {
                            author_fl.AuthorFriendCount,
                            author_br.AuthorAverageReviewRating
                        })
                    .OrderByDescending(b => b.AuthorFriendCount)
                    .ThenByDescending(b => b.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var authorStatisticsx = _dbContext.Authors
                    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        AuthorTotalReviewCount = author.BookAuthors
                            .SelectMany(ba => ba.Book.BookReviews)
                            .Count()
                    })
                    .OrderByDescending(a => a.AuthorTotalReviewCount)
                    .Take(10)
                    .ToList();

                var authorStatisticss123 = _dbContext.Authors
                    .Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        BookAuthorsCount = author.BookAuthors.Count()
                    })
                    .OrderByDescending(a => a.BookAuthorsCount)
                    .Take(10)
                    .ToList();

                //var _dbAccess = new DataAccessLayer();

                var results = _dbAccess.ExecuteSQLToDB<AuthorFriendsAndBookRating>();

                var authorStatisticsss = _dbContext.Authors
                    .Join(_dbContext.BookAuthors,
                        author => author.Id,
                        bookAuthor => bookAuthor.AuthorId,
                        (author, bookAuthor) => new
                        {
                            author,
                            book = bookAuthor.Book
                        })
                    .GroupBy(author => author.author.Id)
                    .Select(author => new
                    {
                        authorId = author.Key,
                        books = author.Select(a => a.book)
                    })
                    .OrderBy(a => a.authorId)
                    .Take(10)
                    .ToList();


                var authorStatisticsssx = _dbContext.Authors
                    .Include(author => author.Friends) 
                    .Select(author => new
                    {
                        AuthorId = author.Id,
                        Friends = author.Friends,
                        BookAuthors = author.BookAuthors,
                        Reviews = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                    })
                    .Take(10)
                    .ToList();


                var authorStatisticss_ = _dbContext.Authors
                    .Include(author => author.Friends)
                    //.Where(author => author.BookAuthors.Any(author => author.Book.BookReviews.Any()))
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count(),
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .Average(br => br.Rating)
                    })
                    .OrderByDescending(a => a.AuthorFriendCount)
                    .ThenByDescending(a => a.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var authorStatisticssc = _dbContext.Authors
                    .Select(author => new
                    {
                        AuthorFriendCount = author.Friends.Count,
                        AuthorAverageReviewRating = author.BookAuthors
                            .SelectMany(author => author.Book.BookReviews)
                            .DefaultIfEmpty()
                            .Average(br => br.Rating)
                    })
                    //.OrderByDescending(a => a.AuthorFriendCount)
                    //.ThenByDescending(a => a.AuthorAverageReviewRating)
                    .Take(10)
                    .ToList();

                var authorReviews = _dbContext.Authors
                    .Select(author => new
                    {
                        AuthorId = author.Id,
                        AuthorBookReviews = author.BookAuthors.SelectMany(bookAuthor => bookAuthor.Book.BookReviews)
                    })
                    .Where(br => br.AuthorBookReviews.Any())
                    .OrderBy(a => a.AuthorId)
                    .Take(10)
                    .ToList();

                var authorReviewss = _dbContext.Authors
                    .Join(_dbContext.BookAuthors,
                          author => author.Id,
                          bookAuthor => bookAuthor.AuthorId,
                          (author, bookAuthor) => new { author, bookAuthor })
                    .Join(_dbContext.Books,
                          temp => temp.bookAuthor.BookId,
                          book => book.Id,
                          (temp, book) => new { temp.author, book })
                    .Join(_dbContext.BookReviews,
                          temp => temp.book.Id,
                          review => review.BookId,
                          (temp, review) => new
                          {
                              temp.author.Id,
                              Review = review
                          })
                    .GroupBy(temp => temp.Id)
                    .Select(group => new
                    {
                        AuthorId = group.Key,
                        AuthorBookReviews = group.Select(item => item.Review)
                    })
                    .OrderBy(a => a.AuthorId)
                    .Take(10)
                    .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}