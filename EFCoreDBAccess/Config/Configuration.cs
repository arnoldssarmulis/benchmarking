﻿using Microsoft.Extensions.Configuration;

namespace EFCoreDBAccess.Config
{
    public static class Configuration
    {
        public static IConfigurationRoot GetConfiguration
        {
            get => new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
        }
    }
}
