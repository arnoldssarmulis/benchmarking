﻿namespace EFCoreDBAccess.Config
{
    public enum DatabaseProviderType
    {
        MSSQLServer,
        PostgreSQL,
        MySQL
    }
}
