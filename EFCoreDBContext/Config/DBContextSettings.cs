﻿using InputData;
using InputDataToEntitiesMap.Config;
using System.Text.Json.Serialization;

namespace EFCoreDBContext.Config
{
    public class DBContextSettings
    {
        public DBContextSettings()
        {
            InputScaleLevel = InputScaleLevel.Percent100;
            DatabaseProvider = DatabaseProviderType.MSSQLServer;
            ReferenceScale = 1;
            MustUseColdCacheTesting = false;
            MustRecreateDB = false;
            MustUseSeeding = true;
        }

        public DBContextSettings(DBContextSettings contextSettings)
        {
            InputScaleLevel = contextSettings.InputScaleLevel;
            DatabaseProvider = contextSettings.DatabaseProvider;
            ReferenceScale = contextSettings.ReferenceScale;
            MustUseColdCacheTesting = contextSettings.MustUseColdCacheTesting;
            MustRecreateDB = contextSettings.MustRecreateDB;
            MustUseSeeding = contextSettings.MustUseSeeding;
        }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public InputScaleLevel InputScaleLevel { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DatabaseProviderType DatabaseProvider { get; set; }

        public double ReferenceScale { get; set; }

        public bool MustUseColdCacheTesting { get; set; }

        public bool MustUseSeeding { get; set; }

        public bool MustRecreateDB { get; set; }
    }
}
