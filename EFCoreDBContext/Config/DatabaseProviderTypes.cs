﻿namespace EFCoreDBContext.Config
{
    public enum DatabaseProviderType
    {
        MSSQLServer,
        PostgreSQL,
        MySQL
    }
}
