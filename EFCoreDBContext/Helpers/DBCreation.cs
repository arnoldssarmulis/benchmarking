﻿using EFCoreDBContext.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Text.RegularExpressions;

namespace EFCoreDBContext.Helpers
{
    internal static class DBCreation
    {
        private static string SQLBasePath
        {
            get
            {
                var config = Configuration.GetConfiguration;
                var sqlBasePath = config.GetSection("BasePathForCreatableSQLs").Value;

                return sqlBasePath;
            }
        }

        public static void CreateCustomDBObjects(DatabaseFacade database)
        {
            string directoryPath = Path.Combine(SQLBasePath, DBProvider.ProviderType.ToString());
            string[] sqlFiles = Directory.GetFiles(directoryPath, "*.sql");

            if (!sqlFiles.Any())
                return;

            foreach (string sqlFile in sqlFiles)
            {
                string sqlContents = File.ReadAllText(sqlFile);

                if (DBProvider.IsMSSQLServer)
                {
                    var batches = SplitSqlScript(sqlContents);

                    batches.ForEach(b => database.ExecuteSqlRaw(b));
                }
                else
                {
                    database.ExecuteSqlRaw(sqlContents);
                }
            }
        }

        static List<string> SplitSqlScript(string sqlScript)
        {
            string[] batches = Regex.Split(sqlScript, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);

            return batches
                .Select(batch => batch.Trim())
                .Where(batch => !string.IsNullOrWhiteSpace(batch))
                .ToList();
        }

    }
}
