﻿using EFCoreDBContext.Config;

namespace EFCoreDBContext.Helpers
{
    internal static class ModelCreation
    {
        public static string GetVarcharColumn(int maxSize)
        {
            return $"{(DBProvider.IsMSSQLServer ? "n" : "")}varchar({maxSize})";
        }
    }
}
