﻿using EFCoreDBContext.Config;
using EFCoreDBContext.Helpers;
using InputDataToEntitiesMap.Config;
using InputDataToEntitiesMap.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LibraryEntities
{
    public class LibraryDBContext : DbContext
    {
        private readonly string _connectionString;
        private readonly bool mustUseSeeding = false;

        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorFriendList> AuthorFriendList { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<BookReview> BookReviews { get; set; }
        public DbSet<Reviewer> Reviewers { get; set; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public LibraryDBContext(DbContextOptions<LibraryDBContext> options, bool mustUseSeeding, bool mustRecreateDB)
            : base(options)
        {
            var config = Configuration.GetConfiguration;
            _connectionString = config.GetConnectionString(DBProvider.ConnectionStringName) ?? 
                throw new NotSupportedException();

            if (mustRecreateDB)
                Database.EnsureDeleted();

            this.mustUseSeeding = mustUseSeeding;

            Database.EnsureCreated();

            if (mustRecreateDB)
            {
                DBCreation.CreateCustomDBObjects(Database);
            }
        }

        public static LibraryDBContext InitDBContext(DBContextSettings? settings = null)
        {
            settings ??= new DBContextSettings();

            InputScale.InputDataScaleLevel = settings.InputScaleLevel;
            InputScale.ReferenceScaleLevel = settings.ReferenceScale;

            DBProvider.ProviderType = settings.DatabaseProvider;

            if (settings.MustRecreateDB)
                settings.MustUseSeeding = true;

            var dbOptions = new DbContextOptions<LibraryDBContext>();

            return new LibraryDBContext(dbOptions, settings.MustUseSeeding, settings.MustRecreateDB);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
                return;

            if (DBProvider.IsMSSQLServer)
            {
                optionsBuilder.UseSqlServer(_connectionString, sqlServerOptions =>
                {
                    sqlServerOptions.CommandTimeout(6000);
                });
            }
            else if (DBProvider.IsPostgreSQL)
            {
                optionsBuilder.UseNpgsql(_connectionString, npgSqlOptions =>
                {
                    npgSqlOptions.CommandTimeout(6000);
                });
            }
            else if (DBProvider.IsMySQL)
            {
                optionsBuilder.UseMySQL(_connectionString, mySqlOptions =>
                {
                    mySqlOptions.CommandTimeout(6000);
                });
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>(entity =>
            {
                entity.HasKey(e => e.Id); 
                entity.Property(e => e.Id)
                      .ValueGeneratedNever(); 

                entity.Property(a => a.Name)
                      .HasColumnType(ModelCreation.GetVarcharColumn(255))
                      .IsRequired();

                entity.Property(a => a.BirthDate)
                      .HasColumnType(ModelCreation.GetVarcharColumn(50));

                entity.HasIndex(a => new { a.Name, a.BirthDate })
                      .HasDatabaseName("IX_Author_NameBirthDate");

                entity.HasOne(a => a.ChildOfAuthor)
                      .WithMany()
                      .HasForeignKey(a => a.ChildOfAuthorId)
                      .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                      .ValueGeneratedNever();

                entity.Property(b => b.Title)
                      .HasColumnType(ModelCreation.GetVarcharColumn(500))
                      .IsRequired();

                entity.HasIndex(b => b.Title)
                      .IsUnique(false); 

                entity.Property(b => b.Genre)
                    .HasColumnType(ModelCreation.GetVarcharColumn(500));

                entity.Property(b => b.BookLink)
                    .HasColumnType(ModelCreation.GetVarcharColumn(200))
                    .IsRequired();

                entity.Property(b => b.BookImageLink)
                    .HasColumnType(ModelCreation.GetVarcharColumn(200));
            });

            modelBuilder.Entity<BookAuthor>()
                .Property(e => e.Id)
                .ValueGeneratedNever();

            ///Aprakstīt kārtīgāk šo te!
            modelBuilder.Entity<AuthorFriendList>(entity =>
            {
                entity.Property(r => r.Id)
                    .ValueGeneratedNever();

                modelBuilder.Entity<AuthorFriendList>()
                    .HasOne(afl => afl.Author)
                    .WithMany(author => author.Friends)
                    .HasForeignKey(afl => afl.AuthorId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.Property(br => br.FriendshipType)
                    .HasColumnType(ModelCreation.GetVarcharColumn(2000));
            });

            modelBuilder.Entity<BookReview>(entity =>
            {
                entity.Property(r => r.Id)
                    .ValueGeneratedNever();

                entity.Property(br => br.ReviewText)
                    .HasColumnType(ModelCreation.GetVarcharColumn(2000));
            });

            modelBuilder.Entity<Reviewer>(entity =>
            {
                entity.Property(r => r.Id)
                    .ValueGeneratedNever();

                entity.Property(r => r.Name)
                    .HasColumnType(ModelCreation.GetVarcharColumn(200));

                entity.Property(r => r.Surname)
                    .HasColumnType(ModelCreation.GetVarcharColumn(200));
            });

            if (mustUseSeeding)
            {
                Console.WriteLine("time_1: " + DateTime.UtcNow);

                CSVBookToEntities.MapCsvBooksToEntityBooks();

                Console.WriteLine("time_2: " + DateTime.UtcNow);
                Console.WriteLine("booksCount: " + CSVBookToEntities.books.Count);
                Console.WriteLine("authorsCount: " + CSVBookToEntities.authors.Count);
                Console.WriteLine("bookAuthorsCount: " + CSVBookToEntities.bookAuthors.Count);
                Console.WriteLine("authorFriendList: " + CSVBookToEntities.authorFriendList.Count);

                modelBuilder.Entity<Book>().HasData(CSVBookToEntities.books);
                modelBuilder.Entity<Author>().HasData(CSVBookToEntities.authors);

                modelBuilder.Entity<AuthorFriendList>().HasData(CSVBookToEntities.authorFriendList);
                modelBuilder.Entity<BookAuthor>().HasData(CSVBookToEntities.bookAuthors);

                Console.WriteLine("time_3: " + DateTime.UtcNow);

                JsonBookReviewToEntities.ConvertToBookReview();

                Console.WriteLine("time_4: " + DateTime.UtcNow);

                Console.WriteLine("rewiewsCount: " + JsonBookReviewToEntities.bookReviews.Count);
                Console.WriteLine("reviewersCount: " + JsonBookReviewToEntities.reviewers.Count);

                modelBuilder.Entity<BookReview>().HasData(JsonBookReviewToEntities.bookReviews);
                modelBuilder.Entity<Reviewer>().HasData(JsonBookReviewToEntities.reviewers);

                Console.WriteLine("time_5: " + DateTime.UtcNow);
                Console.WriteLine("Done " + DateTime.UtcNow);
            }
        }
    }
    
}
