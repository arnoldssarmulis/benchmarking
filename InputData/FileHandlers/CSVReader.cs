﻿using InputData.Models;
using Microsoft.VisualBasic.FileIO;

namespace InputData.FileHandlers
{
    public class CSVReader
    {
        public const string CSV_FOLDER = "csvData";

        public static string CsvFolderPath
        {
            //get => Path.Combine(Environment.CurrentDirectory, CSV_FOLDER);
            get => Path.Combine("C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\TestData", CSV_FOLDER);
        }

        public static List<Book> ReadBooksFromCSV(string fileName)
        {
            string filePath = Path.Combine(CsvFolderPath, fileName);

            var booksList = new List<Book>();

            using (TextFieldParser csvParser = new TextFieldParser(filePath))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    string[] fields = csvParser.ReadFields();
                    var book = new Book
                    {
                        Author = fields[0],
                        BookFormat = fields[1],
                        Description = fields[2],
                        Genre = fields[3],
                        ImageLink = fields[4],
                        ISBN = fields[5],
                        ISBN13 = fields[6],
                        Link = fields[7],
                        Pages = int.TryParse(fields[8], out int p) ? p : 0,
                        Rating = double.TryParse(fields[9], out double r) ? r : 0.0,
                        Reviews = int.TryParse(fields[10], out int rv) ? rv : 0,
                        Title = fields[11],
                        TotalRatings = int.TryParse(fields[12], out int tr) ? tr : 0
                    };

                    booksList.Add(book);
                }
            }

            return booksList;
        }
    }
}
