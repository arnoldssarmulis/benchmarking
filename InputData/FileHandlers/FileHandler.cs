﻿using InputData.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http.Json;
using System.Text.RegularExpressions;

namespace InputData.FileHandlers
{
    public class FileCopyHandler
    {
        public static void FixJson(string fileName)
        {
            try
            {
                string filePath = Path.Combine(JsonReader.JsonFolderPath, fileName);
                int invalidCount = 0;
                int validCount = 0;
                int invalidCount2 = 0;

                if (File.Exists(filePath))
                {
                    List<string> lines = new List<string>(File.ReadAllLines(filePath));

                    for (int i = 0; i < lines.Count; i++)
                    {
                        lines[i] = "," + lines[i];
                        //lines[i] = lines[i].Replace("\\\"", "\"");

                        string pattern = "\"review_sentences\":\\s*\\[\\s*(?:\\[.*?\\](?:,\\s*)?)*\\]";
                        Match match = Regex.Match(lines[i], pattern);

                        MatchCollection matches = Regex.Matches(match.Value, @"""([^""]+)""]");

                        List<string> extractedSentences = new List<string>();
                        string newSentences = "\"review_sentences\": [";
                        foreach (Match _match in matches)
                        {
                            if (_match.Groups.Count <= 1)
                                continue;

                            var val = _match.Groups[1];
                            newSentences += "\"" + val + "\"," ;
                        }
                        newSentences = newSentences.TrimEnd(',');
                        newSentences += "]";

                        newSentences = "{" + newSentences + "}";

                        if (!IsValidJson(newSentences))
                            newSentences = "\"review_sentences\":[]";
                        else
                        {
                            newSentences = newSentences.TrimStart('{');
                            newSentences = newSentences.TrimEnd('}');
                        }

                        var newData = Regex.Replace(lines[i], pattern, newSentences);
                        if (newData.StartsWith(","))
                        {
                            newData = newData.Substring(1);

                            if (!IsValidJson(newData))
                            {
                                newData = "";
                            }
                        }

                        lines[i] = newData;
                    }

                    File.WriteAllLines(filePath, lines);
                }
                else
                {
                    Console.WriteLine("File does not exist.");
                }
            }
            catch (Exception ex)
            {
                // Handle any errors that might occur
                Console.WriteLine("An error occurred: " + ex.Message);
            }

        }

        public static void Rewrite(string sourceFilePath, string destinationFilePath, decimal? parts = null)
        {
            if (!parts.HasValue)
                parts = (decimal?)0.50;

            try
            {
                using (FileStream sourceStream = File.OpenRead(sourceFilePath))
                using (FileStream destinationStream = File.OpenWrite(destinationFilePath))
                {
                    long totalBytesToCopy = (long)(sourceStream.Length * parts);
                    byte[] buffer = new byte[1];
                    int bytesRead;
                    long totalBytesCopied = 0;

                    while ((bytesRead = sourceStream.Read(buffer, 0, buffer.Length)) > 0 && totalBytesCopied < totalBytesToCopy)
                    {
                        destinationStream.Write(buffer, 0, bytesRead);
                        totalBytesCopied += bytesRead;
                    }
                }

                Console.WriteLine("File copied successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }
        }

        private static bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }

            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) ||
                (strInput.StartsWith("[") && strInput.EndsWith("]")))
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    // Exception in parsing json
                    Console.WriteLine("JSON String is invalid. Error: " + jex.Message);
                    return false;
                }
                catch (Exception ex) // some other exception
                {
                    Console.WriteLine("Error: " + ex.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
