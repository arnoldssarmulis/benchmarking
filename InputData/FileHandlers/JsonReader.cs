﻿using Newtonsoft.Json;

namespace InputData.FileHandlers
{
    public static class JsonReader
    {
        public const string JSON_FOLDER = "jsonData";

        public static string JsonFolderPath
        {
            get => Path.Combine("C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\TestData", JSON_FOLDER);
            //get => Path.Combine(Environment.CurrentDirectory, JSON_FOLDER);
        }

        public static List<T> ReadJsonFile<T>(string fileName) where T : class
        {
            string filePath = Path.Combine(JsonFolderPath, fileName);

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File not found: {filePath}");
            }

            try
            {
                string jsonContent = File.ReadAllText(filePath);

                var dataList = JsonConvert.DeserializeObject<List<T>>(jsonContent);

                return dataList;
            }
            catch (Exception ex)
            {
                // Handle any exceptions that might occur during file reading or JSON deserialization
                Console.WriteLine($"Error reading JSON file: {ex.Message}");
                return null;
            }
        }
    }
}
