﻿namespace InputData.Models
{
    public class Book
    {
        public string Author { get; set; }
        public string BookFormat { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public string ImageLink { get; set; }
        public string ISBN { get; set; }
        public string ISBN13 { get; set; }
        public string Link { get; set; }
        public int Pages { get; set; }
        public double Rating { get; set; }
        public int Reviews { get; set; }
        public string Title { get; set; }
        public int TotalRatings { get; set; }
    }
}
