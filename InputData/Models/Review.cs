﻿using Newtonsoft.Json;

namespace InputData.Models
{
    public class Review
    {
        public Review()
        {
            ReviewSentences = new List<string>();
        }

        [JsonProperty("user_id")]
        public string UserId { get; set; }
        public string Timestamp { get; set; }

        [JsonProperty("review_sentences")]
        public List<string> ReviewSentences { get; set; }
        public int Rating { get; set; }

        [JsonProperty("has_spoiler")]
        public bool HasSpoiler { get; set; }

        [JsonProperty("book_id")]
        public string BookId { get; set; }

        [JsonProperty("review_id")]
        public string ReviewId { get; set; }
    }
}
