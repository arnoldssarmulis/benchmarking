﻿namespace InputDataToEntitiesMap.Config
{
    public static class InputScale
    {
        public static InputScaleLevel InputDataScaleLevel;

        public static double ReferenceScaleLevel;

        public static double GetMultiplier
        {
            get
            {
                switch (InputDataScaleLevel)
                {
                    case InputScaleLevel.Percent50:
                        return 0.5;

                    case InputScaleLevel.Percent10:
                        return 0.1;

                    default:
                    case InputScaleLevel.Percent100:
                        return 1;
                }
            }
        }

        public static List<T> TakeConfiguredPercent<T>(IEnumerable<T> collection)
        {
            int count = (int)(collection.Count() * GetMultiplier);
            collection = collection.Take(count);

            return collection.ToList();
        }
    }
}
