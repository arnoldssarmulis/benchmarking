﻿namespace InputDataToEntitiesMap.Config
{
    public enum InputScaleLevel
    {
        Percent10,
        Percent50,
        Percent100
    }
}
