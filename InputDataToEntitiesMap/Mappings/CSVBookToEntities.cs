﻿using InputData.FileHandlers;
using InputDataToEntitiesMap.Config;
using InputDataToEntitiesMap.TableReferences.Helpers;
using InputDataToEntitiesMap.TableReferences.Models;
using LibraryEntities;

namespace InputDataToEntitiesMap.Mappings
{
    public class CSVBookToEntities
    {
        private static HashSet<string> authorNames = new HashSet<string>();

        public static List<Author> authors = new();

        public static List<Book> books = new();

        public static List<BookAuthor> bookAuthors = new();

        public static List<AuthorFriendList> authorFriendList = new();

        public static List<InputData.Models.Book> csvData = new();

        public static void MapCsvBooksToEntityBooks()
        {
            csvData = CSVReader.ReadBooksFromCSV("GoodReads_100k_books.csv");
            csvData = InputScale.TakeConfiguredPercent(csvData);

            books = new List<Book>(csvData.Count);
            authors = new List<Author>(csvData.Count);
            authorNames = new HashSet<string>(csvData.Count);
            authorFriendList = new List<AuthorFriendList>(csvData.Count);
            bookAuthors = new List<BookAuthor>(csvData.Count);

            int authorId = 0;
            int bookId = 0;

            foreach (var csvBook in csvData)
            {
                var book = new Book
                {
                    Id = bookId++,
                    Title = csvBook.Title,
                    YearPublished = ExtractYearPublished(csvBook.Description),
                    FullDatePublished = DateTime.UtcNow,
                    Genre = csvBook.Genre,
                    AvarageRating = csvBook.Rating,
                    Pages = csvBook.Pages,
                    BookLink = csvBook.Link.Length > 200 ? csvBook.Link.Substring(0, 200) : csvBook.Link,
                    BookImageLink = csvBook.ImageLink.Length > 200 ? csvBook.ImageLink.Substring(0, 200) : csvBook.ImageLink
                };
                books.Add(book);

                if (!authorNames.Contains(csvBook.Author))
                {
                    var author = new Author
                    {
                        Id = authorId++,
                        Name = csvBook.Author.Length > 255 ? csvBook.Author.Substring(0, 255) : csvBook.Author,
                        BirthDate = GenerateRandomDate()
                    };

                    bool hasChild = random.Next(2) == 0;
                    if (hasChild)
                        author.ChildOfAuthorId = random.Next(authors.Count + 1);

                    authors.Add(author);
                    authorNames.Add(author.Name);
                }
            }

            CreateBookAuthors();
            CreateAuthorFriendsList();
            IndirectMerger.MergeReferencesBetweenBookAuthorsAndAuthorFriendList();
        }

        private static void CreateBookAuthors()
        {
            var bookAuthorCreateHelper = new TableReferences.RecordCreation<AuthorBookFK, BookAuthorFK>();

            bookAuthorCreateHelper.CreateNvsNRecord(bookAuthors,
                (int bookAuthorId, int bookId, int authorId) => new BookAuthor
                {
                    Id = bookAuthorId,
                    BookId = bookId,
                    AuthorId = authorId
                }
            );
        }

        private static void CreateAuthorFriendsList()
        {
            var authorFriendsCreateHelper = new TableReferences.RecordCreation<AuthorFriendsFK, AuthorFriendsFK>();

            authorFriendsCreateHelper.CreateNvsNRecord(authorFriendList,
                (int Id, int authorId, int friendsWithAuthorId) => new AuthorFriendList
                {
                    Id = Id,
                    AuthorId = authorId,
                    FriendsWithAuthorId = friendsWithAuthorId,
                    FriendsFrom = GenerateRandomDateTime(),
                    FriendsTo = new Random().Next(2) == 0 ? GenerateRandomDateTime() : null,
                    FriendshipType = GenerateRandomFriendshipType()
                }
            );
        }

        private static int ExtractYearPublished(string description)
        {
            var match = System.Text.RegularExpressions.Regex.Match(description, @"\b(19|20)\d{2}\b");
            return match.Success ? int.Parse(match.Value) : DateTime.Now.Year;
        }

        private static readonly Random random = new Random();

        public static string GenerateRandomDate()
        {
            DateTime startDate = new DateTime(1800, 1, 1);
            DateTime endDate = new DateTime(1990, 1, 1);

            int range = (endDate - startDate).Days;
            int randomDays = random.Next(range);

            DateTime randomDate = startDate.AddDays(randomDays);

            string formattedDate = randomDate.ToString("yyyy-MM-dd");

            return formattedDate;
        }

        private static DateTime GenerateRandomDateTime()
        {
            DateTime startDate = new DateTime(2000, 1, 1);
            int range = (DateTime.Today - startDate).Days;

            return startDate.AddDays(new Random().Next(range)).ToUniversalTime();
        }

        private static string GenerateRandomFriendshipType()
        {
            string[] friendshipTypes = { "Close Friend", "Acquaintance", "Colleague" };

            return friendshipTypes[new Random().Next(friendshipTypes.Length)];
        }
    }
}
