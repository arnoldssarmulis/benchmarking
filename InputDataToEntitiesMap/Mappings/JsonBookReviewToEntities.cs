﻿using InputData.FileHandlers;
using InputData.Models;
using InputDataToEntitiesMap.Config;
using InputDataToEntitiesMap.TableReferences.Helpers;
using LibraryEntities;
using System.Text;

namespace InputDataToEntitiesMap.Mappings
{
    public static class JsonBookReviewToEntities
    {
        private static ForeignKeyHelper _bookReview_BookIdFk_Helper;

        public static List<BookReview> bookReviews = new();

        public static List<Reviewer> reviewers = new();

        public static HashSet<Guid> reviewersIds = new HashSet<Guid>();

        public static void ConvertToBookReview()
        {
            var data = JsonReader.ReadJsonFile<Review>("BookRewiews.json");
            data = InputScale.TakeConfiguredPercent(data);

            bookReviews = new List<BookReview>(data.Count);
            reviewers = new List<Reviewer>(data.Count);
            reviewersIds = new HashSet<Guid>(data.Count);
            InitBookIdFk_Helper();

            foreach (var review in data)
            {
                if (review == null)
                    continue;

                var reviewerId = Guid.Parse(review.UserId);

                int? bookId = _bookReview_BookIdFk_Helper.GetNewForeignKey();

                if (bookId.HasValue)
                    bookReviews.Add(new BookReview
                    {
                        Id = Guid.NewGuid(),
                        Timestamp = DateTime.Parse(review.Timestamp).ToUniversalTime(),
                        ReviewText = FilterReviewText(review.ReviewSentences),
                        Rating = review.Rating,
                        BookId = bookId.Value,
                        ReviewerId = reviewerId
                    });

                if (!reviewersIds.Contains(reviewerId))
                {
                    reviewers.Add(new Reviewer
                    {
                        Id = reviewerId,
                        Name = GetName(),
                        Surname = GetSurname(),
                    });

                    reviewersIds.Add(reviewerId);
                }
            }

            IndirectMerger.MergeReferencesBetweenBooksAndBookReviews();
            Console.WriteLine("\n EOF JSON data: " + DateTime.UtcNow);
        }

        private static string FilterReviewText(List<string> reviewSentences)
        {
            int maxReviewTextLength = 2000;
            int currentLength = 0;
            StringBuilder reviewTxt = new(2001);

            reviewSentences.ForEach(review =>
            {
                foreach (var ch in review)
                {
                    if (currentLength >= maxReviewTextLength)
                        return;

                    if (char.IsAscii(ch))
                    {
                        reviewTxt.Append(ch);
                        currentLength++;
                    }
                }

                if (currentLength < maxReviewTextLength)
                {
                    reviewTxt.Append(' ');
                    currentLength++;
                }
            });

            return reviewTxt.ToString();
        }

        private static void InitBookIdFk_Helper()
        {
            int minReviewCountPerBook = ForeignKeyCountsHelper.GetMinMaxBookFkCountPerUnit().Item1;
            int maxReviewCountPerBook = ForeignKeyCountsHelper.GetMinMaxBookFkCountPerUnit().Item2;

            int totalBookCount = CSVBookToEntities.books.Count;

            _bookReview_BookIdFk_Helper = new ForeignKeyHelper(totalBookCount, minReviewCountPerBook, maxReviewCountPerBook, totalBookCount);
        }

        private static readonly List<string> Names = new List<string>
        {
            "John", "Emma", "Michael", "Olivia", "William", "Sophia",
            "James", "Isabella", "Benjamin", "Charlotte", "Lucas", "Amelia",
            "Henry", "Mia", "Alexander", "Harper", "Sebastian", "Evelyn",
            "Jackson", "Avery"
        };

        private static readonly Random randomName = new Random();

        private static readonly List<string> Surnames = new List<string>
        {
            "Smith", "Johnson", "Williams", "Brown", "Jones",
            "Garcia", "Miller", "Davis", "Rodriguez", "Martinez",
            "Hernandez", "Lopez", "Gonzalez", "Wilson", "Anderson",
            "Thomas", "Taylor", "Moore", "Jackson", "Martin"
        };

        private static readonly Random randomSurname = new Random();

        private static string GetSurname()
        {
            int index = randomSurname.Next(Surnames.Count);
            return Surnames[index];
        }

        private static string GetName()
        {
            int index = randomName.Next(Names.Count);
            return Names[index];
        }

    }
}
