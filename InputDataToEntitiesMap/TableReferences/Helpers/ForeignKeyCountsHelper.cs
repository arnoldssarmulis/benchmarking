﻿using InputDataToEntitiesMap.Config;

namespace InputDataToEntitiesMap.TableReferences.Helpers
{
    public static class ForeignKeyCountsHelper
    {
        private static readonly InputScaleLevel _inputScale = InputScale.InputDataScaleLevel;

        private static readonly double relationshipScale = InputScale.ReferenceScaleLevel;

        private static int GetWithScale(int count) => (int)(count * relationshipScale);

        public static (int, int) GetMinMaxBookFkCountPerUnit()
        {
            int minReviewCountPerBook = 5;
            int maxReviewCountPerBook = 5;

            if (_inputScale == InputScaleLevel.Percent10)
            {
                minReviewCountPerBook = GetWithScale(10);
                maxReviewCountPerBook = GetWithScale(20);
            }
            else if (_inputScale == InputScaleLevel.Percent50)
            {
                minReviewCountPerBook = GetWithScale(30);
                maxReviewCountPerBook = GetWithScale(100);
            }
            else if (_inputScale == InputScaleLevel.Percent100)
            {
                minReviewCountPerBook = GetWithScale(30);
                maxReviewCountPerBook = GetWithScale(200);
            }

            return (minReviewCountPerBook, maxReviewCountPerBook);
        }

        public static (int, int) GetMinMaxAuthorBookFkCountPerUnit()
        {
            int minAuthorBookCount = 5;
            int maxAuthorBookCount = 5;

            if (_inputScale == InputScaleLevel.Percent10)
            {
                minAuthorBookCount = GetWithScale(30);
                maxAuthorBookCount = GetWithScale(100);
            }
            else if (_inputScale == InputScaleLevel.Percent50)
            {
                minAuthorBookCount = GetWithScale(100);
                maxAuthorBookCount = GetWithScale(300);
            }
            else if (_inputScale == InputScaleLevel.Percent100)
            {
                minAuthorBookCount = GetWithScale(200);
                maxAuthorBookCount = GetWithScale(500);
            }

            return (minAuthorBookCount, maxAuthorBookCount);
        }

        public static (int, int) GetMinMaxBookAuthorFkCountPerUnit()
        {
            int minBookAuthorCount = 5;
            int maxBookAuthorCount = 5;

            if (_inputScale == InputScaleLevel.Percent10)
            {
                minBookAuthorCount = GetWithScale(30);
                maxBookAuthorCount = GetWithScale(100);
            }
            else if (_inputScale == InputScaleLevel.Percent50)
            {
                minBookAuthorCount = GetWithScale(100);
                maxBookAuthorCount = GetWithScale(300);
            }
            else if (_inputScale == InputScaleLevel.Percent100)
            {
                minBookAuthorCount = GetWithScale(200);
                maxBookAuthorCount = GetWithScale(500);
            }

            return (minBookAuthorCount, maxBookAuthorCount);
        }

        public static (int, int) GetMinMaxAuthorFriendsCountPerUnit()
        {
            int minAuthorFriendsCount = 5;
            int maxAuthorFriendsCount = 5;

            if (_inputScale == InputScaleLevel.Percent10)
            {
                minAuthorFriendsCount = GetWithScale(30);
                maxAuthorFriendsCount = GetWithScale(50);
            }
            else if (_inputScale == InputScaleLevel.Percent50)
            {
                minAuthorFriendsCount = GetWithScale(50);
                maxAuthorFriendsCount = GetWithScale(100);
            }
            else if (_inputScale == InputScaleLevel.Percent100)
            {
                minAuthorFriendsCount = GetWithScale(100);
                maxAuthorFriendsCount = GetWithScale(300);
            }

            return (minAuthorFriendsCount, maxAuthorFriendsCount);
        }

        //public static (int, int) GetMinMaxAuthorBookCountPerUnit()
        //{
        //    int minAuthorBookCount = 5;
        //    int maxAuthorBookCount = 5;

        //    if (_inputScale == InputScaleLevel.Percent10)
        //    {
        //        minAuthorBookCount = 10;
        //        maxAuthorBookCount = 50;
        //    }
        //    else if (_inputScale == InputScaleLevel.Percent50)
        //    {
        //        minAuthorBookCount = 50;
        //        maxAuthorBookCount = 250;
        //    }
        //    else if (_inputScale == InputScaleLevel.Percent100)
        //    {
        //        minAuthorBookCount = 100;
        //        maxAuthorBookCount = 500;
        //    }

        //    return (minAuthorBookCount, maxAuthorBookCount);
        //}

        //public static int GetAuthorBookCount()
        //{
        //    int maxBookAuthorCount = (int)(CSVBookToEntities.csvData.Count * 1.5);

        //    if (CSVBookToEntities.bookAuthors.Count >= maxBookAuthorCount)
        //        return 0;

        //    int bookMaxCount = 10;
        //    int bookMinCount = 0;

        //    if (_inputScale == InputScaleLevel.Percent10)
        //    {
        //        bookMinCount = 10;
        //        bookMaxCount = 50;
        //    }
        //    else if (_inputScale == InputScaleLevel.Percent50)
        //    {
        //        bookMinCount = 50;
        //        bookMaxCount = 250;
        //    }
        //    else if (_inputScale == InputScaleLevel.Percent100)
        //    {
        //        bookMinCount = 100;
        //        bookMaxCount = 500;
        //    }

        //    return new Random().Next(bookMinCount, bookMaxCount);
        //}

        public static int GenerateFriendListCount()
        {
            int maxFriendListCount = 5;

            if (_inputScale == InputScaleLevel.Percent10)
            {
                maxFriendListCount = GetWithScale(30);
            }
            else if (_inputScale == InputScaleLevel.Percent50)
            {
                maxFriendListCount = GetWithScale(50);
            }
            else if (_inputScale == InputScaleLevel.Percent100)
            {
                maxFriendListCount = GetWithScale(100);
            }

            return new Random().Next(maxFriendListCount);
        }

    }
}
