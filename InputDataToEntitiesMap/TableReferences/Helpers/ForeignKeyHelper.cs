﻿using InputDataToEntitiesMap.TableReferences.Models;
using System;
using System.Linq;

namespace InputDataToEntitiesMap.TableReferences.Helpers
{
    public class ForeignKeyHelper
    {
        private readonly int _totalObjectCount = 100000;
        public readonly int _maxFkCountPerUnit = 100;
        private readonly int _minFkPerUnit = 10;
        private readonly int _keyDeliveryCount = 100000;

        private int _totalUsedKeyCount = 0;

        private readonly Random _random = new();

        private Dictionary<int, int> _foreignKeyMap;
        private List<int> _keysArray;

        public ForeignKeyHelper(int totalObjectCount, int minFkPerUnit, int maxFkPerUnit, int keyDeliveryCount)
        {
            _totalObjectCount = totalObjectCount;
            _minFkPerUnit = minFkPerUnit;

            if (_minFkPerUnit <= 0)
                _minFkPerUnit = 1;

            _maxFkCountPerUnit = maxFkPerUnit;
            _keyDeliveryCount = keyDeliveryCount;

            InitForeignKeyMap();
        }

        public ForeignKeyHelper(int totalObjectCount, (int minFkPerUnit, int maxFkPerUnit) fkLimits, int keyDeliveryCount)
        {
            _totalObjectCount = totalObjectCount;
            _minFkPerUnit = fkLimits.minFkPerUnit;

            if (_minFkPerUnit <= 0)
                _minFkPerUnit = 1;

            _maxFkCountPerUnit = fkLimits.maxFkPerUnit;
            _keyDeliveryCount = keyDeliveryCount;

            InitForeignKeyMap();
        }

        public ForeignKeyHelper(ForeignKeySettings foreignKeySettings)
        {
            _totalObjectCount = foreignKeySettings.TotalObjectCount;
            _minFkPerUnit = foreignKeySettings.MinFkCountPerUnit;

            if (_minFkPerUnit <= 0)
                _minFkPerUnit = 1;

            _maxFkCountPerUnit = foreignKeySettings.MaxFkCountPerUnit;
            _keyDeliveryCount = foreignKeySettings.KeyDeliveryCount;

            InitForeignKeyMap();
        }

        public int? GetNewForeignKey(HashSet<int> usedKeySet = null)
        {
            if (_totalUsedKeyCount > _keyDeliveryCount || _foreignKeyMap.Count == 0)
                return null;

            _totalUsedKeyCount++;

            int newKeyIndex = _random.Next(_foreignKeyMap.Count);
            int key = _keysArray[newKeyIndex];

            while (!IsKeyAvaliable(key) || (usedKeySet?.Contains(key) ?? false))
            {
                if (!IsKeyAvaliable(key))
                {
                    _foreignKeyMap.Remove(key);
                    _keysArray.RemoveAt(newKeyIndex);
                }
                if (_foreignKeyMap.Count < 1)
                    return null;

                //if (HandleAllKeysUsedScenariou(usedKeySet) == 1)
                //    return null;
                int? newKey = HandleAllKeysUsedScenariou(usedKeySet);
                if (newKey.HasValue)
                {
                    key = newKey.Value;
                    break;
                }

                newKeyIndex = _random.Next(_foreignKeyMap.Count);
                key = _keysArray[newKeyIndex];
            }

            _foreignKeyMap[key]--;
            return key;
        }

        private bool IsKeyAvaliable(int keyId) => _foreignKeyMap[keyId] > 0;

        private void InitForeignKeyMap()
        {
            if (_totalObjectCount < _maxFkCountPerUnit)
                throw new Exception("_totalObjectCount must be larger than _maxFkCountPerUnit");

            int totalKeyCount = 0;

            _keysArray = new List<int>((int)(_keyDeliveryCount));
            _foreignKeyMap = new Dictionary<int, int>((int)(_keyDeliveryCount));

            Random random = new Random();

            while (totalKeyCount < _keyDeliveryCount)
            {
                int nextForeignKey = random.Next(_totalObjectCount);

                if (!_foreignKeyMap.ContainsKey(nextForeignKey))
                {
                    int keyCountLeftForUse = random.Next(_minFkPerUnit, _maxFkCountPerUnit + 1);

                    totalKeyCount += keyCountLeftForUse;

                    _foreignKeyMap.Add(nextForeignKey, keyCountLeftForUse);
                    _keysArray.Add(nextForeignKey);
                }
            }
        }

        private int? HandleAllKeysUsedScenariou(HashSet<int> usedKeySet = null) 
        {
            //nomainīt atslēgu ar citu
            //atgriezt vnk null
            if (usedKeySet == null)
                return null;

            bool isAllKeysUsed = _foreignKeyMap.Keys.All(key => usedKeySet.Contains(key));

            if (isAllKeysUsed)
            {
                Random random = new Random();

                int newKeyIndex = _random.Next(_foreignKeyMap.Count);
                int key = _keysArray[newKeyIndex];

                int keyCountLeft = _foreignKeyMap[key];
                _foreignKeyMap.Remove(key);
                _keysArray.Remove(key);

                int newForeignKey = random.Next(_totalObjectCount);

                while (_foreignKeyMap.ContainsKey(newForeignKey))
                    newForeignKey = random.Next(_totalObjectCount);

                _foreignKeyMap[newForeignKey] = keyCountLeft;
                _keysArray.Add(newForeignKey);

                return newForeignKey;
            }

            return null;
        }
    }
}
