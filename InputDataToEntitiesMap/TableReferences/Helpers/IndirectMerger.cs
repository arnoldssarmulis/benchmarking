﻿using InputDataToEntitiesMap.Config;
using InputDataToEntitiesMap.Mappings;
using LibraryEntities;

namespace InputDataToEntitiesMap.TableReferences.Helpers
{
    internal static class IndirectMerger
    {
        public static void MergeReferencesBetweenBookAuthorsAndAuthorFriendList()
        {
            Random random = new Random();
            var usedFriendAuthorsId = new HashSet<int>();

            var bookAuthorsIds = CSVBookToEntities.bookAuthors
                .GroupBy(ba => ba.AuthorId)
                .Select(ba => ba.Key)
                .ToList();
            
            var authorFriendList = CSVBookToEntities.authorFriendList;
            int friendAuthorsIdCount = authorFriendList.GroupBy(ba => ba.AuthorId).Count();

            int mergeCount = bookAuthorsIds.Count;
            if (friendAuthorsIdCount < mergeCount)
                mergeCount = friendAuthorsIdCount;

            mergeCount = (int)(mergeCount * InputScale.GetMultiplier);

            bookAuthorsIds.ForEach(authorId =>
            {
                if (usedFriendAuthorsId.Count >= friendAuthorsIdCount || mergeCount <= 0)
                    return;

                int selectedRecordId = random.Next(authorFriendList.Count);
                int selectedAuthorId = authorFriendList[selectedRecordId].AuthorId;

                while (usedFriendAuthorsId.Contains(selectedAuthorId))
                {
                    selectedRecordId = random.Next(authorFriendList.Count);
                    selectedAuthorId = authorFriendList[selectedRecordId].AuthorId;
                }

                if (!usedFriendAuthorsId.Contains(selectedAuthorId))
                    usedFriendAuthorsId.Add(selectedAuthorId);

                foreach (var afl in authorFriendList.Where(afl => afl.AuthorId == selectedAuthorId))
                {
                    afl.AuthorId = authorId;
                }

                mergeCount--;
            });
        }

        public static void MergeReferencesBetweenBooksAndBookReviews()
        {
            if (InputScale.ReferenceScaleLevel != 10)
                return;

            Random random = new Random();
            var usedBooksId = new HashSet<int>();

            var bookAuthors = CSVBookToEntities.bookAuthors;
            var bookReviews = JsonBookReviewToEntities.bookReviews;
            var booksInBookReviews = bookReviews.GroupBy(ba => ba.BookId)
                .Select(ba => ba.Key)
                .ToList();

            var booksInBookAuthors = bookAuthors.GroupBy(ba => ba.BookId)
                .Select(ba => ba.Key)
                .ToList();

            booksInBookReviews.ForEach(ba =>
            {
                if (usedBooksId.Count >= booksInBookAuthors.Count)
                    return;

                int selectedRecordId = random.Next(booksInBookAuthors.Count);
                int selectedBookId = booksInBookAuthors[selectedRecordId];

                while (usedBooksId.Contains(selectedBookId))
                {
                    selectedRecordId = random.Next(booksInBookAuthors.Count);
                    selectedBookId = booksInBookAuthors[selectedRecordId];
                }

                usedBooksId.Add(selectedBookId);

                foreach (var afl in bookAuthors.Where(br => br.BookId == selectedBookId))
                {
                    afl.BookId = ba;
                }
            });
        }
    }
}
