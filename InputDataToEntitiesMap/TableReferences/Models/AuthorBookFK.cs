﻿using InputDataToEntitiesMap.Mappings;
using InputDataToEntitiesMap.TableReferences.Helpers;

namespace InputDataToEntitiesMap.TableReferences.Models
{
    public class AuthorBookFK : ForeignKeySettings
    {
        public AuthorBookFK()
        {
            KeyDeliveryCount = (int)(CSVBookToEntities.books.Count * 1.5);
            var authorBookKeyLimits = ForeignKeyCountsHelper.GetMinMaxAuthorBookFkCountPerUnit();

            MinFkCountPerUnit = authorBookKeyLimits.Item1;
            MaxFkCountPerUnit = authorBookKeyLimits.Item2;

            TotalObjectCount = CSVBookToEntities.authors.Count; //CSVBookToEntities.books.Count;
        }
    }
}
