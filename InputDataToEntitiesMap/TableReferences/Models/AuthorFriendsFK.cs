﻿using InputDataToEntitiesMap.Mappings;
using InputDataToEntitiesMap.TableReferences.Helpers;

namespace InputDataToEntitiesMap.TableReferences.Models
{
    public class AuthorFriendsFK : ForeignKeySettings
    {
        public AuthorFriendsFK()
        {
            KeyDeliveryCount = CSVBookToEntities.authors.Count;
            var authorBookKeyLimits = ForeignKeyCountsHelper.GetMinMaxAuthorBookFkCountPerUnit();

            MinFkCountPerUnit = authorBookKeyLimits.Item1;
            MaxFkCountPerUnit = authorBookKeyLimits.Item2;

            TotalObjectCount = CSVBookToEntities.authors.Count;
        }
    }
}
