﻿using InputDataToEntitiesMap.Mappings;
using InputDataToEntitiesMap.TableReferences.Helpers;

namespace InputDataToEntitiesMap.TableReferences.Models
{
    public class BookAuthorFK : ForeignKeySettings
    {
        public BookAuthorFK()
        {
            KeyDeliveryCount = (int)(CSVBookToEntities.books.Count * 1.5);

            var bookAuthorKeyLimits = ForeignKeyCountsHelper.GetMinMaxBookAuthorFkCountPerUnit();

            MinFkCountPerUnit = bookAuthorKeyLimits.Item1;
            MaxFkCountPerUnit = bookAuthorKeyLimits.Item2;

            TotalObjectCount = CSVBookToEntities.authors.Count;
        }
    }
}
