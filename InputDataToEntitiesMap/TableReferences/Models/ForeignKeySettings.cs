﻿namespace InputDataToEntitiesMap.TableReferences.Models
{
    public abstract class ForeignKeySettings
    {
        public int TotalObjectCount { get; set; }

        public int MinFkCountPerUnit { get; set; }

        public int MaxFkCountPerUnit { get; set; }

        public int KeyDeliveryCount { get; set; }
    }
}
