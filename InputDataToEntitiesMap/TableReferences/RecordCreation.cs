﻿using InputDataToEntitiesMap.Config;
using InputDataToEntitiesMap.TableReferences.Helpers;
using InputDataToEntitiesMap.TableReferences.Models;

namespace InputDataToEntitiesMap.TableReferences
{
    public class RecordCreation<TTableOne, TTableTwo>
        where TTableOne : ForeignKeySettings, new()
        where TTableTwo : ForeignKeySettings, new()
    {
        private readonly int totalObjectCount;

        private ForeignKeyHelper _firstTableHelper;
        private ForeignKeyHelper _secondTableOneHelper;

        private readonly Dictionary<int, HashSet<int>> _usedKeySets;

        public RecordCreation()
        {
            TTableOne tableOneInstance = new TTableOne();
            TTableTwo tableTwoInstance = new TTableTwo();

            totalObjectCount = tableOneInstance.KeyDeliveryCount;
            if (tableTwoInstance.KeyDeliveryCount < totalObjectCount)
                totalObjectCount = tableTwoInstance.KeyDeliveryCount;

            _firstTableHelper = new ForeignKeyHelper(tableOneInstance);
            _secondTableOneHelper = new ForeignKeyHelper(tableTwoInstance);

            _usedKeySets = new Dictionary<int, HashSet<int>>(totalObjectCount);
        }

        public void CreateNvsNRecord<TCollection>(List<TCollection> collection, Func<int, int, int, TCollection> addToCollection)
        {
            int PrimaryKeyId = 0;

            for (int i = 0; i < totalObjectCount; i++)
            {
                //SwitchHelpers();
                //HandleKeys(collection, addToCollection, ref PrimaryKeyId, _firstTableHelper, _secondTableOneHelper);

                int? firstFkId = _firstTableHelper.GetNewForeignKey();
                if (!firstFkId.HasValue)
                    continue;

                _usedKeySets.TryGetValue(firstFkId.Value, out HashSet<int> usedKeySet);

                int? secondFkId = _secondTableOneHelper.GetNewForeignKey(usedKeySet);
                if (!secondFkId.HasValue)
                    continue;

                collection.Add(addToCollection(PrimaryKeyId++, firstFkId.Value, secondFkId.Value));

                bool isKeyInDict = _usedKeySets.TryGetValue(firstFkId.Value, out usedKeySet);

                if (!isKeyInDict)
                {
                    _usedKeySets.Add(firstFkId.Value,
                        new HashSet<int>(_secondTableOneHelper._maxFkCountPerUnit) { secondFkId.Value }
                    );
                }
                else
                {
                    usedKeySet.Add(secondFkId.Value);
                }
            }

            //SwitchHelpers();
            //for (int i = 0; i < totalObjectCount; i++)
            //{
            //    //SwitchHelpers();
            //    //HandleKeys(collection, addToCollection, ref PrimaryKeyId, _firstTableHelper, _secondTableOneHelper);

            //    int? firstFkId = _firstTableHelper.GetNewForeignKey();
            //    if (!firstFkId.HasValue)
            //        continue;

            //    _usedKeySets.TryGetValue(firstFkId.Value, out HashSet<int> usedKeySet);

            //    int? secondFkId = _secondTableOneHelper.GetNewForeignKey(usedKeySet);
            //    if (!secondFkId.HasValue)
            //        continue;

            //    collection.Add(addToCollection(PrimaryKeyId++, firstFkId.Value, secondFkId.Value));

            //    bool isKeyInDict = _usedKeySets.TryGetValue(firstFkId.Value, out usedKeySet);

            //    if (!isKeyInDict)
            //    {
            //        _usedKeySets.Add(firstFkId.Value,
            //            new HashSet<int>(_secondTableOneHelper._maxFkCountPerUnit) { secondFkId.Value }
            //        );
            //    }
            //    else
            //    {
            //        usedKeySet.Add(secondFkId.Value);
            //    }
            //}
        }

        private void SwitchHelpers()
        {
            //if (PrimaryKeyId % 2 == 0)
            //    return;
                
            var temp = _secondTableOneHelper;

            _secondTableOneHelper = _firstTableHelper;

            _firstTableHelper = temp;
        }

        private void HandleKeys<TCollection>(List<TCollection> collection, Func<int, int, int, TCollection> addToCollection, 
            ref int PrimaryKeyId, ForeignKeyHelper firstTableHelper, ForeignKeyHelper secondTableHelper)
        {
            int? firstFkId = firstTableHelper.GetNewForeignKey();
            if (!firstFkId.HasValue)
                return;

            _usedKeySets.TryGetValue(firstFkId.Value, out HashSet<int> usedKeySet);

            int? secondFkId = secondTableHelper.GetNewForeignKey(usedKeySet);
            if (!secondFkId.HasValue)
                return;

            collection.Add(addToCollection(PrimaryKeyId++, firstFkId.Value, secondFkId.Value));

            bool isKeyInDict = _usedKeySets.TryGetValue(firstFkId.Value, out usedKeySet);

            if (!isKeyInDict)
            {
                _usedKeySets.Add(firstFkId.Value,
                    new HashSet<int>(secondTableHelper._maxFkCountPerUnit) { secondFkId.Value }
                );
            }
            else
            {
                usedKeySet.Add(secondFkId.Value);
            }
        }

        private void HandleReferenceScale(ForeignKeySettings tableOneInstance)
        {
            //double relationshipScale = InputScale.ReferenceScaleLevel * 25;

            var division = (double)tableOneInstance.KeyDeliveryCount / (double)tableOneInstance.TotalObjectCount;

            var ceiling = Math.Ceiling(division) + 1;

            tableOneInstance.MinFkCountPerUnit = (int)ceiling; //(int)(tableOneInstance.MinFkCountPerUnit / relationshipScale);
            tableOneInstance.MaxFkCountPerUnit = (int)ceiling + 1; //(int)(tableOneInstance.MaxFkCountPerUnit / relationshipScale);
        }
    }
}
