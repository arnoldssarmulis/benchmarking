﻿namespace LibraryEntities
{
    public class Author
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string BirthDate { get; set; }

        public Author ChildOfAuthor { get; set; }

        public int? ChildOfAuthorId { get; set; }

        public ICollection<BookAuthor> BookAuthors { get; set; }

        public ICollection<AuthorFriendList> Friends { get; set; }

        public ICollection<Author> Childrens { get; set; }
    }
}
