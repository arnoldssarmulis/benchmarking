﻿namespace LibraryEntities
{
    public class AuthorFriendList
    {
        public int Id { get; set; }

        public int AuthorId { get; set; }

        public Author Author { get; set; }

        public int FriendsWithAuthorId { get; set; }

        public Author FriendsWithAuthor { get; set; }

        public DateTime FriendsFrom { get; set; }

        public DateTime? FriendsTo { get; set; }

        public string FriendshipType { get; set; }
    }
}
