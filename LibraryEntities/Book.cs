﻿namespace LibraryEntities
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int YearPublished { get; set; }

        public DateTime FullDatePublished { get; set; }

        public string Genre { get; set; }

        public double? AvarageRating { get; set; }

        public int Pages { get; set; }

        public string BookLink { get; set; }

        public string BookImageLink { get; set; }

        public ICollection<BookAuthor> BookAuthors { get; set; }

        public ICollection<BookReview> BookReviews { get; set; }
    }
}
