﻿namespace LibraryEntities
{
    public class BookReview
    {
        public Guid Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string ReviewText { get; set; }

        public int? Rating { get; set; }

        public int BookId { get; set; }

        public Guid ReviewerId { get; set; }

        public Book Book { get; set; }

        public Reviewer Reviewer { get; set; }
    }
}
