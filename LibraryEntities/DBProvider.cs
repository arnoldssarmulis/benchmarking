﻿using LibraryEntities.Helpers;

namespace LibraryEntities
{
    public static class DBProvider
    {
        public static DatabaseProviderType ProviderType { get; set; }

        static DBProvider()
        {
            var config = Configuration.GetConfiguration;
            var databaseProvider = config.GetSection("ActiveDatabaseProvider").Value;

            switch (databaseProvider)
            {
                case nameof(DatabaseProviderType.MSSQLServer):
                    ProviderType = DatabaseProviderType.MSSQLServer;
                    return;

                case nameof(DatabaseProviderType.PostgreSQL):
                    ProviderType = DatabaseProviderType.PostgreSQL;
                    return;

                case nameof(DatabaseProviderType.MySQL):
                    ProviderType = DatabaseProviderType.MySQL;
                    return;
            }
        }

        public static bool IsMSSQLServer
        {
            get => ProviderType == DatabaseProviderType.MSSQLServer;
        }

        public static bool IsPostgreSQL
        {
            get => ProviderType == DatabaseProviderType.PostgreSQL;
        }

        public static bool IsMySQL
        {
            get => ProviderType == DatabaseProviderType.MySQL;
        }

        public static string ConnectionStringName
        {
            get
            {
                switch (ProviderType)
                {
                    case DatabaseProviderType.MSSQLServer:
                        return "MSSQLServerConnection";

                    case DatabaseProviderType.PostgreSQL:
                        return "PostgreSQLConnection";

                    case DatabaseProviderType.MySQL:
                        return "MySQLConnection";

                    default:
                        throw new Exception();
                }
            }
        }
    }
}
