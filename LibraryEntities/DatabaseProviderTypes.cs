﻿namespace LibraryEntities
{
    public enum DatabaseProviderType
    {
        MSSQLServer,
        PostgreSQL,
        MySQL
    }
}
