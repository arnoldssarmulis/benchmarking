﻿using Microsoft.Extensions.Caching.Memory;

namespace InputDataToEntitiesMap.Helpers
{
    public class CollectionCacheHelper<T>
    {
        private readonly IMemoryCache _memoryCache;

        public CollectionCacheHelper(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void CacheCollection(string key, IEnumerable<T> collection, TimeSpan expiration)
        {
            _memoryCache.Set(key, collection, expiration);
        }

        public IEnumerable<T> RetrieveCachedCollection(string key)
        {
            if (_memoryCache.TryGetValue(key, out IEnumerable<T> cachedCollection))
            {
                return cachedCollection;
            }
            else
            {
                return null;
            }
        }
    }
}
