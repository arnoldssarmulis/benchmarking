﻿using LibraryEntities.TableReferences.Models;

namespace LibraryEntities.Helpers
{
    public class ForeignKeyHelper
    {
        private readonly int _totalObjectCount = 100000;
        private readonly int _maxFkCountPerUnit = 100;
        private readonly int _minFkPerUnit = 10;
        private readonly int _keyDeliveryCount = 100000;

        private int _prevUsedKeyId = 0;
        private int _totalUsedKeyCount = 0;

        private readonly Random _random = new();

        private Dictionary<int, int> _foreignKeyMap = new Dictionary<int, int>();
        private List<int> _keysArray;

        public ForeignKeyHelper(int totalObjectCount, int minFkPerUnit, int maxFkPerUnit, int keyDeliveryCount)
        {
            _totalObjectCount = totalObjectCount;
            _minFkPerUnit = minFkPerUnit;
            _maxFkCountPerUnit = maxFkPerUnit;
            _keyDeliveryCount = keyDeliveryCount;

            InitForeignKeyMap();
        }

        public ForeignKeyHelper(int totalObjectCount, (int minFkPerUnit, int maxFkPerUnit) fkLimits, int keyDeliveryCount)
        {
            _totalObjectCount = totalObjectCount;
            _minFkPerUnit = fkLimits.minFkPerUnit;
            _maxFkCountPerUnit = fkLimits.maxFkPerUnit;
            _keyDeliveryCount = keyDeliveryCount;

            InitForeignKeyMap();
        }

        public ForeignKeyHelper(ForeignKeySettings foreignKeySettings)
        {
            _totalObjectCount = foreignKeySettings.TotalObjectCount;
            _minFkPerUnit = foreignKeySettings.MinFkCountPerUnit;
            _maxFkCountPerUnit = foreignKeySettings.MaxFkCountPerUnit;
            _keyDeliveryCount = foreignKeySettings.KeyDeliveryCount;

            InitForeignKeyMap();
        }

        /// <summary>
        /// Izsniedz jaunu atslēgu ja tai noteiktais skaits nav pārsniegts
        /// Kad visām atslēgām pieejamie skaiti iztērēti atgriež null
        /// </summary>
        /// <returns></returns>
        public int? GetNewForeignKey()
        {
            if (_totalUsedKeyCount > _keyDeliveryCount)
                return null;

            _totalUsedKeyCount++;

            int newKeyIndex = _random.Next(_foreignKeyMap.Count);

            int key = _keysArray[newKeyIndex];

            if (IsKeyAvaliable(key))
            {
                _foreignKeyMap[key]--;

                return key;
            }

            int i = 0;
            //Kad ar randomu pirmā piegājienā neizdodas atrast tad paņem nākamo pirmo pieejamo kas nav iepriekš ņemta
            for (; i < _keysArray.Count && 
                    (!IsKeyAvaliable(_keysArray[i]) || _prevUsedKeyId == _keysArray[i]); i++) ;

            if (i == _foreignKeyMap.Count && IsKeyAvaliable(_prevUsedKeyId))
            {
                _foreignKeyMap[_prevUsedKeyId]--;
                return _prevUsedKeyId;
            }
            if (i == _foreignKeyMap.Count && !IsKeyAvaliable(_prevUsedKeyId))
            {
                _totalUsedKeyCount = _keyDeliveryCount;
                return null;
            }

            _prevUsedKeyId = _keysArray[i];
            _foreignKeyMap[_prevUsedKeyId]--;

            return _prevUsedKeyId;
        }

        private bool IsKeyAvaliable(int newKeyId) => _foreignKeyMap[newKeyId] > 0;

        private void InitForeignKeyMap()
        {
            if (_totalObjectCount < _maxFkCountPerUnit)
                throw new Exception("_totalObjectCount must be larger than _maxFkCountPerUnit");

            int totalKeyCount = 0;

            _keysArray = new List<int>();

            Random random = new Random();

            while(totalKeyCount < _keyDeliveryCount)
            {
                int nextForeignKey = random.Next(_totalObjectCount);

                if (!_foreignKeyMap.ContainsKey(nextForeignKey))
                {
                    int keyCountLeftForUse = random.Next(_minFkPerUnit, _maxFkCountPerUnit);

                    totalKeyCount += keyCountLeftForUse;

                    _foreignKeyMap.Add(nextForeignKey, keyCountLeftForUse);
                    _keysArray.Add(nextForeignKey);
                }
            }
        }
    }
}
