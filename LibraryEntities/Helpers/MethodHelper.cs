﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibraryEntities.Helpers
{
    public static class MethodHelper
    {
        public static void CallStaticMethod(string className, string methodName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            Type classType = assembly.GetType(className);

            if (classType == null)
            {
                throw new Exception($"Class '{className}' not found.");
            }

            MethodInfo method = classType.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);

            if (method == null)
            {
                throw new Exception($"Method '{methodName}' not found in class '{className}'.");
            }

            method.Invoke(null, null);
        }
    }
}
