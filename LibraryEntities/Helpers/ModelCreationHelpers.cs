﻿using LibraryEntities.Config;

namespace LibraryEntities.Helpers
{
    internal static class ModelCreation
    {
        public static string GetVarcharColumn(int maxSize)
        {
            return $"{(DBProvider.IsMSSQLServer ? "n" : "")}varchar({maxSize})";
        }
    }
}
