﻿using InputData;
using InputData.FileHandlers;
using LibraryEntities.Entities;
using LibraryEntities.Helpers;
using LibraryEntities.TableReferences;

namespace LibraryEntities.Mapper
{
    public class CSVBookToEntities
    {
        private static HashSet<string> authorNames = new HashSet<string>();

        public static List<Author> authors = new();

        public static List<Book> books = new();

        public static List<BookAuthor> bookAuthors = new();

        public static List<AuthorFriendList> authorFriendList = new();

        public static List<InputData.Models.Book> csvData = new();

        public static void MapCsvBooksToEntityBooks()
        {
            if (authors.Count > 0)
                return;

            Console.WriteLine("\n readingCSV data: " + DateTime.Now);
            csvData = CSVReader.ReadBooksFromCSV("GoodReads_100k_books.csv");
            csvData = InputScale.TakeConfiguredPercent(csvData);
            Console.WriteLine(" EOF readingCSV data: " + DateTime.Now + "\n");

            int bookAuthorId = 0;
            int authorId = 0;
            int bookId = 0;

            foreach (var csvBook in csvData)
            {
                var book = new Book
                {
                    Id = bookId++,
                    Title = csvBook.Title,
                    YearPublished = ExtractYearPublished(csvBook.Description),
                    FullDatePublished = DateTime.UtcNow, 
                    Genre = csvBook.Genre,
                    AvarageRating = csvBook.Rating,
                    Pages = csvBook.Pages,
                    BookLink = csvBook.Link.Length > 200 ? csvBook.Link.Substring(0, 200) : csvBook.Link,
                    BookImageLink = csvBook.ImageLink.Length > 200 ? csvBook.ImageLink.Substring(0, 200) : csvBook.ImageLink
                };
                books.Add(book);

                if (!authorNames.Contains(csvBook.Author))
                {
                    var author = new Author
                    {
                        Id = authorId++,
                        Name = csvBook.Author.Length > 255 ? csvBook.Author.Substring(0, 255) : csvBook.Author,
                        BirthDate = GenerateRandomDate()
                    };

                    bool hasChild = random.Next(2) == 0;
                    if (hasChild)
                        author.ChildOfAuthorId = random.Next(authors.Count + 1);

                    authors.Add(author);
                    authorNames.Add(author.Name);
                }

                //for (int i = 0; i < ForeignKeyCountsHelper.GetAuthorBookCount(); i++)
                //{
                //    bookAuthors.Add(new BookAuthor
                //    {
                //        Id = bookAuthorId++,
                //        BookId = new Random().Next(bookId - 1),
                //        AuthorId = authorId - 1
                //    });
                //}
            }

            CreateBookAuthors();
            CreateAuthorFriendsList();
        }

        //private static void CreateBookAuthors()
        //{
        //    var nvsnRecordCreateHelper = new RecordCreation<>();
        //}

        private static void CreateBookAuthors()
        {
            int bookAuthorId = 0;

            var authorBookKeyLimits = ForeignKeyCountsHelper.GetMinMaxAuthorBookFkCountPerUnit();
            int totalKeyCount = (int)(books.Count * 1.5);

            var authorBook_BookIdFk_Helper = new ForeignKeyHelper(books.Count, authorBookKeyLimits, totalKeyCount);

            var bookAuthorKeyLimits = ForeignKeyCountsHelper.GetMinMaxBookAuthorFkCountPerUnit();

            var bookAuthor_AuthorIdFk_Helper = new ForeignKeyHelper(authors.Count, bookAuthorKeyLimits, totalKeyCount);

            for (int i = 0; i < totalKeyCount; i++)
            {
                int? authorId = bookAuthor_AuthorIdFk_Helper.GetNewForeignKey();
                int? bookId = authorBook_BookIdFk_Helper.GetNewForeignKey();

                if (bookId.HasValue && authorId.HasValue)
                    bookAuthors.Add(new BookAuthor
                    {
                        Id = bookAuthorId++,
                        BookId = bookId.Value,
                        AuthorId = authorId.Value
                    });
            }
        }

        //private static void CreateBookAuthors()
        //{
        //    int bookAuthorId = 0;

        //    var authorBookKeyLimits = ForeignKeyCountsHelper.GetMinMaxAuthorBookFkCountPerUnit();
        //    int totalAuthorBookCount = (int)(books.Count * 0.75);

        //    var authorBook_BookIdFk_Helper = new ForeignKeyHelper(totalAuthorBookCount, authorBookKeyLimits);

        //    for (int i = 0; i < totalAuthorBookCount && i < authors.Count; i++)
        //    {
        //        int? bookId = authorBook_BookIdFk_Helper.GetNewForeignKey();

        //        if (bookId.HasValue)
        //            bookAuthors.Add(new BookAuthor
        //            {
        //                Id = bookAuthorId++,
        //                BookId = bookId.Value,
        //                AuthorId = i
        //            });
        //    }

        //    var bookAuthorKeyLimits = ForeignKeyCountsHelper.GetMinMaxBookAuthorFkCountPerUnit();
        //    int totalBookAuthorCount = (int)(books.Count * 0.75);
        //    if (totalBookAuthorCount > authors.Count)
        //        totalBookAuthorCount = authors.Count - 1;

        //    var bookAuthor_AuthorIdFk_Helper = new ForeignKeyHelper(totalBookAuthorCount, bookAuthorKeyLimits);

        //    for (int i = 0; i < totalBookAuthorCount && i < books.Count; i++)
        //    {
        //        int? authorId = bookAuthor_AuthorIdFk_Helper.GetNewForeignKey();

        //        if (authorId.HasValue)
        //            bookAuthors.Add(new BookAuthor
        //            {
        //                Id = bookAuthorId++,
        //                BookId = i,
        //                AuthorId = authorId.Value
        //            });
        //    }
        //}

        private static void CreateAuthorFriendsList()
        {
            Random random = new Random();

            for (int i = 0; i < authors.Count; i++)
            {
                if (!IsFriendListRequired())
                    continue;

                if (authorFriendList.Count >= authors.Count)
                    break;

                for (int j = 0; j < ForeignKeyCountsHelper.GenerateFriendListCount(); j++)
                {
                    bool hasFriendsTo = random.Next(2) == 0;

                    authorFriendList.Add(new AuthorFriendList
                    {
                        Id = authorFriendList.Count + 1,
                        AuthorId = authors[i].Id,
                        FriendsWithAuthorId = random.Next(authors.Count),
                        FriendsFrom = GenerateRandomDateTime(),
                        FriendsTo = hasFriendsTo ? GenerateRandomDateTime() : null,
                        FriendshipType = GenerateRandomFriendshipType()
                    });
                }
            }
        }

        private static bool IsFriendListRequired()
        {
            return new Random().Next(2) == 0;
        }

        private static int ExtractYearPublished(string description)
        {
            var match = System.Text.RegularExpressions.Regex.Match(description, @"\b(19|20)\d{2}\b");
            return match.Success ? int.Parse(match.Value) : DateTime.Now.Year; 
        }

        private static readonly Random random = new Random();

        public static string GenerateRandomDate()
        {
            DateTime startDate = new DateTime(1800, 1, 1);
            DateTime endDate = new DateTime(1990, 1, 1);

            int range = (endDate - startDate).Days;
            int randomDays = random.Next(range);

            DateTime randomDate = startDate.AddDays(randomDays);

            string formattedDate = randomDate.ToString("yyyy-MM-dd");

            return formattedDate;
        }

        private static DateTime GenerateRandomDateTime()
        {
            DateTime startDate = new DateTime(2000, 1, 1);
            int range = (DateTime.Today - startDate).Days;

            return startDate.AddDays(new Random().Next(range)).ToUniversalTime();
        }

        private static string GenerateRandomFriendshipType()
        {
            string[] friendshipTypes = { "Close Friend", "Acquaintance", "Colleague" };

            return friendshipTypes[new Random().Next(friendshipTypes.Length)];
        }
    }
}
