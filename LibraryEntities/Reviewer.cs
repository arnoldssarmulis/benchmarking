﻿namespace LibraryEntities
{
    public class Reviewer
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public ICollection<BookReview> BookReviews { get; set; }
    }
}
