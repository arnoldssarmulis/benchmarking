﻿using LibraryEntities.Helpers;
using LibraryEntities.Mapper;

namespace LibraryEntities.TableReferences.Models
{
    public class AuthorBookFK : ForeignKeySettings
    {
        public AuthorBookFK() 
        {
            KeyDeliveryCount = (int)(CSVBookToEntities.books.Count * 1.5);
            var authorBookKeyLimits = ForeignKeyCountsHelper.GetMinMaxAuthorBookFkCountPerUnit();

            MinFkCountPerUnit = authorBookKeyLimits.Item1;
            MaxFkCountPerUnit = authorBookKeyLimits.Item2;

            TotalObjectCount = CSVBookToEntities.books.Count;
        }
    }
}
