﻿using LibraryEntities.Helpers;
using LibraryEntities.TableReferences.Models;

namespace LibraryEntities.TableReferences
{
    public class RecordCreation<TTableOne, TTableTwo>
        where TTableOne : ForeignKeySettings, new()
        where TTableTwo : ForeignKeySettings, new()
    {
        private readonly int totalObjectCount;

        private readonly ForeignKeyHelper _firstTableHelper;
        private readonly ForeignKeyHelper _secondTableOneHelper;

        public RecordCreation() 
        {
            TTableOne tableOneInstance = new TTableOne();
            TTableTwo tableTwoInstance = new TTableTwo();

            totalObjectCount = tableOneInstance.TotalObjectCount;
            if (tableTwoInstance.TotalObjectCount < totalObjectCount)
                totalObjectCount = tableTwoInstance.TotalObjectCount;

            _firstTableHelper = new ForeignKeyHelper(tableOneInstance);
            _secondTableOneHelper = new ForeignKeyHelper(tableTwoInstance);
        }

        private void CreateNvsNRecord<TCollection>(List<TCollection> collection, Func<int, int, int, TCollection> addToCollection) 
            where TCollection : IEnumerable<TCollection>
        {
            int PrimaryKeyId = 0;

            for (int i = 0; i < totalObjectCount; i++)
            {
                int? firstFkId = _firstTableHelper.GetNewForeignKey();
                int? secondFkId = _secondTableOneHelper.GetNewForeignKey();

                bool canAddToCollection = secondFkId.HasValue && firstFkId.HasValue;
                
                if (canAddToCollection)
                {
                    collection.Add(addToCollection(PrimaryKeyId, firstFkId.Value, secondFkId.Value));
                }
            }
        }
    }
}
