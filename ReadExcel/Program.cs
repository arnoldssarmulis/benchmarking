﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.FileIO;

class Program
{
    static void Main(string[] args)
    {
        string csvFilePath = "C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\EFCodeFirst\\ResultsReader\\bin\\Debug\\net7.0" +
            "\\Addon_HybridApproachVsJoin_MSSQLServer___part_18.csv";

        int rowCount = 0;
        int rowsToSkip = 8;

        int countOfLargerTimes = 0;
        int usedRowCount = 0;
        int timesLarger = 5;
        int totalRecordCount = 0;

        List<double> divisions = new List<double>();

        using (TextFieldParser parser = new TextFieldParser(csvFilePath))
        {
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                List<string> fields = parser.ReadFields().ToList();

                if (rowCount < rowsToSkip || !fields.Any(f => !string.IsNullOrEmpty(f))
                    || fields.All(f => string.IsNullOrEmpty(f) || f.Contains("Scale")))
                {
                    rowCount++;
                    continue;
                }

                if (fields[0].StartsWith("L") && fields[0].Contains("menis "))
                {
                    for (int i = 0; i < fields.Count; i++)
                        if (string.IsNullOrEmpty(fields[i]))
                        {
                            fields.Remove(fields[i]);
                            i--;
                        }

                    //totalRecordCount += (fields.Count - 1) / 2;

                    //for (int i = 1; i < fields.Count; i++)
                    //{
                    //    if (double.Parse(fields[i]) >= timesLarger)
                    //    {
                    //        countOfLargerTimes++;
                    //    }
                    //    usedRowCount++;
                    //}

                    for (int i = 1; i + 1 < fields.Count; i += 2)
                    {
                        int division = (int)(double.Parse(fields[i]) / double.Parse(fields[i + 1]));
                        //int division = (int)(double.Parse(fields[i + 1]) / double.Parse(fields[i]));
                        if (division >= timesLarger)
                        {
                            countOfLargerTimes++;
                        }
                        else
                        {

                        }
                        divisions.Add(double.Parse(fields[i]));
                        divisions.Add(double.Parse(fields[i + 1]));

                        usedRowCount++;
                    }

                }


                rowCount++;
            }
        }

        double largestDiff = 0;
        int index = 0;
        for (int i = 0; i + 1 < divisions.Count; i += 2)
        {
            if (largestDiff == 0 || 
                largestDiff < (divisions[i] / divisions[i + 1]) * 100)
            {
                largestDiff = (divisions[i] / divisions[i + 1]) * 100;
                index = i;
            }
            if (largestDiff == 0 ||
                largestDiff < (divisions[i + 1] / divisions[i]) * 100)
            {
                largestDiff = (divisions[i + 1] / divisions[i]) * 100;
                index = i;
            }
        }

        Console.WriteLine($"countOfLargerTimes: {countOfLargerTimes}");
        Console.WriteLine($"usedRowCount: {usedRowCount}");
        Console.WriteLine($"largest diference: {largestDiff}");
        Console.WriteLine();

        Console.WriteLine("Press enter to end app...");

        Console.ReadLine();
    }
}
