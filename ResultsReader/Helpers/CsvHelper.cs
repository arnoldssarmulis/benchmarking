﻿using Org.BouncyCastle.Bcpg.OpenPgp;
using System.Reflection;

namespace ResultsReader.Helpers
{
    internal class CsvHelper
    {
        public static void WriteToCsv<T>(IEnumerable<T> data, string fileName, string header = null)
        {
            string fileBasePath = Directory.GetCurrentDirectory();
            string fullFilePath = Path.Combine(fileBasePath, fileName);
            fullFilePath += ".csv";

            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            using (StreamWriter sw = new StreamWriter(fullFilePath))
            {
                if (!string.IsNullOrEmpty(header))
                {
                    sw.WriteLine(header);
                    sw.WriteLine();
                }

                var properties = typeof(T).GetProperties().ToList();

                var allProperties = new List<PropertyInfo>();
                foreach (var prop in properties)
                {
                    if (prop.PropertyType.IsClass && prop.PropertyType != typeof(string))
                    {
                        var nestedProperties = prop.PropertyType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        allProperties.AddRange(nestedProperties);
                    }
                    else
                    {
                        allProperties.Add(prop);
                    }
                }

                sw.WriteLine(string.Join(",", allProperties.Select(p => p.Name)));

                foreach (var item in data)
                {
                    var values = new List<string>();
                    foreach (var prop in allProperties)
                    {
                        string value = string.Empty;

                        try
                        {
                            value = prop.GetValue(item)?.ToString() ?? string.Empty;
                        }
                        catch
                        {
                            var props = typeof(T);
                            var property = props.GetProperty("MethodResults");
                            object propVal = property.GetValue(item);

                            value = prop.GetValue(propVal)?.ToString() ?? string.Empty;
                        }

                        value = value.Replace("\"", "\"\"");

                        if (value.Contains(",") || value.Contains("\"") || value.Contains("\n") || value.Contains("\r"))
                        {
                            value = $"\"{value}\"";
                        }
                        values.Add(value);
                    }

                    sw.WriteLine(string.Join(",", values));
                }
            }
        }
    }
}
