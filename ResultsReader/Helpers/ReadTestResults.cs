﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Models;
using System.Text.RegularExpressions;

namespace ResultsReader.Helpers
{
    internal class ReadTestResults
    {
        private readonly static string rootFolder = "C:\\Users\\Arnolds\\Desktop\\LU PI MG\\3 semestris\\DatZ6016 - Maģistra kursa darbs datorzinātnēs\\Kursa darbs\\.NET Solutions\\EFCodeFirst\\Benchmarking\\bin\\Release\\net7.0\\BenchmarkDotNet.Artifacts";

        //public readonly static string postfix = "_11";

        public readonly static string postfix = "_18";

        public static List<ResultModel> Read()
        {
            List<ResultModel> testResults = new List<ResultModel>();

            var subdirectoryEntries = Directory
                .GetDirectories(rootFolder)
                .Where(dir => dir.EndsWith(postfix))
                .ToList();

            subdirectoryEntries.ForEach(entry =>
            {
                var path = entry.Split('\\');
                var directoryName = path[path.Length - 1];
                var serverName = Regex.Match(directoryName, @"[a-zA-Z]*").Value;

                Enum.TryParse(serverName, out DatabaseProviderType databaseProvider);

                var percentDirs = Directory.GetDirectories(entry).ToList();

                percentDirs.ForEach(pd =>
                {
                    var path = pd.Split('\\');
                    var directoryName = path[path.Length - 1];

                    Enum.TryParse(directoryName, out InputScaleLevel inputScaleLevel);

                    var results = Directory.GetFiles(pd).ToList();

                    results.ForEach(path =>
                    {
                        var fileName = Path.GetFileName(path);
                        var refScale = Regex.Match(fileName, @"ReferenceScale_[.\d]*").Value;
                        refScale = refScale.Replace("ReferenceScale_", "");

                        double referenceScale = double.Parse(refScale);

                        var fileTestResults = ReadFile(path);

                        fileTestResults.ForEach(r =>
                        {
                            r.ClassName = Regex.Match(fileName, @"^[a-zA-Z]+").Value;
                            r.InputScaleLevel = inputScaleLevel;
                            r.ReferenceScale = referenceScale;
                            r.DatabaseProvider = databaseProvider;
                        });

                        testResults.AddRange(fileTestResults);
                    });
                });
            });

            return testResults;
        }

        public static List<ResultModel> ReadFile(string filePath)
        {
            List<ResultModel> results = new List<ResultModel>();

            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                List<string> lineNames = new List<string>();

                while ((line = sr.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line) || line[0] != '|')
                        continue;

                    if (!lineNames.Any())
                    {
                        line = line.Replace(" ", string.Empty);
                        lineNames = line.Split(new char[] { '|' })
                            .Where(l => l.Any())
                            .ToList();

                        continue;
                    }

                    line = line.Replace(" ", string.Empty);
                    if (!line.Any(c => char.IsLetter(c)))
                        continue;

                    var lineValues = line.Split(new char[] { '|' })
                        .Where(l => l.Any())
                        .ToList();

                    CollectResultRecord(results, lineValues, lineNames);
                }
            }

            return results;
        }

        public static void CollectResultRecord(List<ResultModel> results, List<string> lineValues, List<string> lineNames)
        {
            if (lineValues.Any(lv => lv == "NA" || lv == "?"))
            {
                if (lineValues[lineNames.IndexOf("Error")] == "NA")
                    lineValues[lineNames.IndexOf("Error")] = "0";

                if (lineValues.Any(lv => lv == "NA" || lv == "?"))
                    throw new Exception("invalid results");
            }

            string meanValue = lineValues[lineNames.IndexOf("Mean")];
            string meanValMeasurement = Regex.Match(meanValue, @"[a-zμ]+").Value;

            meanValue = meanValue.Replace(",", string.Empty);
            double mean = double.Parse(Regex.Match(meanValue, @"[.\d]+").Value);

            string allocValue = lineValues[lineNames.IndexOf("Allocated")];
            string allocValMeasurement = Regex.Match(allocValue, @"[A-Z]+").Value;
            double alloc = double.Parse(Regex.Match(allocValue, @"[.\d]+").Value);

            string allocRatio = lineNames.Any(ln => ln == "AllocRatio") ?
                                 lineValues[lineNames.IndexOf("AllocRatio")] : null;

            string ratio = lineNames.Any(ln => ln == "Ratio") ?
                                 lineValues[lineNames.IndexOf("Ratio")] : null;


            results.Add(new ResultModel
            {
                MethodResults = new MethodResults
                {
                    Method = lineValues[lineNames.IndexOf("Method")],
                    Mean = mean,
                    MeanMeasureUnit = meanValMeasurement,
                    Allocated = alloc,
                    AllocatedMeasureUnit = allocValMeasurement,
                    AllocRatio = !string.IsNullOrEmpty(allocRatio) ? double.Parse(allocRatio) : null,
                    Ratio = !string.IsNullOrEmpty(ratio) ? double.Parse(ratio) : null
                }
            });
        }
    }
}
