﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Models;
using System.Text.RegularExpressions;

namespace ResultsReader.Helpers
{
    internal class ResultsValidator
    {
        public static void Validate(List<ResultModel> allResults)
        {
            var invalidRows = allResults.Where(r => !ValidateResult(r)).ToList();

            if (invalidRows.Any())
                throw new Exception("invalid");
        }

        private static bool ValidateResult(ResultModel result)
        {
            var properties = typeof(ResultModel).GetProperties();

            foreach (var property in properties)
            {
                var value = property.GetValue(result);

                if (value is string stringValue && stringValue.ToUpper() == "NA")
                {
                    return false;
                }

                if (value is double doubleValue && double.IsNaN(doubleValue))
                {
                    return false;
                }

                if (property.PropertyType == typeof(double?) && value != null && double.IsNaN((double)value))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
