﻿namespace ResultsReader.Models
{
    public class ComplexityLvl
    {
        public ComplexityLvl(string name, int complexity)
        {
            Name = name;
            Complexity = complexity;
        }

        public string Name { get; set; }

        public int Complexity {  get; set; }
    }
}
