﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;

namespace ResultsReader.Models
{
    public class ConfigBase
    {
        public ConfigBase()
        {
            ReferenceScale = null;
            DatabaseProvider = null;
            InputScaleLevel = null;
        }

        public double? ReferenceScale { get; set; }

        public DatabaseProviderType? DatabaseProvider { get; set; }

        public InputScaleLevel? InputScaleLevel { get; set; }
    }
}
