﻿using MySqlX.XDevAPI.Common;

namespace ResultsReader.Models
{
    public class MethodResults
    {
        public string Method { get; set; }

        public string MeanMeasureUnit { get; set; }

        public double Mean { get; set; }

        public double Allocated { get; set; }

        public string AllocatedMeasureUnit { get; set; }

        public double? Ratio { get; set; }

        public double? AllocRatio { get; set; }

        public string GetMeanWithMeasure()
        {
            return $"{Mean} {MeanMeasureUnit}";
        }

        public double GetMeanInMS()
        {
            double result;

            if (MeanMeasureUnit == "μs")
            {
                result = Mean / 1000.0;
            }
            else if (MeanMeasureUnit == "ms")
            {
                result = Mean;
            }
            else if (MeanMeasureUnit == "s")
            {
                result = Mean * 1000.0;
            }
            else
            {
                throw new InvalidOperationException("Invalid MeanMeasureUnit. Must be 'ms' or 'μs' or 's'.");
            }

            result = Math.Round(result, 3);

            return result;
        }

        public string GetMeanAbsolute()
        {
            if (MeanMeasureUnit == "ms" || MeanMeasureUnit == "μs")
            {
                var meanInMs = GetMeanInMS();
                bool isSeconds = false;

                if (meanInMs >= 1000)
                {
                    meanInMs /= 1000;
                    isSeconds = true;
                }

                return $"{Math.Round(meanInMs, 1)} {(isSeconds ? "s" : "ms")}";
            }

            var mean = Math.Round(Mean, 1);

            return $"{mean} {MeanMeasureUnit}";
        }

    }
}
