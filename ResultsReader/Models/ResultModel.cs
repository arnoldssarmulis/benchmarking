﻿namespace ResultsReader.Models
{
    public class ResultModel : ConfigBase
    {
        public string ClassName { get; set; }

        public MethodResults MethodResults { get; set; }
    }
}
