﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections;

namespace ResultsReader
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var results = ReadTestResults.Read();

            ResultsValidator.Validate(results);

            //TestResultProjections.ComplexityIncrease(results);
            //TestResultProjections.AvgComplexityIncreaseForDBs(results);
            //TestResultProjections.AvgComplexityIncreaseForDBsByPercents(results);

            //NavVsJoins.CreateCSVsForAllScaleLevels(results);
            //NavVsJoins.CreateCSVsForAllRefLevels(results);

            //JoinVsBaseline.CreateCSVsForAllScaleLevels(results);
            //JoinVsBaseline.CreateCSVsForAllRefLevels(results);

            //NavVsJoinByDifference.CreateCSVForAllScaleLevels(results);
            //BaselineVsFastest.CreateCSVsForAllDbs(results);

            //FullTableNavVsJoin.CreateCSVsForAllTablesBaseline(results);

            //GroupJoinVsSelect.CreateCSVsForGroupJoinVsSelect(results);
            //SelectVsSelectMany.CreateCSVsForGroupJoinVsSelect(results);

            HybridApproachVsJoin.CreateCSVsForJoinVsHybrid(results);

            Console.WriteLine("Press any key to end application..");
            Console.ReadLine();
        }
    }
}