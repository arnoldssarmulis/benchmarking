﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ResultsReader.Projections
{
    internal class BaselineVsFastest
    {
        public static void CreateCSVsForAllDbs(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? null,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("BaselineVsJoinByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 2;

            var func = bool (ResultModel rm, DatabaseProviderType database, double rs, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 &&
                            rm.DatabaseProvider == database &&
                            rm.ReferenceScale == rs &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
                            (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));

            //var func = bool (ResultModel rm, DatabaseProviderType database, InputScaleLevel scaleLevel, int lvl, string methodType) =>
            //    rm.ReferenceScale == 0.5 &&
            //    rm.InputScaleLevel == scaleLevel &&
            //    rm.DatabaseProvider == database &&
            //    rm.ClassName.Contains(complexityLevels[lvl].Name) &&
            //    rm.MethodResults.Method.Contains(methodType) &&
            //    (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
            //    (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));


            //var calc = double (DatabaseProviderType database, InputScaleLevel scaleLevel, int lvl) =>
            //                    Math.Min(resultProjection.Single(rp => func(rp, database, scaleLevel, lvl, "Join")).MethodResults.GetMeanInMS(),
            //                             resultProjection.Single(rp => func(rp, database, scaleLevel, lvl, "Property")).MethodResults.GetMeanInMS())
            //                        / resultProjection.Single(rp => func(rp, database, scaleLevel, lvl, "Baseline")).MethodResults.GetMeanInMS();

            var calc = double (DatabaseProviderType database, double rs, int lvl) =>
                                Math.Min(resultProjection.Single(rp => func(rp, database, rs, lvl, "Join")).MethodResults.GetMeanInMS(),
                                         resultProjection.Single(rp => func(rp, database, rs, lvl, "Property")).MethodResults.GetMeanInMS())
                                / resultProjection.Single(rp => func(rp, database, rs, lvl, "Baseline")).MethodResults.GetMeanInMS();

            var toCSVList = Enumerable.Range(0, 3)
                .Select(al =>
                {
                    var row = new
                    {
                        MSQRefScale001Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, 0.01, lvl), 4),
                        MSQRefScale05Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, 0.5, lvl), 4),
                        MSQRefScale10Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, 10, lvl), 4),
                        PSGRefScale001Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, 0.01, lvl), 4),
                        PSGRefScale05Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, 0.5, lvl), 4),
                        PSGRefScale10Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, 10, lvl), 4),
                        MYSRefScale001Diff = Math.Round(calc(DatabaseProviderType.MySQL, 0.01, lvl), 4),
                        MYSRefScale05Diff = Math.Round(calc(DatabaseProviderType.MySQL, 0.5, lvl), 4),
                        MYSRefScale10Diff = Math.Round(calc(DatabaseProviderType.MySQL, 10, lvl), 4)
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            //var toCSVList = Enumerable.Range(0, 3)
            //    .Select(al =>
            //    {
            //        var row = new
            //        {
            //            MSQScale10Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, InputScaleLevel.Percent10, lvl), 3),
            //            MSQScale50Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, InputScaleLevel.Percent50, lvl), 3),
            //            MSQScale100Diff = Math.Round(calc(DatabaseProviderType.MSSQLServer, InputScaleLevel.Percent100, lvl), 3),
            //            PSGScale10Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, InputScaleLevel.Percent10, lvl), 3),
            //            PSGScale50Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, InputScaleLevel.Percent50, lvl), 3),
            //            PSGScale100Diff = Math.Round(calc(DatabaseProviderType.PostgreSQL, InputScaleLevel.Percent100, lvl), 3),
            //            MYSScale10Diff = Math.Round(calc(DatabaseProviderType.MySQL, InputScaleLevel.Percent10, lvl), 3),
            //            MYSScale50Diff = Math.Round(calc(DatabaseProviderType.MySQL, InputScaleLevel.Percent50, lvl), 3),
            //            MYSScale100Diff = Math.Round(calc(DatabaseProviderType.MySQL, InputScaleLevel.Percent100, lvl), 3)
            //        };

            //        lvl++;

            //        return row;
            //    })
            //    .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
