﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using Microsoft.EntityFrameworkCore.Storage;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;

namespace ResultsReader.Projections
{
    internal class FullTableNavVsJoin
    {
        public static void CreateCSVsForAllTables(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MySQL,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig($"s_{config.DatabaseProvider}_FullTableNavVsJoin", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 0;

            var funcScale = bool (ResultModel rm, InputScaleLevel sl, int lvl, string methodType) =>
                            rm.ReferenceScale == 0.5 && 
                            rm.InputScaleLevel == sl &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");


            var funcRefScale = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 && 
                            rm.ReferenceScale == rc &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");

            var toCSVList = Enumerable.Range(0, 5)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10Property = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent10, lvl, "Property")).MethodResults.GetMeanAbsolute(),
                        Scale10Join = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent10, lvl, "Join")).MethodResults.GetMeanAbsolute(),
                        Scale50Property = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent50, lvl, "Property")).MethodResults.GetMeanAbsolute(),
                        Scale50Join = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent50, lvl, "Join")).MethodResults.GetMeanAbsolute(),
                        Scale100Property = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent100, lvl, "Property")).MethodResults.GetMeanAbsolute(),
                        Scale100Join = resultProjection.Single(rp => funcScale(rp, InputScaleLevel.Percent100, lvl, "Join")).MethodResults.GetMeanAbsolute(),
                        RefScale001Property = resultProjection.Single(rp => funcRefScale(rp, 0.01, lvl, "Property")).MethodResults.GetMeanAbsolute(),
                        RefScale001Join = resultProjection.Single(rp => funcRefScale(rp, 0.01, lvl, "Join")).MethodResults.GetMeanAbsolute(),
                        RefScale10Property = resultProjection.Single(rp => funcRefScale(rp, 10, lvl, "Property")).MethodResults.GetMeanAbsolute(),
                        RefScale10Join = resultProjection.Single(rp => funcRefScale(rp, 10, lvl, "Join")).MethodResults.GetMeanAbsolute()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }

        public static void CreateCSVsForAllTablesBaseline(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MySQL,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");
            string methodName = "Join";

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig($"s_{methodName}_FullTableBaseline", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 2;

            var func = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 &&
                            rm.ReferenceScale == rc &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
                            (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));

            var funcInputScale = bool (ResultModel rm, InputScaleLevel inputScale, int lvl, string methodType) =>
                rm.ReferenceScale == 0.5 &&
                rm.InputScaleLevel == inputScale &&
                rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                rm.MethodResults.Method.Contains(methodType) &&
                (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
                (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));

            var toCSVList = Enumerable.Range(0, 3)
                .Select(al =>
                {
                    //var row = new
                    //{
                    //    Scale10Property = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                    //    Scale10Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                    //    Scale50Property = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                    //    Scale50Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                    //    Scale100Property = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                    //    Scale100Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                    //    RefScale001Property = resultProjection.Single(rp => func(rp, 0.01, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                    //    RefScale001Baseline = resultProjection.Single(rp => func(rp, 0.01, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                    //    RefScale10Property = resultProjection.Single(rp => func(rp, 10, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                    //    RefScale10Baseline = resultProjection.Single(rp => func(rp, 10, lvl, "Baseline")).MethodResults.GetMeanAbsolute()
                    //};

                    var row = new
                    {
                        Scale10Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                        Scale10Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                        Scale50Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                        Scale50Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                        Scale100Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                        Scale100Baseline = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                        RefScale001Join = resultProjection.Single(rp => func(rp, 0.01, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                        RefScale001Baseline = resultProjection.Single(rp => func(rp, 0.01, lvl, "Baseline")).MethodResults.GetMeanAbsolute(),
                        RefScale10Join = resultProjection.Single(rp => func(rp, 10, lvl, methodName)).MethodResults.GetMeanAbsolute(),
                        RefScale10Baseline = resultProjection.Single(rp => func(rp, 10, lvl, "Baseline")).MethodResults.GetMeanAbsolute()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);

        }

    }
}
