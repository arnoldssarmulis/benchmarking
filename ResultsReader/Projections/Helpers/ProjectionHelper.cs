﻿using ResultsReader.Helpers;
using ResultsReader.Models;

namespace ResultsReader.Projections.Helpers
{
    internal static class ProjectionHelper
    {
        public static List<ComplexityLvl> GetComplexityLevels
        {
            get => new List<ComplexityLvl>
            {
                new ComplexityLvl("BasicComplexityIncrease", 1),
                new ComplexityLvl("BasicComplexityIncrease", 2),
                new ComplexityLvl("BasicComplexityIncrease", 3),
                new ComplexityLvl("AvarageComplexity", 1),
                new ComplexityLvl("MaxComplexity", 1)
            };
        }

        public static string[] BaseClases = new string[]
        {
            "BasicComplexityIncreaseBase",
            "AvarageComplexityBase",
            "MaxComplexityBase"
        };

        public static string[] AddonClasses = new string[]
        {
            "BasicComplexityIncreaseWithAddons",
            "AvarageComplexityWithAddons",
            "MaxComplexityWithAddons"
        };

        public static List<ResultModel> GetResultsByConfig(List<ResultModel> allResults, string[] classesToInclude, ConfigBase config)
        {
            var resultProjection = allResults
                .Where(al =>
                        (!config.DatabaseProvider.HasValue || al.DatabaseProvider == config.DatabaseProvider) &&
                        (!config.InputScaleLevel.HasValue || al.InputScaleLevel == config.InputScaleLevel) &&
                        (!config.ReferenceScale.HasValue || al.ReferenceScale == config.ReferenceScale) &&
                        classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            return resultProjection;
        }

        public static string GetFileNameByConfig(string fileName, ConfigBase config)
        {
            fileName += $"_{config.DatabaseProvider}_{config.InputScaleLevel}_{config.ReferenceScale:0.00}_part{ReadTestResults.postfix}";

            return fileName;
        }

        public static string GetUsedConfigs(List<ResultModel> allResults)
        {
            var usedScaleLvls = new List<string>();
            var usedRefScales = new List<string>();
            var usedDBs = new List<string>();

            allResults.ForEach(result =>
            {
                if (!usedDBs.Any(d => d == result.DatabaseProvider.ToString()))
                    usedDBs.Add(result.DatabaseProvider.ToString());

                if (!usedRefScales.Any(d => d == result.ReferenceScale.ToString()))
                    usedRefScales.Add(result.ReferenceScale.ToString());

                if (!usedScaleLvls.Any(d => d == result.InputScaleLevel.ToString()))
                    usedScaleLvls.Add(result.InputScaleLevel.ToString());
            });

            string header = $"Used databases:  {string.Join(" ", usedDBs)}" +
                 $", Used reference scales:  {string.Join(" ", usedRefScales)}" +
                 $", Used input lvl scales:  {string.Join(" ", usedScaleLvls)}";

            return header;
        }

        public static string GetUsedClases(string[] usedClasses)
        {
            return $", , , , , usedClasses: {string.Join(" ", usedClasses)}";
        }
    }
}
