﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;

namespace ResultsReader.Projections
{
    public static class HybridApproachVsJoin
    {
        public static void CreateCSVsForJoinVsHybrid(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = new string[]
            {
                "HybridApproach"
            };

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            allResults = allResults.Where(r => r.ClassName == "HybridApproach").ToList();

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("HybridApproachVsJoin", config);

            var func = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 &&
                            rm.ReferenceScale == rc &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.Contains($"0{lvl}");

            var funcInputScale = bool (ResultModel rm, InputScaleLevel inputScale, int lvl, string methodType) =>
                rm.ReferenceScale == 0.5 &&
                rm.InputScaleLevel == inputScale &&
                rm.MethodResults.Method.Contains(methodType) &&
                rm.MethodResults.Method.Contains($"0{lvl}");

            string hybridAproachType = "HybridAproach";
            string methodJoinType = "Join";
            int lvl = 2;

            var toCSVList = Enumerable.Range(0, 3)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10HybridAproach = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, hybridAproachType)).MethodResults.GetMeanInMS(),
                        Scale10Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        Scale50HybridAproach = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, hybridAproachType)).MethodResults.GetMeanInMS(),
                        Scale50Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        Scale100HybridAproach = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, hybridAproachType)).MethodResults.GetMeanInMS(),
                        Scale100Join = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        RefScale001HybridAproach = resultProjection.Single(rp => func(rp, 0.01, lvl, hybridAproachType)).MethodResults.GetMeanInMS(),
                        RefScale001Join = resultProjection.Single(rp => func(rp, 0.01, lvl, methodJoinType)).MethodResults.GetMeanInMS()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
