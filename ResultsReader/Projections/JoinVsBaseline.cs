﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;

namespace ResultsReader.Projections
{
    internal class JoinVsBaseline
    {
        public static void CreateCSVsForAllScaleLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? 0.5,
            };

            var classesToInclude = ProjectionHelper.BaseClases;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("BaselineVsJoinByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 2;

            var func = bool (ResultModel rm, InputScaleLevel sl, int lvl, string methodType) =>
                            rm.InputScaleLevel == sl &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
                            (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));

            var elem = resultProjection.Where(rp => func(rp, InputScaleLevel.Percent10, lvl, "Baseline"));

            var toCSVList = Enumerable.Range(0, 3)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent10, lvl, "Join")).MethodResults.GetMeanInMS(),
                        Scale10Baseline = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent10, lvl, "Baseline")).MethodResults.GetMeanInMS(),
                        Scale50Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent50, lvl, "Join")).MethodResults.GetMeanInMS(),
                        Scale50Baseline = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent50, lvl, "Baseline")).MethodResults.GetMeanInMS(),
                        Scale100Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent100, lvl, "Join")).MethodResults.GetMeanInMS(),
                        Scale100Baseline = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent100, lvl, "Baseline")).MethodResults.GetMeanInMS()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }

        public static void CreateCSVsForAllRefLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? InputScaleLevel.Percent100,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = ProjectionHelper.BaseClases;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("BaselineVsJoinByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 2;

            var func = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.ReferenceScale == rc &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            (methodType == "Baseline" || rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}") &&
                            (complexityLevels[lvl].Complexity != 3 || rm.MethodResults.Method == "Query_03_Baseline" || rm.MethodResults.Method.Contains(methodType)));

            //var elem = resultProjection.Where(rp => func(rp, rc, lvl, "Baseline"));

            var toCSVList = Enumerable.Range(0, 3)
                .Select(al =>
                {
                    var row = new
                    {
                        RefScale001Join = resultProjection.Single(rp => func(rp, 0.01, lvl, "Join")).MethodResults.GetMeanInMS(),
                        RefScale001Baseline = resultProjection.Single(rp => func(rp, 0.01, lvl, "Baseline")).MethodResults.GetMeanInMS(),
                        RefScale05Join = resultProjection.Single(rp => func(rp, 0.5, lvl, "Join")).MethodResults.GetMeanInMS(),
                        RefScale05Baseline = resultProjection.Single(rp => func(rp, 0.5, lvl, "Baseline")).MethodResults.GetMeanInMS(),
                        RefScale10Join = resultProjection.Single(rp => func(rp, 10, lvl, "Join")).MethodResults.GetMeanInMS(),
                        RefScale10Baseline = resultProjection.Single(rp => func(rp, 10, lvl, "Baseline")).MethodResults.GetMeanInMS()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
