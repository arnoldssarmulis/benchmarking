﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;

namespace ResultsReader.Projections
{
    internal class NavVsJoinByDifference
    {
        public static void CreateCSVForAllScaleLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.PostgreSQL,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("NavVsJoinsByDiferenceByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 0;

            var func = bool (ResultModel rm, InputScaleLevel sl, int lvl, string methodType) =>
                            rm.ReferenceScale == 0.5 &&
                            rm.InputScaleLevel == sl &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");

            var funcRefScale = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 &&
                            rm.ReferenceScale == rc &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");

            var getScaleDiff = double (InputScaleLevel sl, int lvl) =>
                resultProjection.Single(rp => func(rp, sl, lvl, "Property")).MethodResults.GetMeanInMS() /
                resultProjection.Single(rp => func(rp, sl, lvl, "Join")).MethodResults.GetMeanInMS();

            var getRefScaleDiff = double (double rs, int lvl) =>
                resultProjection.Single(rp => funcRefScale(rp, rs, lvl, "Property")).MethodResults.GetMeanInMS() /
                resultProjection.Single(rp => funcRefScale(rp, rs, lvl, "Join")).MethodResults.GetMeanInMS();

            var toCSVList = Enumerable.Range(0, 5)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10Diff = Math.Round(getScaleDiff(InputScaleLevel.Percent10, lvl), 2),
                        Scale50Diff = Math.Round(getScaleDiff(InputScaleLevel.Percent50, lvl), 2),
                        Scale100Diff = Math.Round(getScaleDiff(InputScaleLevel.Percent100, lvl), 2),
                        RefScale001Diff = Math.Round(getRefScaleDiff(0.01, lvl), 2),
                        RefScale10Diff = Math.Round(getRefScaleDiff(10, lvl), 2)
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
