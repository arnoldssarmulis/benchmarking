﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;
using System.Text.RegularExpressions;

namespace ResultsReader.Projections
{
    internal static class NavVsJoins
    {
        public static void CreateCSVsForAllScaleLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? 0.5,
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") + 
                ProjectionHelper.GetFileNameByConfig("NavVsJoinsByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 0;

            var func = bool (ResultModel rm, InputScaleLevel sl, int lvl, string methodType) => 
                            rm.InputScaleLevel == sl &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");

            var toCSVList = Enumerable.Range(0, 5)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10Property = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent10, lvl, "Property")).MethodResults.GetMeanInMS(),
                        Scale10Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent10, lvl, "Join")).MethodResults.GetMeanInMS(),
                        Scale50Property = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent50, lvl, "Property")).MethodResults.GetMeanInMS(),
                        Scale50Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent50, lvl, "Join")).MethodResults.GetMeanInMS(),
                        Scale100Property = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent100, lvl, "Property")).MethodResults.GetMeanInMS(),
                        Scale100Join = resultProjection.Single(rp => func(rp, InputScaleLevel.Percent100, lvl, "Join")).MethodResults.GetMeanInMS()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }

        public static void CreateCSVsForAllRefLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? InputScaleLevel.Percent100,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = ProjectionHelper.AddonClasses;

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("NavVsJoinsByAllScales", config);

            var complexityLevels = ProjectionHelper.GetComplexityLevels;
            int lvl = 0;

            var func = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.ReferenceScale == rc &&
                            rm.ClassName.Contains(complexityLevels[lvl].Name) &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.EndsWith($"_0{complexityLevels[lvl].Complexity}");

            var toCSVList = Enumerable.Range(0, 5)
                .Select(al =>
                {
                    var row = new
                    {
                        RefScale001Property = resultProjection.Single(rp => func(rp, 0.01, lvl, "Property")).MethodResults.GetMeanInMS(),
                        RefScale001Join = resultProjection.Single(rp => func(rp, 0.01, lvl, "Join")).MethodResults.GetMeanInMS(),
                        RefScale05Property = resultProjection.Single(rp => func(rp, 0.5, lvl, "Property")).MethodResults.GetMeanInMS(),
                        RefScale05Join = resultProjection.Single(rp => func(rp, 0.5, lvl, "Join")).MethodResults.GetMeanInMS(),
                        RefScale10Property = resultProjection.Single(rp => func(rp, 10, lvl, "Property")).MethodResults.GetMeanInMS(),
                        RefScale10Join = resultProjection.Single(rp => func(rp, 10, lvl, "Join")).MethodResults.GetMeanInMS()
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
