﻿using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;
using System.Text;
using System.Text.RegularExpressions;

namespace ResultsReader.Projections
{
    internal static class ResultProjectionHelper
    {
        public static void OutputToCsvResultsByNavVSJoin(List<ResultModel> allResults, string[] classesToInclude, ConfigBase config, string fileName)
        {
            fileName += $"_{config.DatabaseProvider}_{config.InputScaleLevel}_{config.ReferenceScale:0.00}_part{ReadTestResults.postfix}";

            var resultProjection = allResults
                .Where(al =>
                        (!config.DatabaseProvider.HasValue || al.DatabaseProvider == config.DatabaseProvider) &&
                        (!config.InputScaleLevel.HasValue || al.InputScaleLevel == config.InputScaleLevel) &&
                        (!config.ReferenceScale.HasValue || al.ReferenceScale == config.ReferenceScale) &&
                        classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .Select(al =>
                {
                    var methodName = al.ClassName + "." + al.MethodResults.Method;
                    var methodNumber = Regex.Match(al.MethodResults.Method, @"_[\d]+$").Value;

                    var joinMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .First();

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = joinMethod.ClassName + "." + joinMethod.MethodResults.Method,
                        MeanNavProperty = al.MethodResults.GetMeanInMS(),
                        MeanJoin = joinMethod.MethodResults.GetMeanInMS()
                    };

                    return row;
                })
                .OrderByDescending(r => r.MeanNavProperty - r.MeanJoin);

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }

        public static void OutputToCsvResultsByNavVSJoinWithBaseline(List<ResultModel> allResults, string[] classesToInclude, ConfigBase config, string fileName)
        {
            fileName += $"_{config.DatabaseProvider}_{config.InputScaleLevel}_{config.ReferenceScale:0.00}_part{ReadTestResults.postfix}";

            var resultProjection = allResults
            .Where(al =>
                    (!config.DatabaseProvider.HasValue || al.DatabaseProvider == config.DatabaseProvider) &&
                    (!config.InputScaleLevel.HasValue || al.InputScaleLevel == config.InputScaleLevel) &&
                    (!config.ReferenceScale.HasValue || al.ReferenceScale == config.ReferenceScale) &&
                    classesToInclude.Any(cn => cn == al.ClassName))
            .OrderByDescending(al => al.MethodResults.GetMeanInMS())
            .ToList();

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);

            var toCSVList = resultProjection
                .Where(al => (al.MethodResults.Method.Contains("Property") ||
                             al.MethodResults.Method.Contains("Baseline", StringComparison.CurrentCultureIgnoreCase)) &&
                             (!al.ClassName.Contains("BasicComplexityIncrease") ||
                                !al.MethodResults.Method.EndsWith("_1") && !al.MethodResults.Method.EndsWith("_2"))
                )
                .Select(al =>
                {
                    var methodName = al.ClassName + "." + al.MethodResults.Method;
                    var methodNumber = Regex.Match(al.MethodResults.Method, @"_[\d]+$").Value;

                    var joinMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .First();

                    var baseLineMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     al.MethodResults.Method.Contains("Baseline", StringComparison.CurrentCultureIgnoreCase))
                        .First();

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = joinMethod.ClassName + "." + joinMethod.MethodResults.Method,
                        MethodNameBaseline = baseLineMethod.ClassName + "." + baseLineMethod.MethodResults.Method,
                        MeanNavProperty = al.MethodResults.GetMeanInMS(),
                        MeanJoin = joinMethod.MethodResults.GetMeanInMS(),
                        AvgMeanBaseline = baseLineMethod.MethodResults.GetMeanInMS()
                    };

                    return row;
                })
                .OrderByDescending(r => r.MeanNavProperty - r.MeanJoin);

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }

        public static void OutputToCsvResultsByAvgNavVSJoin(List<ResultModel> allResults, string[] classesToInclude, ConfigBase config, string fileName)
        {
            fileName += $"_{config.DatabaseProvider}_{config.InputScaleLevel}_{config.ReferenceScale:0.00}_part{ReadTestResults.postfix}";

            var resultProjection = allResults
            .Where(al =>
                    (!config.DatabaseProvider.HasValue || al.DatabaseProvider == config.DatabaseProvider) &&
                    (!config.InputScaleLevel.HasValue || al.InputScaleLevel == config.InputScaleLevel) &&
                    (!config.ReferenceScale.HasValue || al.ReferenceScale == config.ReferenceScale) &&
                    classesToInclude.Any(cn => cn == al.ClassName))
            .OrderByDescending(al => al.MethodResults.GetMeanInMS())
            .ToList();

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .GroupBy(al => new { al.ClassName, al.MethodResults.Method })
                .Select(al =>
                {
                    var methodName = al.Key.ClassName + "." + al.Key.Method;
                    var methodNumber = Regex.Match(al.Key.Method, @"_[\d]+$").Value;
                    var avgMethodMean = al.Average(a => a.MethodResults.GetMeanInMS());
                    avgMethodMean = Math.Round(avgMethodMean, 2);

                    var methodNameJoin = string.Empty;
                    var avgMeanJoin = 0.00;

                    var joinMethods = resultProjection
                        .Where(rp => rp.ClassName == al.Key.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .GroupBy(jm => new { jm.ClassName, jm.MethodResults.Method })
                        .ToList();

                    joinMethods
                        .ForEach(r =>
                        {
                            methodNameJoin = r.Key.ClassName + "." + r.Key.Method;
                            avgMeanJoin = r.Average(a => a.MethodResults.GetMeanInMS());
                        });

                    avgMeanJoin = Math.Round(avgMeanJoin, 2);

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = methodNameJoin,
                        AvgMeanNavProperty = avgMethodMean,
                        AvgMeanJoin = avgMeanJoin
                    };

                    return row;
                })
                .OrderByDescending(c => c.AvgMeanNavProperty - c.AvgMeanJoin);

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
