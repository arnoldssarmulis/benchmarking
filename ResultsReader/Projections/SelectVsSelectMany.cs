﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using ResultsReader.Projections.Helpers;

namespace ResultsReader.Projections
{
    internal class SelectVsSelectMany
    {
        public static void CreateCSVsForGroupJoinVsSelect(List<ResultModel> allResults, ConfigBase config = null)
        {
            config = new ConfigBase()
            {
                DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                InputScaleLevel = config?.InputScaleLevel ?? null,
                ReferenceScale = config?.ReferenceScale ?? null,
            };

            var classesToInclude = new string[]
            {
                "JoinApproachesGroupJoinVsSelectManyAddons"
            };

            var resultProjection = ProjectionHelper.GetResultsByConfig(allResults, classesToInclude, config);

            var headerLine = ProjectionHelper.GetUsedConfigs(resultProjection);
            headerLine += ProjectionHelper.GetUsedClases(classesToInclude);
            bool hasUsedBaseClasses = headerLine.Contains("Base");

            string fileName = (hasUsedBaseClasses ? "Base_" : "Addon_") +
                ProjectionHelper.GetFileNameByConfig("SelectVsSelectMany", config);

            var func = bool (ResultModel rm, double rc, int lvl, string methodType) =>
                            rm.InputScaleLevel == InputScaleLevel.Percent100 &&
                            rm.ReferenceScale == rc &&
                            rm.MethodResults.Method.Contains(methodType) &&
                            rm.MethodResults.Method.Contains($"0{lvl}");

            var funcInputScale = bool (ResultModel rm, InputScaleLevel inputScale, int lvl, string methodType) =>
                rm.ReferenceScale == 0.5 &&
                rm.InputScaleLevel == inputScale &&
                rm.MethodResults.Method.Contains(methodType) &&
                rm.MethodResults.Method.Contains($"0{lvl}");

            string methodPropertyType = "Property";
            string methodJoinType = "Join";
            int lvl = 1;

            var toCSVList = Enumerable.Range(0, 4)
                .Select(al =>
                {
                    var row = new
                    {
                        Scale10Select = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, methodPropertyType)).MethodResults.GetMeanInMS(),
                        Scale10SelectMany = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent10, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        Scale50Select = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, methodPropertyType)).MethodResults.GetMeanInMS(),
                        Scale50SelectMany = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent50, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        Scale100Select = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, methodPropertyType)).MethodResults.GetMeanInMS(),
                        Scale100SelectMany = resultProjection.Single(rp => funcInputScale(rp, InputScaleLevel.Percent100, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                        RefScale001Select = resultProjection.Single(rp => func(rp, 0.01, lvl, methodPropertyType)).MethodResults.GetMeanInMS(),
                        RefScale001SelectMany = resultProjection.Single(rp => func(rp, 0.01, lvl, methodJoinType)).MethodResults.GetMeanInMS(),
                    };

                    lvl++;

                    return row;
                })
                .ToList();

            CsvHelper.WriteToCsv(toCSVList, fileName, headerLine);
        }
    }
}
