﻿using EFCoreDBContext.Config;
using Google.Protobuf.WellKnownTypes;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using System.Linq;
using System.Text.RegularExpressions;

namespace ResultsReader.Projections
{
    internal class TestResultProjections
    {
        /// <summary>
        /// ComplexityIncrease for navigation properties vs joins
        /// </summary>
        public static void ComplexityIncrease(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAllTogether",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            var resultProjection = allResults
                .Where(al =>
                    al.DatabaseProvider == DatabaseProviderType.MSSQLServer &&
                    al.InputScaleLevel == InputScaleLevel.Percent100 &&
                    al.ReferenceScale == 1.00 &&
                    classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .Select(al =>
                {
                    var methodName = al.ClassName + "." + al.MethodResults.Method;
                    var methodNumber = Regex.Match(al.MethodResults.Method, @"_[\d]+$").Value;
                    var joinMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .First();

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = joinMethod.ClassName + "." + joinMethod.MethodResults.Method,
                        MeanNavProperty = al.MethodResults.GetMeanInMS(),
                        MeanJoin = joinMethod.MethodResults.GetMeanInMS()
                    };

                    return row;
                });

            CsvHelper.WriteToCsv(toCSVList, "ComplexityIncrease");
        }

        /// <summary>
        /// ComplexityIncrease for navigation properties vs joins calculated as avarage for method 
        /// </summary>
        public static void AvgComplexityIncreaseForDBs(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAllTogether",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            var resultProjection = allResults
                .Where(al =>
                    al.InputScaleLevel == InputScaleLevel.Percent100 &&
                    al.ReferenceScale == 1.00 &&
                    classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var methods = resultProjection
                .GroupBy(al => al.DatabaseProvider)
                .Select(c => new
                {
                    DB = c.Key,
                    Count = c.Count()
                })
                .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .GroupBy(al => new { al.ClassName, al.MethodResults.Method })
                .Select(al =>
                {
                    var methodName = al.Key.ClassName + "." + al.Key.Method;
                    var methodNumber = Regex.Match(al.Key.Method, @"_[\d]+$").Value;
                    var avgMethodMean = al.Average(a => a.MethodResults.GetMeanInMS());
                    avgMethodMean = Math.Round(avgMethodMean, 2);

                    var methodNameJoin = string.Empty;
                    var avgMeanJoin = 0.00;

                    var joinMethods = resultProjection
                        .Where(rp => rp.ClassName == al.Key.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .GroupBy(jm => new { jm.ClassName, jm.MethodResults.Method })
                        .ToList();

                    joinMethods
                        .ForEach(r =>
                        {
                            methodNameJoin = r.Key.ClassName + "." + r.Key.Method;
                            avgMeanJoin = r.Average(a => a.MethodResults.GetMeanInMS());
                        });

                    avgMeanJoin = Math.Round(avgMeanJoin, 2);

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = methodNameJoin,
                        AvgMeanNavProperty = avgMethodMean,
                        AvgMeanJoin = avgMeanJoin
                    };

                    return row;
                })
                .OrderByDescending(c => c.AvgMeanJoin + c.AvgMeanNavProperty);

            CsvHelper.WriteToCsv(toCSVList, "AvgComplexityIncreaseForDBs");
        }

        public static void ComplexityIncreaseForDBsWithBaseline(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAllTogether",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            var resultProjection = allResults
                .Where(al =>
                    al.InputScaleLevel == InputScaleLevel.Percent100 &&
                    al.ReferenceScale == 1.00 &&
                    classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property") &&
                            (al.ClassName != "BasicComplexityIncreaseWithAllTogether" || al.MethodResults.Method.EndsWith("_03")))
                .GroupBy(al => new { al.ClassName, al.MethodResults.Method })
                .Select(al =>
                {
                    var methodName = al.Key.ClassName + "." + al.Key.Method;
                    var methodNumber = Regex.Match(al.Key.Method, @"_[\d]+$").Value;
                    var avgMethodMean = al.Average(a => a.MethodResults.GetMeanInMS());
                    avgMethodMean = Math.Round(avgMethodMean, 2);

                    var methodNameJoin = string.Empty;
                    var avgMeanJoin = 0.00;

                    var joinMethods = resultProjection
                        .Where(rp => rp.ClassName == al.Key.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber) &&
                                     (rp.ClassName != "BasicComplexityIncreaseWithAllTogether" || rp.MethodResults.Method.EndsWith("_03")))
                        .GroupBy(jm => new { jm.ClassName, jm.MethodResults.Method })
                        .ToList();

                    joinMethods
                        .ForEach(r =>
                        {
                            methodNameJoin = r.Key.ClassName + "." + r.Key.Method;
                            avgMeanJoin = r.Average(a => a.MethodResults.GetMeanInMS());
                        });

                    avgMeanJoin = Math.Round(avgMeanJoin, 2);

                    var methodNameBaseLine = string.Empty;
                    var avgMeanBaseLine = 0.00;

                    var baseLineMethods = resultProjection
                        .Where(rp => rp.ClassName == al.Key.ClassName &&
                                     rp.MethodResults.Method.Contains("Baseline", StringComparison.CurrentCultureIgnoreCase))
                        .GroupBy(jm => new { jm.ClassName, jm.MethodResults.Method })
                        .ToList();

                    baseLineMethods
                        .ForEach(r =>
                        {
                            methodNameBaseLine = r.Key.ClassName + "." + r.Key.Method;
                            avgMeanBaseLine = r.Average(a => a.MethodResults.GetMeanInMS());
                        });

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = methodNameJoin,
                        MethodNameBaseline = methodNameBaseLine,
                        AvgMeanNavProperty = avgMethodMean,
                        AvgMeanJoin = avgMeanJoin,
                        AvgMeanBaseline = avgMeanBaseLine
                    };

                    return row;
                })
                .OrderByDescending(c => c.AvgMeanJoin + c.AvgMeanNavProperty);

            CsvHelper.WriteToCsv(toCSVList, "AvgComplexityIncreaseForDBs");
        }

        /// <summary>
        /// ComplexityIncrease for navigation properties vs joins calculated as avarage for method by InputScale
        /// </summary>
        public static void AvgComplexityIncreaseForDBsByPercents(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAllTogether",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            var resultProjection = allResults
                .Where(al =>
                    al.InputScaleLevel == InputScaleLevel.Percent100 &&
                    classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            //var resultProjection = allResults
            //    .Where(al =>
            //        al.ReferenceScale == 1.00 &&
            //        classesToInclude.Any(cn => cn == al.ClassName))
            //    .OrderByDescending(al => al.MethodResults.GetMeanInMS())
            //    .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .GroupBy(al => new { al.ClassName, al.MethodResults.Method })
                .Select(al =>
                {
                    var methodName = al.Key.ClassName + "." + al.Key.Method;
                    var methodNumber = Regex.Match(al.Key.Method, @"_[\d]+$").Value;
                    var avgMethodMean = al.Average(a => a.MethodResults.GetMeanInMS());
                    avgMethodMean = Math.Round(avgMethodMean, 2);

                    var methodNameJoin = string.Empty;
                    var avgMeanJoin = 0.00;

                    var joinMethods = resultProjection
                        .Where(rp => rp.ClassName == al.Key.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .GroupBy(jm => new { jm.ClassName, jm.MethodResults.Method })
                        .ToList();

                    joinMethods
                        .ForEach(r =>
                        {
                            methodNameJoin = r.Key.ClassName + "." + r.Key.Method;
                            avgMeanJoin = r.Average(a => a.MethodResults.GetMeanInMS());
                        });

                    avgMeanJoin = Math.Round(avgMeanJoin, 2);

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = methodNameJoin,
                        AvgMeanNavProperty = avgMethodMean,
                        AvgMeanJoin = avgMeanJoin
                    };

                    return row;
                })
                .OrderByDescending(c => c.AvgMeanJoin + c.AvgMeanNavProperty);

            CsvHelper.WriteToCsv(toCSVList, "AvgComplexityIncreaseForDBs");
        }

        /// <summary>
        /// ComplexityIncrease for navigation properties vs joins calculated as avarage for method by InputScale
        /// </summary>
        public static void MaxComplexityWithAddons(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAllTogether",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            //var classesToInclude = new string[]
            //{
            //    "MaxComplexityWithAddons"
            //};

            var resultProjection = allResults
                .Where(al => classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .Select(al =>
                {
                    var methodName = al.ClassName + "." + al.MethodResults.Method;
                    var methodNumber = Regex.Match(al.MethodResults.Method, @"_[\d]+$").Value;
                    var joinMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .First();

                    var baselineMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Baseline", StringComparison.CurrentCultureIgnoreCase))
                        .First();

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = joinMethod.ClassName + "." + joinMethod.MethodResults.Method,
                        MethodNameBaseline = baselineMethod.MethodResults.Method,
                        MeanNavProperty = al.MethodResults.GetMeanInMS(),
                        MeanJoin = joinMethod.MethodResults.GetMeanInMS(),
                        MeanBaseline = baselineMethod.MethodResults.GetMeanInMS()
                    };

                    return row;
                })
                .OrderByDescending(c => c.MeanNavProperty - c.MeanJoin)
                .Take(5);

            CsvHelper.WriteToCsv(toCSVList, "AvgComplexityIncreaseForDBs");
        }
    }
}
