﻿using EFCoreDBContext.Config;
using InputDataToEntitiesMap.Config;
using ResultsReader.Helpers;
using ResultsReader.Models;
using System.Text.RegularExpressions;

namespace ResultsReader.Projections
{
    internal class TestResultProjectionsLowLvl
    {
        public static void CreateCSVsForAllScaleLevels(List<ResultModel> allResults, ConfigBase config = null)
        {
            foreach (var refScale in new double[] { 0.01, 0.5, 10 })
            {
                config = new ConfigBase()
                {
                    DatabaseProvider = config?.DatabaseProvider ?? DatabaseProviderType.MSSQLServer,
                    InputScaleLevel = config?.InputScaleLevel ?? InputScaleLevel.Percent50,
                    ReferenceScale = refScale
                };

                BaseComplexityIncreaseWithoutBaselines(allResults, config);
                AddonComplexityIncreaseWithoutBaselines(allResults, config);
            }
        }

        public static void BaseComplexityIncreaseWithoutBaselines(List<ResultModel> allResults, ConfigBase config = null)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseBase",
                "AvarageComplexityBase",
                "MaxComplexityBase"
            };

            if (config == null)
                config = new ConfigBase()
                {
                    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                    InputScaleLevel = InputScaleLevel.Percent100,
                    ReferenceScale = 0.01
                };

            string fileName = $"BaseComplexityIncreaseWithoutBaselines";

            ResultProjectionHelper.OutputToCsvResultsByNavVSJoin(allResults, classesToInclude, config, fileName);
        }

        public static void AddonComplexityIncreaseWithoutBaselines(List<ResultModel> allResults, ConfigBase config = null)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseWithAddons",
                "AvarageComplexityWithAddons",
                "MaxComplexityWithAddons"
            };

            if (config == null)
                config = new ConfigBase()
                {
                    DatabaseProvider = DatabaseProviderType.MSSQLServer,
                    InputScaleLevel = InputScaleLevel.Percent100,
                    ReferenceScale = 0.01
                };

            string fileName = "AddonComplexityIncreaseWithoutBaselines";

            ResultProjectionHelper.OutputToCsvResultsByNavVSJoin(allResults, classesToInclude, config, fileName);
        }

        public static void BaseComplexityIncreaseWithBaselines(List<ResultModel> allResults)
        {
            var classesToInclude = new string[]
            {
                "BasicComplexityIncreaseBase",
                "AvarageComplexityBase",
                "MaxComplexityBase"
            };

            //var classesToInclude = new string[]
            //{
            //    "BasicComplexityIncreaseWithAddons",
            //    "AvarageComplexityWithAddons",
            //    "MaxComplexityWithAddons"
            //};

            var resultProjection = allResults
                .Where(al =>
                    al.DatabaseProvider == DatabaseProviderType.MSSQLServer &&
                    al.InputScaleLevel == InputScaleLevel.Percent100 &&
                    al.ReferenceScale == 0.5 &&
                    classesToInclude.Any(cn => cn == al.ClassName))
                .OrderByDescending(al => al.MethodResults.GetMeanInMS())
                .ToList();

            var toCSVList = resultProjection
                .Where(al => al.MethodResults.Method.Contains("Property"))
                .Select(al =>
                {
                    var methodName = al.ClassName + "." + al.MethodResults.Method;
                    var methodNumber = Regex.Match(al.MethodResults.Method, @"_[\d]+$").Value;

                    var joinMethod = resultProjection
                        .Where(rp => rp.ClassName == al.ClassName &&
                                     rp.MethodResults.Method.Contains("Join") &&
                                     rp.MethodResults.Method.Contains(methodNumber))
                        .First();

                    var row = new
                    {
                        MethodNameNavProperty = methodName,
                        MethodNameJoin = joinMethod.ClassName + "." + joinMethod.MethodResults.Method,
                        MeanNavProperty = al.MethodResults.GetMeanInMS(),
                        MeanJoin = joinMethod.MethodResults.GetMeanInMS()
                    };

                    return row;
                });

            CsvHelper.WriteToCsv(toCSVList, "BaseComplexityIncrease");
        }
    }
}
